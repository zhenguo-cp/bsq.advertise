LOCAL_PATH := $(call my-dir)

# LOCAL_EXPORT_C_INCLUDES := \
# 	$(advertise_root_dir)/submodules/openssl/.. \
# 	$(advertise_root_dir)/submodules/openssl/include \
# 	$(advertise_root_dir)/submodules/openssl/crypto \
# 	$(advertise_root_dir)/submodules/openssl/crypto/asn1 \
# 	$(advertise_root_dir)/submodules/openssl/crypto/evp \
# 	$(advertise_root_dir)/submodules/openssl/include \
# 	$(advertise_root_dir)/submodules/openssl/include/openssl


include $(CLEAR_VARS)
LOCAL_MODULE := posix-prebuilt
LOCAL_SRC_FILES := libposix.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := ssl-prebuilt
LOCAL_EXPORT_C_INCLUDES := \
	$(advertise_root_dir)/submodules/openssl/.. \
	$(advertise_root_dir)/submodules/openssl/include \
	$(advertise_root_dir)/submodules/openssl/crypto
LOCAL_SRC_FILES := libssl-linphone-armeabi-v7a.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := crypto-prebuilt
LOCAL_EXPORT_C_INCLUDES := \
	$(advertise_root_dir)/submodules/openssl/crypto/asn1 \
        $(advertise_root_dir)/submodules/openssl/crypto/evp \
        $(advertise_root_dir)/submodules/openssl/include \
        $(advertise_root_dir)/submodules/openssl/include/openssl
LOCAL_SRC_FILES := libcrypto-linphone-armeabi-v7a.so
include $(PREBUILT_SHARED_LIBRARY)

include $(LOCAL_PATH)/../submodules/build/libnet/Android.mk
include $(advertise_root_dir)/submodules/disp_pwm/Android.mk
#include $(advertise_root_dir)/submodules/openssl/Android.mk
