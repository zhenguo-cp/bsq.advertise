APP_PROJECT_PATH := $(call my-dir)/..
APP_MODULES := bsq_net posix-prebuilt PWMJni

APP_STL := stlport_static

advertise_root_dir:=$(APP_PROJECT_PATH)

APP_BUILD_SCRIPT := $(advertise_root_dir)/jni/Android.mk
APP_PLATFORM := android-10
APP_ABI := armeabi-v7a armeabi
#APP_CFLAGS:=-DDISABLE_NEON
