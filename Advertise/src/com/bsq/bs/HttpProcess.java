package com.bsq.bs;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;

import com.bsq.advertise.AdvertiseController;
import com.bsq.advertise.BSQNet;
import com.bsq.advertise.util.SystemInfo;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class HttpProcess {
    private final static int HTTPPERIOD = 5000 * 2; // milliSecond 5sec
    private final static String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final DateFormat MDTM_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINESE);
    private Timer httpRequestTimer = new Timer("HTTPREQUEST", true);
    private Thread httpRequestThread = null;
    private ArrayList<httpDownloadDesc> downloadDescs;
    private final static String sdcardPath = AdvertiseController.getSdcardPath();
    private final static String resourcePath = sdcardPath + "/resource";
    private final static String httpSchedTmp = "schedule.tmp";
    private final static String httpSchedFile = "schedule.txt";
    private String errorData = "", statusData = "", cmdData = "", retData = "";
    private Context context = AdvertiseController.instance().getApplicationContext();
    private SmilParser parser = null;
    public final static int MSG_RECREATE = 0;
    public final static int MSG_UPDATE = 0xFC;
    private String filelistPath = null;
    private boolean isParseFilelist = false;
    private String publishName = null;
    private int fileDownloaderIndex = 0;
    private httpDownloadDesc curr = null;
    private long newProgress = 0;
    private long preProgress = 0;
    private long downloadTotalSize = 0;
    private SystemInfo systemInfo;
    private String serverPath = null;
    public static final String PREFS_NAME = "BSQPolicy";
    public static final String PREFS_REG = "BSQRegister";
    private boolean pendingMission = false;

    public HttpProcess() {
        systemInfo = AdvertiseController.instance().getSystemInfo();
        setDownloadDescs(new ArrayList<HttpProcess.httpDownloadDesc>());
        parser = new SmilParser(context);
        Schedule();
    }

    public void startHttpProcess() {
        TimerTask httpRequestTask = new TimerTask() {

            @Override
            public void run() {
                if (httpRequestThread == null) {
                    httpRequestThread = new Thread(httpRequestRunnable, "httpRequestThread");
                    httpRequestThread.start();
                }
                if (httpRequestThread != null)
                    httpRequestThread.run();
            }
        };

        try {
            httpRequestTimer.schedule(httpRequestTask, 0, HTTPPERIOD);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void stopHttpProcess() {
        httpRequestTimer.cancel();
    }

    private Runnable httpRequestRunnable = new Runnable() {

        @Override
        public void run() {
            sendHttpRequest();
        }
    };

    private void publishProgress(int arg0, int arg1) {
        // The *.smil fileSize is not count in total, so the newProgress will greater than 100
        // arg1 is referred as current file total size, arg0 is referred as current completedSize
        long total = 0;
        long done = 0;

        ((httpDownloadDesc) getDownloadDescs().get(fileDownloaderIndex)).completedSize = arg0;

        for (int i = 0; i < getDownloadDescs().size(); i++) {
            httpDownloadDesc tmp = ((httpDownloadDesc) getDownloadDescs().get(i));
            total += tmp.totalSize;
            done += tmp.completedSize;
        }

        if (total < 1)
            return;
        newProgress = done * 100 / total;
        if (newProgress > 100)
            newProgress = 100;

        //		Log.v("cook", "total: " +  total);
        //		Log.v("cook", "done: " +  done);

        if (newProgress > preProgress) {
            errorData = "";
            statusData = "";
            cmdData = "transfer";
            retData = publishName + "_3_" + total + "_" + newProgress;
            //Log.v("cook", "retData: " + retData);

            preProgress = newProgress;
            new Thread(notifyHttpServerRunnable).start();
        }

        downloadTotalSize = total;
    }

    Runnable notifyHttpServerRunnable = new Runnable() {

        @Override
        public void run() {
            notifyHttpServer();
        }
    };

    public void uploadFromBreak() {
        File tmpFile = new File(resourcePath + "/" + httpSchedTmp);
        if (tmpFile.exists()) {
            // We had a pending mission
            Log.v("cook", "We had a pending mission");
            pendingMission = true;
            try {
                BufferedReader reader = new BufferedReader(new FileReader(tmpFile));
                serverPath = reader.readLine();
                reader.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.v("cook", "serverPath: " + serverPath);
            serverPath = serverPath.substring(serverPath.indexOf("resource/") + 9);
            serverPath = serverPath.replaceAll("/files.txt", "");
            serverPath = serverPath.trim();

            publishName = serverPath.substring(serverPath.lastIndexOf('/') + 1);
            publishName = publishName.trim();

            getDownloadDescs().clear();
            httpDownloadDesc temp[] = new httpDownloadDesc[2];
            for (int i = 0; i < temp.length; i++)
                temp[i] = new httpDownloadDesc();
            temp[0].override = true;
            temp[0].completedSize = 0;
            temp[0].totalSize = 0;
            temp[0].filePath = systemInfo.getHttpAddress() + "/" + serverPath + "/filelist.txt";

            getDownloadDescs().add(temp[0]);

            temp[1].override = true;
            temp[1].completedSize = 0;
            temp[1].totalSize = 0;
            temp[1].filePath = systemInfo.getHttpAddress() + "/" + serverPath + "/files.txt";

            getDownloadDescs().add(temp[1]);

            File resource = new File(resourcePath);
            if (!resource.exists())
                resource.mkdir();

            errorData = "";
            statusData = "";
            cmdData = "transfer";
            retData = publishName + "_3_0_0";

            //			sendHttpRequest();

            DownloadSystemFiles();
        }
    }

    private void updateSchedule() {
        Log.v("cook", "update successful, write schedule");
        try {
            File schedule = new File(resourcePath + "/" + httpSchedFile);
            if (!schedule.exists())
                schedule.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(schedule);
            outputStream.write(filelistPath.getBytes());
            outputStream.write('\n');
            outputStream.flush();
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void Schedule() {
        Log.v("cook", "schedule");

        ArrayList<String> SmilPath = new ArrayList<String>();

        File schedule = new File(resourcePath + "/" + httpSchedFile);
        if (schedule.exists()) {
            try {
                String temp;
                BufferedReader reader = new BufferedReader(new FileReader(schedule));
                while (true) {
                    temp = reader.readLine();
                    if (temp == null)
                        break;
                    temp.trim();
                    SmilPath.add(temp);
                }
                reader.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            if (SmilPath.size() > 0)
                parseFileList(SmilPath.get(0));

            String cipher = null;
            SharedPreferences setting = AdvertiseController.instance().getSharedPreferences(PREFS_REG, Context.MODE_MULTI_PROCESS);
            String toVerify = setting.getString("register", cipher);

            //			Log.v("cook", "toVerify: " + toVerify);

            if (toVerify != null && BSQNet.instance().verifyCipher(toVerify.trim())) {
                Log.v("cook", "registered user, go to play");
                if (AdvertiseController.isUsingCS()) {
                    AdvertiseController.ftpMSG.obtainMessage(AdvertiseController.MSG_START_CS).sendToTarget();
                } else {
                    AdvertiseController.ftpMSG.obtainMessage(AdvertiseController.MSG_START_BS).sendToTarget();
                }
            } else {
                AdvertiseController.instance().alertUser();
                cmdData = "register";
                retData = BSQNet.instance().getCipher();
            }

            new Thread(new Runnable() {

                @Override
                public void run() {
                    uploadFromBreak();
                }
            }).start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void notifyHttpServer() {
        String currentDateTime = new SimpleDateFormat(DATEFORMAT, Locale.US).format(new Date(System.currentTimeMillis()));

        if (newProgress == 100)
            retData = publishName + "_1_" + downloadTotalSize + "_100";
        try {
            String requestUrl = systemInfo.getHttpAddress() +
                    "/Commheart.php?" +
                    "t=" + currentDateTime +
                    "&id=" + (SystemInfo.getHardwareAddress() != null ? SystemInfo.getHardwareAddress().replace(":", "") : "") +
                    "&ip=" + SystemInfo.getHostAddress() +
                    "&ver=" + SystemInfo.getVersion() +
                    "&name=" + systemInfo.getName() +
                    "&pos=" + systemInfo.getPosition() +
                    "&error=" + errorData +
                    "&status=" + statusData +
                    "&cmd=" + cmdData +
                    "&rt=" + retData;

            requestUrl = new String(requestUrl.getBytes("utf-8"), "8859_1");

            URL url = new URL(requestUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);

            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                byte[] buf = new byte[256];
                int length = in.read(buf);
                if (length > 0)
                    handleResponse(new String(buf, 0, length));

                if (newProgress == 100) {
                    updateSchedule();
                    AdvertiseController.ftpMSG.obtainMessage(MSG_RECREATE).sendToTarget();
                }

            }

            urlConnection.disconnect();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (SocketException e1) {
            Log.v("cook", "SocketException: " + e1.getLocalizedMessage());
            e1.printStackTrace();
        } catch (IOException e1) {
            Log.v("cook", "IOException: " + e1.getLocalizedMessage());
            e1.printStackTrace();
        }
    }

    private synchronized boolean sendHttpRequest() {
        String httpAddress = null;
        if (cmdData.equals("register"))
            httpAddress = "http://121.199.8.115:30002"; // Hardcode
        else
            httpAddress = systemInfo.getHttpAddress();

        if (httpAddress == null || httpAddress.isEmpty()) {
            stopHttpProcess();
            return false;
        }

        String currentDateTime = new SimpleDateFormat(DATEFORMAT, Locale.US).format(new Date(System.currentTimeMillis()));

        try {
            String requestUrl = httpAddress +
                    "/Commheart.php?" +
                    "t=" + currentDateTime +
                    "&id=" + (SystemInfo.getHardwareAddress() != null ? SystemInfo.getHardwareAddress().replace(":", "") : "") +
                    "&ip=" + SystemInfo.getHostAddress() +
                    "&ver=" + SystemInfo.getVersion() +
                    "&name=" + systemInfo.getName() +
                    "&pos=" + systemInfo.getPosition() +
                    "&error=" + errorData +
                    "&status=" + statusData +
                    "&cmd=" + cmdData +
                    "&rt=" + retData;
            requestUrl = new String(requestUrl.getBytes("utf-8"), "8859_1");
            Log.v("cook", "requestUrl: " + requestUrl);

            URL url = new URL(requestUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);

            if (cmdData.contains("screen")) {
                urlConnection.setDoOutput(true);
                //				urlConnection.setChunkedStreamingMode(0);
                BufferedOutputStream os = new BufferedOutputStream(urlConnection.getOutputStream());
                File file = new File(sdcardPath, "screen.jpg");
                if (file.exists()) {
                    long length = file.length();
                    char[] buf = new char[(int) length];
                    BufferedReader reader = new BufferedReader(new FileReader(file));
                    reader.read(buf);
                    reader.close();

                    String body = "&cmd=" + cmdData + "&rt=" + retData + new String(buf);
                    body = new String(body.getBytes("utf-8"), "8859_1");
                    os.write(body.getBytes("utf-8"));
                    os.flush();
                    os.close();
                    Log.v("cook", "upload screen.jpg done");

                    errorData = "";
                    statusData = "";
                    cmdData = "";
                    retData = "";
                }
            }

            int status = urlConnection.getResponseCode();
            if (status == HttpURLConnection.HTTP_OK) {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                byte[] buf = new byte[256];
                int length = in.read(buf);
                if (length > 0)
                    handleResponse(new String(buf, 0, length));
            }

            urlConnection.disconnect();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (SocketException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        return true;
    }

    private void handleResponse(String response) throws IOException {
        Log.v("cook", "Response: " + response);
        if (response.contains("web_cmd")) {
            String webCmd = response.substring(response.indexOf("web_cmd="));
            if (webCmd.contains("schedule")) {
                //				Log.v("cook", "response: " + response);

                SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_MULTI_PROCESS);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("usingCS", false);
                AdvertiseController.setUsingCS(false);
                editor.commit();

                Log.v("cook", "Set play BS as default");

                serverPath = webCmd.substring(webCmd.indexOf(" ")).replace("/filelist.txt|files.txt", "").trim();
                publishName = serverPath.substring(serverPath.lastIndexOf('/') + 1);

                Log.v("cook", "serverPath: " + serverPath);
                Log.v("cook", "publishName: " + publishName);

                getDownloadDescs().clear();
                httpDownloadDesc temp[] = new httpDownloadDesc[2];
                for (int i = 0; i < temp.length; i++)
                    temp[i] = new httpDownloadDesc();
                temp[0].override = true;
                temp[0].completedSize = 0;
                temp[0].totalSize = 0;
                temp[0].filePath = systemInfo.getHttpAddress() + "/" + serverPath + "/filelist.txt";

                getDownloadDescs().add(temp[0]);

                temp[1].override = true;
                temp[1].completedSize = 0;
                temp[1].totalSize = 0;
                temp[1].filePath = systemInfo.getHttpAddress() + "/" + serverPath + "/files.txt";

                getDownloadDescs().add(temp[1]);

                File resource = new File(resourcePath);
                if (!resource.exists())
                    resource.mkdir();

                File tmpFile = new File(resourcePath + "/" + httpSchedTmp);
                if (!tmpFile.exists())
                    tmpFile.createNewFile();

                FileOutputStream outputStream = new FileOutputStream(tmpFile);
                outputStream.write(new String(resourcePath + "/" + serverPath + "/files.txt").getBytes());
                outputStream.write('\n');
                outputStream.close();

                errorData = "";
                statusData = "";
                cmdData = "transfer";
                retData = publishName + "_3_0_0";
                sendHttpRequest();

                Ion.getDefault(context).cancelAll();
                DownloadSystemFiles();
            } else if (webCmd.contains("update")) {
                String updateUrl = systemInfo.getHttpAddress() + "/update/config.sqensq";
                final String downloadPath = resourcePath + File.separator + "Advertise.apk";
                Log.v("cook", "updateUrl: " + updateUrl);
                Log.v("cook", "downloadPath: " + downloadPath);
                Ion.with(context, updateUrl)
                        .write(new File(downloadPath))
                        .setCallback(new FutureCallback<File>() {

                            @Override
                            public void onCompleted(Exception arg0, File arg1) {
                                Log.v("cook", "Download completed: " + arg1.getPath());
                                AdvertiseController.instance().updateAPK(arg1.getPath());
                                //						BSQNet.instance().updateAPK(arg1.getPath());
                            }
                        });

            } else if (webCmd.contains("register")) {
                String cipher = webCmd.split(";")[1];
                if (BSQNet.instance().verifyCipher(cipher)) {
                    SharedPreferences setting = AdvertiseController.instance().getSharedPreferences(PREFS_REG, Context.MODE_MULTI_PROCESS);
                    SharedPreferences.Editor editor = setting.edit();
                    editor.putString("register", cipher);
                    editor.commit();
                    cmdData = "";
                    retData = "";
                    AdvertiseController.instance().cancelAlert();
                    Log.v("cook", "registered, write info");
                    if (AdvertiseController.isUsingCS()) {
                        AdvertiseController.ftpMSG.obtainMessage(AdvertiseController.MSG_START_CS).sendToTarget();
                    } else {
                        AdvertiseController.ftpMSG.obtainMessage(AdvertiseController.MSG_START_BS).sendToTarget();
                    }
                }
            } else if (webCmd.equals("web_cmd=poweroff")) {
                SystemInfo.setStatus(SystemInfo.DEVICE_SHUTDOWN);
            } else if (webCmd.contains("sleep")) {
                SystemInfo.setStatus(SystemInfo.DEVICE_SLEEP);
                cmdData = "sleep";
                retData = "ok";
                errorData = "";
                statusData = "";
            } else if (webCmd.contains("reboot")) {
                SystemInfo.setStatus(SystemInfo.DEVICE_REBOOT);
            } else if (webCmd.contains("screen")) {
                long fileSize = SystemInfo.takeScreenShot();
                cmdData = "screen";
                retData = "1024,768,screen.jpg<" + fileSize + ">";
                errorData = "";
                statusData = "";
            } else if (webCmd.contains("config|PowerOn")) {
                String elements[] = webCmd.split("=");
                if (elements.length >= 3) {
                    Log.v("cook", "set PowerOn: " + elements[2]);
                    systemInfo.setPoweron(elements[2]);
                } else if (elements.length >= 2) {
                    Log.v("cook", "Cancel the schedule of powerOn/Off");
                    systemInfo.CancelPowerSchedule();
                }
            } else if (webCmd.contains("config|PowerOff")) {
                String elements[] = webCmd.split("=");
                if (elements.length >= 3) {
                    Log.v("cook", "set PowerOff: " + elements[2]);
                    systemInfo.setPoweroff(elements[2]);
                    systemInfo.writeConfig();
                }
            } else if (webCmd.contains("config|volume")) {
                String volume = webCmd.substring(webCmd.lastIndexOf('=') + 1);
                Log.v("cook", "volume: " + volume);
                SystemInfo.setVolume(Integer.parseInt(volume));
                systemInfo.writeConfig();
            } else if (webCmd.contains("delete|")) {
                String fileName = webCmd.substring(webCmd.lastIndexOf('|') + 1);
                Log.v("cook", "filetodelete: " + fileName);
                cmdData = "delete";
                retData = SystemInfo.deleteFile(fileName) == true ? "ok" : "error";
                errorData = "";
                statusData = "";
            } else if (webCmd.contains("playerInfo")) {
                String name = webCmd.substring(webCmd.indexOf('|') + 1, webCmd.lastIndexOf('|'));
                String position = webCmd.substring(webCmd.lastIndexOf('|') + 1);
                systemInfo.setInfoR(name, position);
            } else if (webCmd.contains("synctime")) {
                cmdData = "synctime";
                retData = "ok";
                errorData = "";
                statusData = "";

                int start = response.indexOf("svr_time=");
                if (start == -1)
                    return;
                start += 9;

                int end = response.lastIndexOf('|');
                if (end == -1)
                    return;

                String srv_time = response.substring(start, end).trim();
                Log.v("cook", "Set Time: " + srv_time);

                try {
                    Date d = MDTM_DATE_FORMAT.parse(srv_time);
                    Calendar set = Calendar.getInstance();
                    set.clear();
                    set.setTime(d);
                    SystemClock.setCurrentTimeMillis(set.getTimeInMillis());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (webCmd.contains("filelist")) {
                cmdData = "filelist";
                retData = SystemInfo.getFilesList();
                errorData = "";
                statusData = "ok";
            }
        }

        if (isParseFilelist)
            parseFileList(filelistPath);
    }

    private void parseFiles(String path) throws IOException {
        Log.v("cook", "parseFiles");
        BufferedReader reader = new BufferedReader(new FileReader(path));

        getDownloadDescs().clear();
        String temp;

        while (true) {
            temp = reader.readLine();
            if (temp == null)
                break;

            httpDownloadDesc desc = new httpDownloadDesc();
            desc.totalSize = Long.parseLong(temp.split(" ")[1]);
            desc.filePath = temp.split(" ")[2];
            getDownloadDescs().add(desc);
            temp = null;
        }

        reader.close();

        for (int i = 0; i < getDownloadDescs().size(); i++) {
            Log.v("cook", "filePath: " + getDownloadDescs().get(i).filePath + " totalSize: " + getDownloadDescs().get(i).totalSize);
        }
        Log.v("cook", "downloading files from files: " + getDownloadDescs().size());
        fileDownloaderIndex = 0;
        newProgress = 0;
        preProgress = 0;
        FileDownloader(fileDownloaderProgressCallback, FileDownloaderCallback);
    }

    private void parseFileList(String path) throws IOException {
        if (!new File(path).exists())
            return;

        Document document = Jsoup.parse(new File(path), "UTF-8");
        Elements list = document.getElementsByAttribute("src");

        String temp;
        for (int i = 0; i < list.size(); i++) {
            temp = list.get(i).attr("src");
            if (new File(resourcePath + temp.substring(temp.lastIndexOf('/'))).exists())
                //				parser.startSmilParser(resourcePath + temp.substring(temp.lastIndexOf('/')));
                parser.startSmilParser(resourcePath + File.separator + temp);

        }

        isParseFilelist = false;
    }

    private void DownloadSystemFiles() {
        String requestUrl;

        Log.v("cook", "DownloadSystemFiles");
        // Create server Path
        if (!new File(resourcePath + "/" + serverPath).exists()) {
            if (!new File(resourcePath + "/" + serverPath).mkdirs()) {
                Log.v("cook", "Creating DIR: " + resourcePath + "/" + serverPath + " failed");
                return;
            }
        }

        for (int i = 0; i < getDownloadDescs().size(); i++) {
            final httpDownloadDesc desc = ((httpDownloadDesc) getDownloadDescs().get(i));
            requestUrl = desc.filePath;


            Ion.with(context, requestUrl)
                    .write(new File(resourcePath + "/" + serverPath + "/" + requestUrl.substring(requestUrl.lastIndexOf('/'))))
                    .setCallback(new FutureCallback<File>() {

                        @Override
                        public void onCompleted(Exception arg0, File arg1) {
                            Log.v("cook", "download completed: " + arg1.getName());

                            if (arg1.getName().equals("files.txt")) {
                                try {
                                    parseFiles(arg1.getPath());
                                    return;
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else if (arg1.getName().equals("filelist.txt")) {
                                filelistPath = arg1.getPath();

                                //						try {
                                //							File schedule = new File(resourcePath + "/" + httpSchedFile);
                                //							if (!schedule.exists())
                                //								schedule.createNewFile();
                                //							FileOutputStream outputStream = new FileOutputStream(schedule);
                                //							outputStream.write(filelistPath.getBytes());
                                //							outputStream.write('\n');
                                //							outputStream.close();
                                //						} catch (FileNotFoundException e) {
                                //							e.printStackTrace();
                                //						} catch (IOException e) {
                                //							e.printStackTrace();
                                //						}
                            }
                        }
                    });
        }
    }

    private boolean Exists(String filePath) {
        if (pendingMission)
            return false;
        if (filePath.contains(".smil"))
            return false;
        return new File(filePath).exists();
    }

    private void FileDownloader(ProgressCallback pc, FutureCallback<File> fc) {
        String requestUrl;

        if (fileDownloaderIndex >= getDownloadDescs().size()) {
            isParseFilelist = true; // indicate download is completed
            fileDownloaderIndex = 0;
            pendingMission = false;
            // TODO: for test, comment this code for now
            File tmpFile = new File(resourcePath + "/" + httpSchedTmp);
            if (tmpFile.exists())
                tmpFile.delete();
            //AdvertiseController.ftpMSG.obtainMessage(MSG_RECREATE).sendToTarget(); // recreate when the newprogress equals 100
            return;
        }

        final httpDownloadDesc desc = ((httpDownloadDesc) getDownloadDescs().get(fileDownloaderIndex));
        curr = desc;
        requestUrl = desc.filePath;

        Log.v("cook", "Downloading: " + requestUrl);

        if (requestUrl.contains(".smil")) {
            Ion.with(context, requestUrl)
                    .progressHandler(pc)
                    .write(new File(resourcePath + File.separator + serverPath + File.separator + requestUrl.substring(requestUrl.lastIndexOf('/'))))
                    .setCallback(fc);
        }

//		Ion.with(context, requestUrl)
//		.progressHandler(pc)
//		.write(new File(resourcePath + requestUrl.substring(requestUrl.lastIndexOf('/'))))
//		.setCallback(fc);

        if (!Exists(resourcePath + requestUrl.substring(requestUrl.lastIndexOf('/'))))
            Ion.with(context, requestUrl)
                    .progressHandler(pc)
                    .write(new File(resourcePath + requestUrl.substring(requestUrl.lastIndexOf('/'))))
                    .setCallback(fc);
        else {
            Log.v("cook", "File exists: " + requestUrl);
            File tmpFile = new File(resourcePath + requestUrl.substring(requestUrl.lastIndexOf('/')));
            FileInputStream in;

            try {
                in = new FileInputStream(tmpFile);
                if (in.available() < curr.totalSize)
                    Ion.with(context, requestUrl)
                            .progressHandler(pc)
                            .write(new File(resourcePath + requestUrl.substring(requestUrl.lastIndexOf('/'))))
                            .setCallback(fc);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            curr.completedSize += curr.totalSize;
            fileDownloaderIndex++;
            FileDownloader(fileDownloaderProgressCallback, FileDownloaderCallback);
            // In this case, onCompleted callback will not be called, so we download manual
        }
    }

    private FutureCallback<File> FileDownloaderCallback = new FutureCallback<File>() {

        @Override
        public void onCompleted(Exception arg0, File arg1) {
            Log.v("cook", "download completed: " + arg1.getPath());

            // download next file
            fileDownloaderIndex++;
            FileDownloader(fileDownloaderProgressCallback, FileDownloaderCallback);
        }
    };

    private ProgressCallback fileDownloaderProgressCallback = new ProgressCallback() {

        @Override
        public void onProgress(int arg0, int arg1) {
            // publish download progress
            publishProgress(arg0, arg1);
        }
    };

    /**
     * @return the downloadDescs
     */
    public synchronized ArrayList<httpDownloadDesc> getDownloadDescs() {
        return downloadDescs;
    }

    /**
     * @param downloadDescs the downloadDescs to set
     */
    public void setDownloadDescs(ArrayList<httpDownloadDesc> downloadDescs) {
        this.downloadDescs = downloadDescs;
    }

    public class httpDownloadDesc {
        boolean override;
        String filePath;
        long completedSize;
        long totalSize;
    }
}
