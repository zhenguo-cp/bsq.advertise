package com.bsq.bs;

import android.content.Context;
import android.util.Log;

import com.bsq.advertise.AdvertiseController;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SmilParser {

    private int WIDTH, HEIGHT;
    public static BSProgramme programme;
    private final static String sdcardPath = AdvertiseController.getSdcardPath();
    private final static String resourcePath = sdcardPath + "/resource";
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);

    public SmilParser(Context context) {
        WIDTH = context.getResources().getDisplayMetrics().widthPixels;
        HEIGHT = context.getResources().getDisplayMetrics().heightPixels;

        programme = new BSProgramme();
    }

    public enum ELEMENTTYPE {
        BG, VIDEO, IMAGE, TEXT, CLOCK, DATE, WEATHER, BROWSER;
    }

    public class Coords {
        String id;
        public float left;
        public float top;
        public int width;
        public int height;
    }

    public class LayoutDetails {
        public Coords coords;
        public ELEMENTTYPE elementType;
        public String bgSrc;
        public String url;
        public String region;
        public String city;
        public int dur;
        public float size;
        public String language;
        public String fgColor, bgColor;
        public String mode;

        public ArrayList<String> videoSrc;
        public ArrayList<Integer> videoDur;
        public ArrayList<Integer> videoRepeat;
        public ArrayList<String> imgSrc;
        public ArrayList<String> imgDur;
        public ArrayList<String> imgMode;
        public ArrayList<String> textSrc;
        public ArrayList<String> textRepeat;
        public ArrayList<String> textFgColor;
        public ArrayList<String> textbgColor;
        public ArrayList<String> textMode;
        public ArrayList<String> textSpeed;
        public ArrayList<String> textRSS;
        public ArrayList<String> textFont;
        public ArrayList<Float> textSize;

        public LayoutDetails() {
            coords = new Coords();
            videoSrc = new ArrayList<String>();
            videoDur = new ArrayList<Integer>();
            videoRepeat = new ArrayList<Integer>();
            imgSrc = new ArrayList<String>();
            imgDur = new ArrayList<String>();
            imgMode = new ArrayList<String>();
            textSrc = new ArrayList<String>();
            textRepeat = new ArrayList<String>();
            textFgColor = new ArrayList<String>();
            textbgColor = new ArrayList<String>();
            textMode = new ArrayList<String>();
            textSpeed = new ArrayList<String>();
            textRSS = new ArrayList<String>();
            textFont = new ArrayList<String>();
            textSize = new ArrayList<Float>();
        }
    }

    public class BSProgramme {
        private Date start;
        private Date end;

        public BSProgramme() {
            elements = new ArrayList<>();

            try {
                start = SIMPLE_DATE_FORMAT.parse("2014-01-01 01:01:00");
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                end = SIMPLE_DATE_FORMAT.parse("2014-01-01 01:01:00");
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        public ArrayList<LayoutDetails> elements;

        public Date getStart() {
            return start;
        }

        public Date getEnd() {
            return end;
        }
    }

    public void startSmilParser(String path) throws IOException {
        Log.v("cook", "Parse Smil: " + path);
        if (!new File(path).exists()) {
            Log.v("cook", "Parse smil failed");
            return;
        }
        Document doc = Jsoup.parse(new File(path), "UTF-8");
        Elements layout = doc.getElementsByAttribute("id");
        Elements extra = doc.getElementsByAttribute("region");

        programme.elements.clear();

        Element cur = null;
        for (int i = 0; i < layout.size(); i++) {
            cur = layout.get(i);
            if (cur.attr("id").contains("bg")) {
                Double left = Double.valueOf(cur.attr("left"));
                left = left / 100 * WIDTH;
                Double top = Double.valueOf(cur.attr("top"));
                top = top / 100 * HEIGHT;
                Double width = Double.valueOf(cur.attr("width"));
                width = width / 100 * WIDTH;
                Double height = Double.valueOf(cur.attr("height"));
                height = height / 100 * HEIGHT;

                LayoutDetails bg = new LayoutDetails();
                bg.coords.left = left.floatValue();
                bg.coords.top = top.floatValue();
                bg.coords.width = width.intValue();
                bg.coords.height = height.intValue();
                bg.coords.id = cur.attr("id");
                bg.elementType = ELEMENTTYPE.BG;

                programme.elements.add(bg);
            } else if (cur.attr("id").contains("video")) {
                Double left = Double.valueOf(cur.attr("left"));
                left = left / 100 * WIDTH;
                Double top = Double.valueOf(cur.attr("top"));
                top = top / 100 * HEIGHT;
                Double width = Double.valueOf(cur.attr("width"));
                width = width / 100 * WIDTH;
                Double height = Double.valueOf(cur.attr("height"));
                height = height / 100 * HEIGHT;

                LayoutDetails video = new LayoutDetails();
                video.coords.left = left.floatValue();
                video.coords.top = top.floatValue();
                video.coords.width = width.intValue();
                video.coords.height = height.intValue();
                video.coords.id = cur.attr("id");

                video.elementType = ELEMENTTYPE.VIDEO;

                programme.elements.add(video);
            } else if (cur.attr("id").contains("img")) {
                Double left = Double.valueOf(cur.attr("left"));
                left = left / 100 * WIDTH;
                Double top = Double.valueOf(cur.attr("top"));
                top = top / 100 * HEIGHT;
                Double width = Double.valueOf(cur.attr("width"));
                width = width / 100 * WIDTH;
                Double height = Double.valueOf(cur.attr("height"));
                height = height / 100 * HEIGHT;

                LayoutDetails img = new LayoutDetails();
                img.coords.left = left.floatValue();
                img.coords.top = top.floatValue();
                img.coords.width = width.intValue();
                img.coords.height = height.intValue();
                img.coords.id = cur.attr("id");
                img.elementType = ELEMENTTYPE.IMAGE;

                programme.elements.add(img);
            } else if (cur.attr("id").contains("date")) {
                Double left = Double.valueOf(cur.attr("left"));
                left = left / 100 * WIDTH;
                Double top = Double.valueOf(cur.attr("top"));
                top = top / 100 * HEIGHT;
                Double width = Double.valueOf(cur.attr("width"));
                width = width / 100 * WIDTH;
                Double height = Double.valueOf(cur.attr("height"));
                height = height / 100 * HEIGHT;

                LayoutDetails date = new LayoutDetails();
                date.coords.left = left.floatValue();
                date.coords.top = top.floatValue();
                date.coords.width = width.intValue();
                date.coords.height = height.intValue();
                date.coords.id = cur.attr("id");
                date.elementType = ELEMENTTYPE.DATE;

                programme.elements.add(date);
            } else if (cur.attr("id").contains("clock")) {
                Double left = Double.valueOf(cur.attr("left"));
                left = left / 100 * WIDTH;
                Double top = Double.valueOf(cur.attr("top"));
                top = top / 100 * HEIGHT;
                Double width = Double.valueOf(cur.attr("width"));
                width = width / 100 * WIDTH;
                Double height = Double.valueOf(cur.attr("height"));
                height = height / 100 * HEIGHT;

                LayoutDetails clock = new LayoutDetails();
                clock.coords.left = left.floatValue();
                clock.coords.top = top.floatValue();
                clock.coords.width = width.intValue();
                clock.coords.height = height.intValue();
                clock.coords.id = cur.attr("id");
                clock.elementType = ELEMENTTYPE.CLOCK;

                programme.elements.add(clock);
            } else if (cur.attr("id").contains("text")) {
                Double left = Double.valueOf(cur.attr("left"));
                left = left / 100 * WIDTH;
                Double top = Double.valueOf(cur.attr("top"));
                top = top / 100 * HEIGHT;
                Double width = Double.valueOf(cur.attr("width"));
                width = width / 100 * WIDTH;
                Double height = Double.valueOf(cur.attr("height"));
                height = height / 100 * HEIGHT;

                LayoutDetails text = new LayoutDetails();
                text.coords.left = left.floatValue();
                text.coords.top = top.floatValue();
                text.coords.width = width.intValue();
                text.coords.height = height.intValue();
                text.coords.id = cur.attr("id");
                text.elementType = ELEMENTTYPE.TEXT;

                programme.elements.add(text);
            } else if (cur.attr("id").contains("weather")) {
                Double left = Double.valueOf(cur.attr("left"));
                left = left / 100 * WIDTH;
                Double top = Double.valueOf(cur.attr("top"));
                top = top / 100 * HEIGHT;
                Double width = Double.valueOf(cur.attr("width"));
                width = width / 100 * WIDTH;
                Double height = Double.valueOf(cur.attr("height"));
                height = height / 100 * HEIGHT;

                LayoutDetails weather = new LayoutDetails();
                weather.coords.left = left.floatValue();
                weather.coords.top = top.floatValue();
                weather.coords.width = width.intValue();
                weather.coords.height = height.intValue();
                weather.coords.id = cur.attr("id");
                weather.elementType = ELEMENTTYPE.WEATHER;

                programme.elements.add(weather);

            } else if (cur.attr("id").contains("browser")) {
                Double left = Double.valueOf(cur.attr("left"));
                left = left / 100 * WIDTH;
                Double top = Double.valueOf(cur.attr("top"));
                top = top / 100 * HEIGHT;
                Double width = Double.valueOf(cur.attr("width"));
                width = width / 100 * WIDTH;
                Double height = Double.valueOf(cur.attr("height"));
                height = height / 100 * HEIGHT;

                LayoutDetails browser = new LayoutDetails();
                browser.coords.left = left.floatValue();
                browser.coords.top = top.floatValue();
                browser.coords.width = width.intValue();
                browser.coords.height = height.intValue();
                browser.coords.id = cur.attr("id");
                browser.elementType = ELEMENTTYPE.BROWSER;

                programme.elements.add(browser);
            }
        }

        for (int i = 0; i < extra.size(); i++) {
            cur = extra.get(i);
            if (cur.attr("region").contains("bg")) {
                LayoutDetails bg = FindElement(cur.attr("region"));
                if (bg != null) {
                    bg.bgSrc = resourcePath + cur.attr("src").substring(cur.attr("src").lastIndexOf('/'));
                }
            } else if (cur.attr("region").contains("video")) {
                LayoutDetails video = FindElement(cur.attr("region"));
                if (video != null) {
                    video.videoSrc.add(resourcePath + cur.attr("src").substring(cur.attr("src").lastIndexOf('/')));
                    video.videoDur.add(Integer.parseInt(cur.attr("dur")));
                    video.videoRepeat.add(Integer.parseInt(cur.attr("repeat")));
                }
            } else if (cur.attr("region").contains("img")) {
                LayoutDetails img = FindElement(cur.attr("region"));
                if (img != null) {
                    img.imgSrc.add(resourcePath + cur.attr("src").substring(cur.attr("src").lastIndexOf('/')));
                    img.imgDur.add(cur.attr("dur"));
                    img.imgMode.add(cur.attr("mode"));
                }
            } else if (cur.attr("region").contains("text")) {
                LayoutDetails text = FindElement(cur.attr("region"));
                if (text != null) {
                    text.textSrc.add(resourcePath + cur.attr("src").substring(cur.attr("src").lastIndexOf('/')));
                    text.textRepeat.add(cur.attr("repeat"));
                    text.textFgColor.add(cur.attr("tc"));
                    text.textbgColor.add(cur.attr("bgc"));
                    text.textMode.add(cur.attr("mode"));
                    text.textSpeed.add(cur.attr("speed"));
                    text.textRSS.add(cur.attr("rss"));
                    text.textFont.add(cur.attr("font"));
                    text.textSize.add(Float.parseFloat(cur.attr("size")));
                }
            } else if (cur.attr("region").contains("date")) {
                LayoutDetails date = FindElement(cur.attr("region"));
                date.size = Float.parseFloat(cur.attr("size"));
                date.fgColor = cur.attr("tc");
                date.bgColor = cur.attr("bgc");
            } else if (cur.attr("region").contains("clock")) {
                LayoutDetails clock = FindElement(cur.attr("region"));
                clock.size = Float.parseFloat(cur.attr("size"));
                clock.fgColor = cur.attr("tc");
                clock.bgColor = cur.attr("bgc");
            } else if (cur.attr("region").contains("weather")) {
                LayoutDetails weather = FindElement(cur.attr("region"));
                weather.city = cur.attr("city");
                weather.dur = Integer.parseInt(cur.attr("dur"));
                weather.size = Float.parseFloat(cur.attr("size"));
                weather.language = cur.attr("language");
                weather.fgColor = "#" + cur.attr("tc");
                weather.bgColor = "#" + cur.attr("bgc");
            } else if (cur.attr("region").contains("browser")) {
                LayoutDetails browser = FindElement(cur.attr("region"));
                browser.url = cur.attr("url");
            }
        }

        Log.v("cook", "Parse smil file successed");
    }

    private LayoutDetails FindElement(String region) {
        for (int i = 0; i < programme.elements.size(); i++) {
            if (programme.elements.get(i).coords.id.equals(region))
                return programme.elements.get(i);
        }

        return null;
    }
}
