package com.bsq.schedule;

import android.content.Context;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.bsq.advertise.AdvertiseController;
import com.bsq.advertise.Common;
import com.bsq.advertise.ViewType;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProgramProvider {

	private List<Map<String, String>> startEndTimePairs;
	private List<Map<String, String>> dataTypePairs;
	private List<Program> programmes;
	private int programIndex;
	private File resource, config;
	private int WIDTH, HEIGHT;

	public ProgramProvider(Context context) {
		startEndTimePairs = new ArrayList<>();
		dataTypePairs = new ArrayList<>();
		programmes = new ArrayList<>();

		initialize(context);
	}

	public boolean initialize(Context context) {
		String sdcardPath = AdvertiseController.getSdcardPath();
		if (sdcardPath == null)
			return false;
		resource = new File(sdcardPath + "/resource");
		if (!resource.exists())
			return false;
		config = new File(resource + "/config.xml");
		if (!config.exists())
			return false;

		WIDTH = context.getResources().getDisplayMetrics().widthPixels;
		HEIGHT = context.getResources().getDisplayMetrics().heightPixels;

		try {
			BufferedInputStream buf = new BufferedInputStream(new FileInputStream(config.getPath()));
			xmlParser(buf, null);
		} catch (FileNotFoundException e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
			return false;
		} catch (XmlPullParserException e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public Program getCurrentProgram(int index) {
		if (programmes.size() - 1 >= programIndex)
			return programmes.get(index);
		
		return null;
	}
	
	public int getProgramSize() {
		return programmes.size();
	}

	private void readPlaylist(XmlPullParser parser, Program display) {
		try {
			parser.require(XmlPullParser.START_TAG, null, "playlist");
			Common cur;

			while (parser.nextTag() != XmlPullParser.END_TAG) {
				if (parser.getName().equals("video")) {
					String file_path = null;
					if (parser.getAttributeName(0).equals("stream"))
						file_path = new String(parser.getAttributeValue(0));
					else
						file_path = new String(resource.getPath() + "/" + parser.getAttributeValue(0));
					for (int i = 0; i < display.viewList.size(); i++) {
						cur = display.viewList.get(i);
						if (cur.viewType == ViewType.VIDEO) {
							cur.videoPlayList.add(file_path);
							break;
						}
					}
					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("image")) {
					// take care of the index
					int index = Integer.parseInt(parser.getAttributeValue(0), 10);

					for (int i = 0; i < display.viewList.size(); i++) {
						cur = display.viewList.get(i);
						if (cur.viewType == ViewType.IMAGE 
								&& cur.placeInfo.position == index) {
							cur.imagePlayList.add(resource.getPath() + "/" + parser.getAttributeValue(1));
							cur.animList.add(parser.getAttributeValue(2));
							cur.intervalList.add(parser.getAttributeValue(3).toString()) ;
							break;
						}
					}

					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("text")) {
					int index = Integer.parseInt(parser.getAttributeValue(1), 10);
					for (int i = 0; i < display.viewList.size(); i++) {
						cur = display.viewList.get(i);
						if (cur.viewType == ViewType.TEXT
								&& cur.placeInfo.position == index) {
							cur.filePath = resource.getPath() + "/" + parser.getAttributeValue(0); 
							cur.fgColorStr = parser.getAttributeValue(2).toString();
							cur.bgColorStr = parser.getAttributeValue(3).toString();
							cur.textSize = Integer.parseInt(parser.getAttributeValue(4).toString(), 10);
							cur.mode = parser.getAttributeValue(5);
							cur.speed = parser.getAttributeValue(6);
							cur.font = parser.getAttributeValue(7);

							break;
						}
					}
					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("clock")) {
					for (int i = 0; i < display.viewList.size(); i++) {
						cur = display.viewList.get(i);
						if (cur.viewType == ViewType.CLOCK) {
							cur.textSize = Integer.parseInt(parser.getAttributeValue(0), 10);
							cur.fgColorStr = parser.getAttributeValue(1).toString();
							cur.bgColorStr = parser.getAttributeValue(2).toString();
							break; 
						}
					}
					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("date")) {
					for (int i = 0; i < display.viewList.size(); i++) {
						cur = display.viewList.get(i);
						if (cur.viewType == ViewType.DATE) {
							cur.textSize = Integer.parseInt(parser.getAttributeValue(0), 10);
							cur.fgColorStr = parser.getAttributeValue(1).toString();
							cur.bgColorStr = parser.getAttributeValue(2).toString();
							break;
						}
					}
					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("weather")) {
					for (int i = 0; i < display.viewList.size(); i++) {
						cur = display.viewList.get(i);
						if (cur.viewType == ViewType.WEATHER) {
							cur.language = parser.getAttributeValue(0);
							cur.textSize = Integer.parseInt(parser.getAttributeValue(1), 10);
							cur.interval = parser.getAttributeValue(2);
							cur.fgColorStr = parser.getAttributeValue(3);
							cur.bgColorStr = parser.getAttributeValue(4);
							cur.cityName = parser.getAttributeValue(5);
							break;
						}
					}
					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("browser")) {
					for (int i = 0; i < display.viewList.size(); i++) {
						cur = display.viewList.get(i);
						if (cur.viewType == ViewType.BROWSER) {
							cur.browser_url = parser.getAttributeValue(0);
							break;
						}
					}
					parser.nextTag(); // skip END_TAG like  "/>"
				}
			}

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readTemplate(XmlPullParser parser, Program display) {

		try {
			parser.require(XmlPullParser.START_TAG, null, "template");
			int image_positon = 0;
			int text_positon = 0;

			while (parser.nextTag() != XmlPullParser.END_TAG) {
				if (parser.getName().equals("bg")) {
					Common bg = new Common();
					bg.viewType = ViewType.BG;
					bg.filePath = resource.getPath() + "/" + parser.getAttributeValue(0);
					display.viewList.add(bg);

					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("video")) {
					Double x = Double.valueOf(parser.getAttributeValue(0));
					x = x / 100 * WIDTH;
					Double y = Double.valueOf(parser.getAttributeValue(1));
					y = y / 100 * HEIGHT;
					Double w = Double.valueOf(parser.getAttributeValue(2));
					w = w / 100 * WIDTH;
					Double h = Double.valueOf(parser.getAttributeValue(3));
					h = h / 100 * HEIGHT;

					Common video = new Common();
					video.viewType = ViewType.VIDEO;
					video.placeInfo.left = x.floatValue();
					video.placeInfo.top = y.floatValue();
					video.placeInfo.right = w.intValue();
					video.placeInfo.buttom = h.intValue();
					display.viewList.add(video);
					display.setHasVideos(true);

					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("image")) {
					// take care of index
					Double x = Double.valueOf(parser.getAttributeValue(0));
					//x = x / 100 * WIDTH;
					x = WIDTH * x / 100;
					Double y = Double.valueOf(parser.getAttributeValue(1));
					y = y / 100 * HEIGHT;
					Double w = Double.valueOf(parser.getAttributeValue(2));
					w = w / 100 * WIDTH;
					Double h = Double.valueOf(parser.getAttributeValue(3));
					h = h / 100 * HEIGHT;

					Common image = new Common();
					image.viewType = ViewType.IMAGE;
					image.placeInfo.left = x.floatValue();
					image.placeInfo.top = y.floatValue();
					image.placeInfo.right = w.intValue();
					image.placeInfo.buttom = h.intValue();
					image.placeInfo.position = image_positon;
					image_positon++;
					display.viewList.add(image);
					display.setHasImages(true);

					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("text")) {
					// take care of index
					Double x = Double.valueOf(parser.getAttributeValue(0));
					//x = x / 100 * WIDTH;
					x = WIDTH * x / 100;
					Double y = Double.valueOf(parser.getAttributeValue(1));
					y = y / 100 * HEIGHT;
					Double w = Double.valueOf(parser.getAttributeValue(2));
					w = w / 100 * WIDTH;
					Double h = Double.valueOf(parser.getAttributeValue(3));
					h = h / 100 * HEIGHT;

					Common text = new Common();
					text.viewType = ViewType.TEXT;
					text.placeInfo.left = x.floatValue();
					text.placeInfo.top = y.floatValue();
					text.placeInfo.right = w.intValue();
					text.placeInfo.buttom = h.intValue();
					text.placeInfo.position = text_positon;
					text_positon++;
					display.viewList.add(text);

					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("clock")) {
					Double x = Double.valueOf(parser.getAttributeValue(0));
					//x = x / 100 * WIDTH;
					x = WIDTH * x / 100;
					Double y = Double.valueOf(parser.getAttributeValue(1));
					y = y / 100 * HEIGHT;
					Double w = Double.valueOf(parser.getAttributeValue(2));
					w = w / 100 * WIDTH;
					Double h = Double.valueOf(parser.getAttributeValue(3));
					h = h / 100 * HEIGHT;

					Common clock = new Common();
					clock.viewType = ViewType.CLOCK;
					clock.placeInfo.left = x.floatValue();
					clock.placeInfo.top = y.floatValue();
					clock.placeInfo.right = w.intValue();
					clock.placeInfo.buttom = h.intValue();
					display.viewList.add(clock);

					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("date")) {
					Double x = Double.valueOf(parser.getAttributeValue(0));
					//x = x / 100 * WIDTH;
					x = WIDTH * x / 100;
					Double y = Double.valueOf(parser.getAttributeValue(1));
					y = y / 100 * HEIGHT;
					Double w = Double.valueOf(parser.getAttributeValue(2));
					w = w / 100 * WIDTH;
					Double h = Double.valueOf(parser.getAttributeValue(3));
					h = h / 100 * HEIGHT;

					Common date = new Common();
					date.viewType = ViewType.DATE;
					date.placeInfo.left = x.floatValue();
					date.placeInfo.top = y.floatValue();
					date.placeInfo.right = w.intValue();
					date.placeInfo.buttom = h.intValue();
					display.viewList.add(date);

					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("weather")) {
					Double x = Double.valueOf(parser.getAttributeValue(0));
					//x = x / 100 * WIDTH;
					x = WIDTH * x / 100;
					Double y = Double.valueOf(parser.getAttributeValue(1));
					y = y / 100 * HEIGHT;
					Double w = Double.valueOf(parser.getAttributeValue(2));
					w = w / 100 * WIDTH;
					Double h = Double.valueOf(parser.getAttributeValue(3));
					h = h / 100 * HEIGHT;

					Common weather = new Common();
					weather.viewType = ViewType.WEATHER;
					weather.placeInfo.left = x.floatValue();
					weather.placeInfo.top = y.floatValue();
					weather.placeInfo.right = w.intValue();
					weather.placeInfo.buttom = h.intValue();
					display.viewList.add(weather);

					parser.nextTag(); // skip END_TAG like  "/>"
				} else if (parser.getName().equals("browser")) {
					Double x = Double.valueOf(parser.getAttributeValue(0));
					//x = x / 100 * WIDTH;
					x = WIDTH * x / 100;
					Double y = Double.valueOf(parser.getAttributeValue(1));
					y = y / 100 * HEIGHT;
					Double w = Double.valueOf(parser.getAttributeValue(2));
					w = w / 100 * WIDTH;
					Double h = Double.valueOf(parser.getAttributeValue(3));
					h = h / 100 * HEIGHT;

					Common browser = new Common();
					browser.viewType = ViewType.BROWSER;
					browser.placeInfo.left = x.floatValue();
					browser.placeInfo.top = y.floatValue();
					browser.placeInfo.right = w.intValue();
					browser.placeInfo.buttom = h.intValue();
					display.viewList.add(browser);

					parser.nextTag(); // skip END_TAG like  "/>"
				}
			}
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readProgramlist(XmlPullParser parser) {
		Program displayContext = new Program();

		try {
			parser.require(XmlPullParser.START_TAG, null, "programlist");
			displayContext.setScheduleTime(parser.getAttributeValue(1));
			
			if (displayContext.getScheduleTime().equals("default")) {
				displayContext.setHasTimes(false);
			} else 
				displayContext.setHasTimes(true);
			displayContext.setScheduleType(parser.getAttributeValue(0));

			while (parser.nextTag() != XmlPullParser.END_TAG) {
				if (parser.getName().endsWith("template")) {
					readTemplate(parser, displayContext);
				} else if (parser.getName().endsWith("playlist")) {
					readPlaylist(parser, displayContext);
				}
			}

			programmes.add(displayContext);
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readSys(XmlPullParser parser) {
		try {
			while (parser.nextTag() != XmlPullParser.END_TAG) {

				if (parser.getName().equals("datetype")) {
					Map<String, String> datatype = new HashMap<String, String>();
					datatype.put("type", parser.getAttributeValue(0));
					datatype.put("include", parser.getAttributeValue(1));
					dataTypePairs.add(datatype);
					parser.nextTag(); // skip END_TAG like  "/>"
					Log.v("cook", "add dataType");
				} else if (parser.getName().equals("time")) {
					Map<String, String> time = new HashMap<String, String>();
					time.put("start", parser.getAttributeValue(0));
					time.put("end", parser.getAttributeValue(1));
					startEndTimePairs.add(time);
					parser.nextTag(); // skip END_TAG like  "/>"
					Log.v("cook", "add time");
				}
			}
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readProgram(XmlPullParser parser) {
		try {
			parser.require(XmlPullParser.START_TAG, null, "program");

			while (parser.nextTag() != XmlPullParser.END_TAG) {

				if (parser.getName().equals("sys")) {
					readSys(parser);
				} else if (parser.getName().equals("programlist")) {
					readProgramlist(parser);
				}
			}
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean xmlParser(InputStream arg0, String arg1) throws XmlPullParserException, IOException, NumberFormatException  {
		try {
			XmlPullParser xpp = Xml.newPullParser();
			xpp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			xpp.setInput(arg0, arg1);
			xpp.nextTag();
			int eventType = xpp.getEventType();

			if (eventType != XmlPullParser.END_DOCUMENT && xpp.getName().endsWith("program"))
				readProgram(xpp);

			return true;
		} finally {
			arg0.close();
		}
	}

	public List<Map<String, String>> getStartEndTimePairs() {
		return startEndTimePairs;
	}

	public List<Map<String, String>> getDataTypePairs() {
		return dataTypePairs;
	}

	public List<Program> getProgrammes() {
		return programmes;
	}
}
