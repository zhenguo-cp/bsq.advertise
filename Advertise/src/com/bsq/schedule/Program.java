package com.bsq.schedule;

import java.util.ArrayList;

import com.bsq.advertise.Common;

public class Program {

	public enum TYPE {
		BG, VIDEO, IMAGE, TEXT, CLOCK, DATE, WEATHER;
	}

	public enum Policy {
		VIDEO, IMAEG, TIME, NONE;
	}
	
	public ArrayList<Common> viewList;
	private Policy mPolicy;

	private String scheduleTime;
	private String scheduleType;
	private boolean hasVideos;
	private boolean hasImages;
	private boolean hasTimes;
	
	public Program() {
		super();

		this.viewList = new ArrayList<Common>();
		this.mPolicy = Policy.NONE;
		
		this.scheduleTime = null;
		this.hasVideos = false;
		this.hasImages = false;
		this.hasTimes = false;
	}

	public Policy getPolicy() {
		if (hasTimes) {
			mPolicy = Policy.TIME;
		} else if (hasVideos) {
			mPolicy = Policy.VIDEO;
		} else if (hasImages) {
			mPolicy = Policy.IMAEG;
		}
		
		return mPolicy;
	}
	
	public String getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(String scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	/**
	 * @return the scheduleType
	 */
	public String getScheduleType() {
		return scheduleType;
	}

	/**
	 * @param scheduleType the scheduleType to set
	 */
	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public boolean isHasVideos() {
		return hasVideos;
	}

	public void setHasVideos(boolean hasVideos) {
		this.hasVideos = hasVideos;
	}

	public boolean isHasImages() {
		return hasImages;
	}

	public void setHasImages(boolean hasImages) {
		this.hasImages = hasImages;
	}

	public boolean isHasTimes() {
		return hasTimes;
	}

	public void setHasTimes(boolean hasTimes) {
		this.hasTimes = hasTimes;
	}
}
