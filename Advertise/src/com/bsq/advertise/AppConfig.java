package com.bsq.advertise;

/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version
 * 2 of the License, or (at your option) any later version.
 * Created by macro on 15/6/21.
 */
public class AppConfig {
    public static String TV_INTRO_FILE_PATH = AdvertiseController.getSdcardPath() + "/resource/K720.png";
}
