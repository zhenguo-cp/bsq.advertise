package com.bsq.advertise;

public class NotifyData {

	public enum SOURCE {
		PROGRAM, VIDEO, IMAGE, NONE;
	}
	
	private SOURCE mSource;
	
	public NotifyData() {
		mSource = SOURCE.NONE;
	}
	
	public void setSource(SOURCE source) {
		this.mSource = source;
	}
	
	public SOURCE getSource() {
		return this.mSource;
	}
}
