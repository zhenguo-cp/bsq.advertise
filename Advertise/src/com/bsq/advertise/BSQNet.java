package com.bsq.advertise;

public class BSQNet {
	
	private static BSQNet instance;
	/* Declare the native method we needed */
	public native int UDP_Creation();
	public native void UDP_Destory();
	public native int TCP_Creation();
	public native void TCP_Destory();
	public native void TCP_reply(String msg);
	public native String getCipher();
	public native boolean verifyCipher(String cipher);
	public native boolean updateAPK(String path);
	
	/* Load native library */
	static {
		System.loadLibrary("crypto-linphone-armeabi-v7a");
		System.loadLibrary("ssl-linphone-armeabi-v7a");
		System.loadLibrary("bsq_net");
	}
	
	public BSQNet() {
		instance = this;
	}
	
	public static BSQNet instance() {
		if (instance == null) {
			instance = new BSQNet();
		}
		
		return instance;
	}
}
