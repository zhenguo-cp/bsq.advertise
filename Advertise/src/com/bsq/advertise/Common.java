package com.bsq.advertise;

import java.util.ArrayList;

public class Common {
	public Common() {
		placeInfo = new Coordinate();
		videoPlayList = new ArrayList<String>();
		imagePlayList = new ArrayList<String>();
		intervalList = new ArrayList<String>();
		animList = new ArrayList<String>();
	}
	
	public ViewType viewType;
	public Coordinate placeInfo;
	
	public ArrayList<String> videoPlayList;
	public ArrayList<String> imagePlayList;
	public ArrayList<String> intervalList;
	public ArrayList<String> animList;

	public String filePath;
	public String mode;
	public String speed;
	public String font;
	public String text;
	
	public float textSize;
	public String bgColorStr;
	public String fgColorStr;
	
	public String language;
	public String interval;
	public String cityName;
	
	public String browser_url;
}
