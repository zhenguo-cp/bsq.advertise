package com.bsq.advertise;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bsq.advertise.NotifyData.SOURCE;
import com.bsq.advertise.util.AlarmReceiver;
import com.bsq.advertise.util.Dev_MountInfo;
import com.bsq.advertise.util.SystemInfo;
import com.bsq.advertise.util.SystemUiHider;
import com.bsq.bs.SmilParser;
import com.bsq.bs.SmilParser.BSProgramme;
import com.bsq.bs.SmilParser.ELEMENTTYPE;
import com.bsq.bs.SmilParser.LayoutDetails;
import com.bsq.schedule.Program;
import com.bsq.schedule.Program.Policy;
import com.bsq.schedule.ProgramProvider;
import com.bsq.udp.BsqVideoView;
import com.bsq.widget.A20VideoPlayer;
import com.bsq.widget.ImageAreaFragment;
import com.bsq.widget.ImagePlayer;

import org.apache.http.util.EncodingUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import io.vov.vitamio.LibsChecker;

public class AdvertiseController extends FragmentActivity implements Observer {
    private static final DateFormat PROGRAM_SCHEDULE_DATE_FORMAT = new SimpleDateFormat(
            "HH:mm", Locale.CHINESE);
    /**
     * Messenger for communicating with service.
     */
    private Messenger mService = null;
    /**
     * Flag indicating whether we have called bind on the service.
     */
    private boolean mIsBound;

    private static AdvertiseController instance;
    private static boolean usingCS = false;
    private BsqVideoView mBsqVideoView;
    private A20VideoPlayer a20VideoPlayer;
    private FragmentWeather fragmentWeather;
    private FragmentManager fragmentManager;
    private static String sdcardPath;
    private AlertDialog dialog = null;
    private ImagePlayer player;

    public static final int POWERSCHEDULEON = 0xff1;
    public static final int POWERSCHEDULEOFF = 0xff2;


    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 200;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    private BroadcastReceiver mExternalStorageReceiver;
    private boolean mExternalStorageAvailable = false;
    private boolean mExternalStorageWriteable = false;

    private FTPClient ftpClient;
    private SystemInfo systemInfo;

    public final static int MSG_START_BS = 1;
    public final static int MSG_START_CS = 2;
    public final static int MSG_UPDATE = 0xFC;

    public static final String PREFS_NAME = "BSQPolicy";

    private ProgramProvider dataStruct;
    public final static String PROGRAM_INDEX = "_index";
    private static final String TAG = AdvertiseController.class.getSimpleName();
    ;
    private int programIndex = 0;
    private Timer mScheduletimer;
    private ImageView mQrCode;
    private ImageView mTVIntro;
    private int currentPostion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advertise_controller);

        instance = this;

        if (!LibsChecker.checkVitamioLibs(this)) {
            java.lang.System.exit(-1);
            return;
        }

        // get sdcard path first
        startWatchingExternalStorage();

        dataStruct = new ProgramProvider(this);

        if (savedInstanceState != null) {
            programIndex = savedInstanceState.getInt(PROGRAM_INDEX);
            if (programIndex >= dataStruct.getProgramSize())
                programIndex = 0;
        }

        Log.d(TAG, "programIndex; " + programIndex);

        mScheduletimer = new Timer(true);

        systemInfo = new SystemInfo(getApplicationContext());

        setFtpClient(new FTPClient());

        final View contentView = findViewById(R.id.fullscreen_content);
        contentView.setKeepScreenOn(true);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView,
                HIDER_FLAGS);
        mSystemUiHider.setup();

        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = contentView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            contentView
                                    .animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            contentView.setVisibility(visible ? View.VISIBLE
                                    : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        mBsqVideoView = (BsqVideoView) findViewById(R.id.video_player);
        a20VideoPlayer = (A20VideoPlayer) findViewById(R.id.a20_video_player);
        a20VideoPlayer
                .setOnCompletionListener(new android.media.MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(android.media.MediaPlayer mp) {
                        a20VideoPlayer.stopPlayback();
                        a20VideoPlayer.playVideoList();
                    }
                });
        a20VideoPlayer
                .setOnErrorListener(new android.media.MediaPlayer.OnErrorListener() {

                    @Override
                    public boolean onError(android.media.MediaPlayer mp,
                                           int what, int extra) {
                        Log.v("cook", "A20 video player error");
                        return false;
                    }
                });

        fragmentManager = getSupportFragmentManager();
        fragmentWeather = (FragmentWeather) fragmentManager
                .findFragmentById(R.id.fragment_weather);
        fragmentWeather.setUserVisibleHint(false);
        fragmentWeather.getView().setVisibility(View.GONE);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME,
                MODE_MULTI_PROCESS);
        boolean usingCS = settings.getBoolean("usingCS", isUsingCS());
        setUsingCS(usingCS);

        doBindService();

        mQrCode = (ImageView) findViewById(R.id.qr_code);
        mTVIntro = (ImageView) findViewById(R.id.tv_intro);

        startQRCode(sdcardPath + "/resource/USA.png");

        // startService(new Intent(this, Watchdog.class));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean result = super.onKeyDown(keyCode, event);

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (mTVIntro.getVisibility() == View.VISIBLE) {
                    mTVIntro.setVisibility(View.GONE);
                } else {
                    if (AppConfig.TV_INTRO_FILE_PATH != null) {
                        mTVIntro.setImageURI(Uri.fromFile(new File(AppConfig.TV_INTRO_FILE_PATH)));
                        mTVIntro.setVisibility(View.VISIBLE);
                    }
                }
                result = true;

                break;
            default:
                break;
        }

        return result;
    }

    public void startQRCode(String imagePath) {
        if (imagePath != null) {
            mQrCode.setVisibility(View.VISIBLE);
            currentPostion = -1;
            Uri uri = Uri.fromFile(new File(imagePath));
            mQrCode.setImageURI(uri);
            mQrCode.post(mQRCodeRunnable);
        }
    }

    private Runnable mQRCodeRunnable = new Runnable() {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
        @Override
        public void run() {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mQrCode.getLayoutParams();
            if (params != null) {
                if (currentPostion == -1) {
                    currentPostion = RelativeLayout.ALIGN_PARENT_LEFT;
                    params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                } else if (currentPostion == RelativeLayout.ALIGN_PARENT_LEFT) {
                    params.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    currentPostion = RelativeLayout.ALIGN_PARENT_RIGHT;
                } else if (currentPostion == RelativeLayout.ALIGN_PARENT_RIGHT) {
                    params.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    currentPostion = RelativeLayout.ALIGN_PARENT_LEFT;
                }

                mQrCode.setLayoutParams(params);
            }

            mQrCode.postDelayed(this, 10000);
        }
    };

    public static AdvertiseController instance() {
        if (instance == null)
            return null;

        return instance;
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);

        if (level == TRIM_MEMORY_UI_HIDDEN) {
            Log.v("cook", "onTrimMemory");
            if (mBsqVideoView != null) {
                mBsqVideoView.stopPlayback();
                mBsqVideoView.disableSync();
                mBsqVideoView = null;
            }
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mBsqVideoView != null) {
            mBsqVideoView.stopPlayback();
            mBsqVideoView.disableSync();
            mBsqVideoView = null;
        }

        if (a20VideoPlayer != null)
            a20VideoPlayer.stopPlayback();

        if (mScheduletimer != null)
            mScheduletimer.cancel();

        stopWatchingExternalStorage();
        doUnbindService();

        Log.v("cook", "onDestroy");
    }

    @Override
    protected void onStop() {
        super.onStop();

        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(PREFS_NAME,
                MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("usingCS", isUsingCS());

        // Commit the edits!
        editor.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Log.d(TAG, "onSaveInstanceState");
        outState.putInt(PROGRAM_INDEX, ++programIndex);
    }

    @Override
    protected void onResume() {
        super.onResume();

        a20VideoPlayer.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // TODO: unstable function
        final Timer timer = new Timer();
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                ActivityManager manager = (ActivityManager) getApplicationContext()
                        .getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = manager.getRunningTasks(1).get(0).topActivity;
                int taskId = manager.getRunningTasks(2).get(1).id;

                if (cn != null && !cn.equals("com.bsq.advertise")) {
                    manager.moveTaskToFront(taskId, 0);
                    a20VideoPlayer.start();
                }
            }
        };
        timer.schedule(task, 1000 * 60 * 5); // 5 secs for default
        a20VideoPlayer.start();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void alertUser() {
        try {
            dialog = new AlertDialog.Builder(this)
                    .setTitle(R.string.title)
                    .setMessage(
                            "终端ID: " + SystemInfo.getHardwareAddress())
                            // .setMessage(R.string.message)
                    .setPositiveButton(R.string.btn_ok, null).show();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void cancelAlert() {
        dialog.cancel();
    }

    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    public static Handler ftpMSG = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FTPClient.MSG_RECREATE:
                    instance.recreate();
                    break;
                case MSG_START_BS:
                    instance.startAdvertisingBS();
                    break;
                case MSG_START_CS:
                    instance.startAdvertising();
                    break;
            }
        }

        ;
    };

    public void updateAPK(String path) {
        final String apkPath = path;
        new Thread(new Runnable() {

            @Override
            public void run() {
                Log.v("cook", "Start the update progress");
                try {
                    String[] cmd = {"/system/bin/sh", "-c", "sync "};
                    Process process = Runtime.getRuntime().exec(cmd);
                    process.waitFor();
                    process.destroy();
                    Log.v("cook", "sync done");

                    String[] cmd1 = {"/system/bin/sh", "-c",
                            "pm install -r " + apkPath.trim()};
                    Process process1 = Runtime.getRuntime().exec(cmd1);
                    process1.waitFor();
                    process1.destroy();
                } catch (IOException e) {
                    Log.v("cook", "updateAPK: " + e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    Log.v("cook", "updateAPK: " + e.getLocalizedMessage());
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void sleepDevice() {
        Log.v("cook", "sleepDevice");
        try {
            String[] cmd = {"/system/bin/sh", "-c",
                    "echo standby > /sys/power/state",};
            Process process = Runtime.getRuntime().exec(cmd);
            process.waitFor();
            process.destroy();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void wakeUpDevice() {
        Log.v("cook", "wakeUpDevice");
        try {
            String[] cmd = {"/system/bin/sh", "-c",
                    "echo on > /sys/power/state",};
            Process process = Runtime.getRuntime().exec(cmd);
            process.waitFor();
            process.destroy();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void rebootDevice() {
        Intent i = new Intent(Intent.ACTION_REBOOT);
        i.putExtra("nowait", 1);
        i.putExtra("interval", 1);
        i.putExtra("window", 0);
        sendBroadcast(i);
    }

    public void shutdownDevice() {
        Intent i = new Intent(Intent.ACTION_SHUTDOWN);
        i.putExtra("nowait", 1);
        i.putExtra("interval", 1);
        i.putExtra("window", 0);
        sendBroadcast(i);
    }

    public void takeScreenShot() {
        View view = this.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache(true);
        Bitmap bm = view.getDrawingCache(true);
        try {
            FileOutputStream fos = new FileOutputStream(sdcardPath
                    + "/test.png");
            bm.compress(CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setBackground() {
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.ad_ctl);
        rl.setBackgroundResource(R.drawable.noprogram);
    }

    public void setPowerOnSchedule(long milliseconds) {
        Log.v("cook", "powerOn after: " + milliseconds / 1000 / 60);
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra("action", POWERSCHEDULEON);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
                POWERSCHEDULEON, intent, 0);

        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + milliseconds, pendingIntent);
    }

    public void setPowerOffSchedule(long milliseconds) {
        Log.v("cook", "powerOff after: " + milliseconds / 1000 / 60);
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra("action", POWERSCHEDULEOFF);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
                POWERSCHEDULEOFF, intent, 0);

        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + milliseconds, pendingIntent);
    }

    private Runnable advertiseProvider = new Runnable() {
        @Override
        public void run() {
            recreate();
        }
    };

    public void startAdvertisingBS() {
        BSProgramme programme = SmilParser.programme;
        if (programme != null && programme.elements.size() == 0) {
            setBackground();
            return;
        }

        Calendar calendar = Calendar.getInstance();
        if (programme.getStart() != null && programme.getEnd() != null
                && calendar.getTime().after(programme.getStart()) && calendar.getTime().before(programme.getEnd())) {
            // 播放时间范围内，设置结束定时器
            mQrCode.postDelayed(advertiseProvider, (programme.getEnd().getTime() - calendar.getTime().getTime()));
        } else if (calendar.getTime().before(programme.getStart())) {
            // 时间未到,设置开始定时器
            mQrCode.postDelayed(advertiseProvider, programme.getStart().getTime() - calendar.getTime().getTime());
        } else {
            // 过时了
        }

        for (int i = 0; i < programme.elements.size(); i++) {
            LayoutDetails cur = programme.elements.get(i);
            if (cur.elementType == ELEMENTTYPE.BG) {
                ImageView imageView = new ImageView(this);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setImageURI(Uri.parse(cur.bgSrc));
                RelativeLayout rl = (RelativeLayout) findViewById(R.id.ad_ctl);
                rl.setBackground(imageView.getDrawable());
            } else if (cur.elementType == ELEMENTTYPE.VIDEO) {
                boolean using_a20 = true;

                for (int j = 0; j < cur.videoSrc.size(); j++) {
                    if (cur.videoSrc.get(j).startsWith("rtsp://")
                            || cur.videoSrc.get(j).startsWith("rtmp://")
                            || cur.videoSrc.get(j).startsWith("http://")) {
                        using_a20 = false;
                        break;
                    }
                }

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        cur.coords.width, cur.coords.height);
                mBsqVideoView.setLayoutParams(params);
                mBsqVideoView.setVisibility(View.VISIBLE);
                mBsqVideoView.setLayout(cur.coords.left, cur.coords.top,
                        cur.coords.width, cur.coords.width);
                mBsqVideoView.videoPlayList = cur.videoSrc;

                if (mBsqVideoView.videoPlayList.size() > 0) {
                    if (systemInfo.getSyncType().equals("0"))
                        mBsqVideoView.playVideoList();
                    else if (systemInfo.getSyncType().equals("1")) {
                        // master
                        mBsqVideoView.enableSync(BsqVideoView.UDP_MASTER, systemInfo.getGroupId());
                        mBsqVideoView.playVideoList();
                    } else if (systemInfo.getSyncType().equals("2")) {
                        // slave
                        mBsqVideoView.enableSync(BsqVideoView.UDP_SLAVE, systemInfo.getGroupId());
                    }
                }

                //				if (using_a20) {
                //					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                //							cur.coords.width, cur.coords.height);
                //					a20VideoPlayer.setLayoutParams(params);
                //					a20VideoPlayer.setVisibility(View.VISIBLE);
                //					a20VideoPlayer.setX(cur.coords.left);
                //					a20VideoPlayer.setY(cur.coords.top);
                //					a20VideoPlayer.videoPlayList = cur.videoSrc;
                //					if (a20VideoPlayer.videoPlayList.size() > 0)
                //						a20VideoPlayer.playVideoList();
                //				} else {
                //					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                //							cur.coords.width, cur.coords.height);
                //					mBsqVideoView.setLayoutParams(params);
                //					mBsqVideoView.setVisibility(View.VISIBLE);
                //					mBsqVideoView.setLayout(cur.coords.left, cur.coords.top,
                //							cur.coords.width, cur.coords.width);
                //					mBsqVideoView.videoPlayList = cur.videoSrc;
                //					if (mBsqVideoView.videoPlayList.size() > 0)
                //						mBsqVideoView.playVideoList();
                //				}

            } else if (cur.elementType == ELEMENTTYPE.IMAGE) {
                player = new ImagePlayer(this);
                player.initFrom(cur);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        cur.coords.width, cur.coords.height);
                addContentView(player, params);
                player.setX(cur.coords.left);
                player.setY(cur.coords.top);
            } else if (cur.elementType == ELEMENTTYPE.TEXT) {
                MarqueeTextView marqueeTextView = new MarqueeTextView(
                        getApplicationContext(), null);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        cur.coords.width, cur.coords.height);
                addContentView(marqueeTextView, params);

                String text = null;
                if (cur.textSrc.size() > 0)
                    text = readMessage(cur.textSrc.get(0));
                else
                    continue;
                marqueeTextView.setX(cur.coords.left);
                marqueeTextView.setY(cur.coords.top);
                marqueeTextView.setGravity(0x10);
                marqueeTextView.setText(text);
                marqueeTextView
                        .setTypeface(Typeface.MONOSPACE, Typeface.NORMAL);
                marqueeTextView.getPaint().setTextSize(
                        cur.textSize.get(0) / 100 * params.height);
                // marqueeTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
                // cur.textSize.get(0) / 100 * params.height);
                marqueeTextView.setTextColor(Color.parseColor("#"
                        + cur.textFgColor.get(0)));
                marqueeTextView.setBackgroundColor(Color.parseColor("#"
                        + cur.textbgColor.get(0)));
                if (cur.textSpeed.get(0).equals("1"))
                    marqueeTextView.setScrollSpeed(MarqueeTextView.SPEED_FAST);
                else if (cur.textSpeed.get(0).equals("8000"))
                    marqueeTextView
                            .setScrollSpeed(MarqueeTextView.SPEED_NORMAL);
                else if (cur.textSpeed.get(0).equals("30000"))
                    marqueeTextView.setScrollSpeed(MarqueeTextView.SPEED_SLOW);
                if (cur.textMode.get(0).equals("1"))
                    marqueeTextView.startScroll(Integer.parseInt(cur.textMode
                            .get(0)));
            } else if (cur.elementType == ELEMENTTYPE.DATE) {
                DateView dateView = new DateView(this);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        cur.coords.width, cur.coords.height);
                addContentView(dateView, params);
                dateView.setX(cur.coords.left);
                dateView.setY(cur.coords.top);
                dateView.getPaint().setTextSize(cur.size / 100 * params.height);
                dateView.setBackgroundColor(Color.parseColor("#" + cur.bgColor));
                dateView.setTextColor(Color.parseColor("#" + cur.fgColor));
                dateView.setGravity(Gravity.CENTER);
            } else if (cur.elementType == ELEMENTTYPE.CLOCK) {
                ClockView clockView = new ClockView(this);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        cur.coords.width, cur.coords.height);
                addContentView(clockView, params);
                clockView.setX(cur.coords.left);
                clockView.setY(cur.coords.top);
                clockView.getPaint()
                        .setTextSize(cur.size / 100 * params.height);
                clockView.setBackgroundColor(Color
                        .parseColor("#" + cur.bgColor));
                clockView.setTextColor(Color.parseColor("#" + cur.fgColor));
                clockView.setGravity(0x11);
            } else if (cur.elementType == ELEMENTTYPE.WEATHER) {
                fragmentWeather.setWeatherLocation(cur.city);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        cur.coords.width, cur.coords.height);
                fragmentWeather.getView().setVisibility(View.VISIBLE);
                fragmentWeather.getView().setX(cur.coords.left);
                fragmentWeather.getView().setY(cur.coords.top);
                fragmentWeather.getView().setLayoutParams(params);
                fragmentWeather.getView().setBackgroundColor(
                        Color.parseColor(cur.bgColor));
                fragmentWeather.setWeatherLocation(cur.city);
            } else if (cur.elementType == ELEMENTTYPE.BROWSER) {
                BSQBrowser browserview = new BSQBrowser(this);
                browserview.setDownloadListener(new DownloadListener() {

                    @Override
                    public void onDownloadStart(String url, String userAgent,
                                                String contentDisposition, String mimetype,
                                                long contentLength) {
                    }
                });
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        cur.coords.width, cur.coords.height);
                addContentView(browserview, params);
                browserview.setX(cur.coords.left);
                browserview.setY(cur.coords.top);
                browserview.loadUrl(cur.url);
            }
        }

        Log.v("cook", "BS start successed");
    }

    private String readMessage(String filePath) {
        try {
            FileInputStream stream = new FileInputStream(filePath);
            int len = stream.available();
            byte[] buffer = new byte[len];

            stream.read(buffer);
            stream.close();

            return EncodingUtils.getString(buffer, "utf-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void startScheduleTimer(String date) {
        mScheduletimer = new Timer(true);
        try {
            mScheduletimer.schedule(new TimerTask() {

                @Override
                public void run() {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            instance.recreate();
                        }
                    });
                }
            }, PROGRAM_SCHEDULE_DATE_FORMAT.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void startAdvertising() {
        /* parser the config.xml and show */
        // GetConfig configs = new GetConfig(getApplicationContext());
        // if (configs.configInit(getApplicationContext()) == false) {
        // return;
        // }

        // DisplayContext display = configs.display;
        Program display = dataStruct.getCurrentProgram(programIndex);
        if (display == null)
            return;
        Policy policy = display.getPolicy();

        if (policy == Policy.TIME) {
            startScheduleTimer(display.getScheduleTime());
        }

        if (display.viewList.size() == 0)
            setBackground();

        Common cur_object;

        for (int i = 0; i < display.viewList.size(); i++) {
            cur_object = display.viewList.get(i);
            if (cur_object.viewType == ViewType.BG) {
                ImageView imageView = new ImageView(this);
                imageView
                        .setImageURI(Uri.parse(display.viewList.get(i).filePath));
                RelativeLayout rl = (RelativeLayout) findViewById(R.id.ad_ctl);
                rl.setBackground(imageView.getDrawable());
            } else if (cur_object.viewType == ViewType.VIDEO) {
                Common video = display.viewList.get(i);
                boolean using_a20 = true;

                for (int j = 0; j < video.videoPlayList.size(); j++) {
                    if (video.videoPlayList.get(j).startsWith("rtsp://")
                            || video.videoPlayList.get(j).startsWith("rtmp://")
                            || video.videoPlayList.get(j).startsWith("http://")) {
                        using_a20 = false;
                        break;
                    }
                }

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        video.placeInfo.right, video.placeInfo.buttom);
                mBsqVideoView.setLayoutParams(params);
                mBsqVideoView.setVisibility(View.VISIBLE);
                mBsqVideoView.setLayout(video.placeInfo.left,
                        video.placeInfo.top, video.placeInfo.right,
                        video.placeInfo.buttom);
                mBsqVideoView.videoPlayList = video.videoPlayList;
                if (mBsqVideoView.videoPlayList.size() > 0) {
                    if (systemInfo.getSyncType().equals("0"))
                        mBsqVideoView.playVideoList();
                    else if (systemInfo.getSyncType().equals("1")) {
                        // master
                        mBsqVideoView.enableSync(BsqVideoView.UDP_MASTER, systemInfo.getGroupId());
                        mBsqVideoView.playVideoList();
                    } else if (systemInfo.getSyncType().equals("2")) {
                        // slave
                        mBsqVideoView.enableSync(BsqVideoView.UDP_SLAVE, systemInfo.getGroupId());
                    }
                }


                //				if (using_a20) {
                //					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                //							video.placeInfo.right, video.placeInfo.buttom);
                //					a20VideoPlayer.setLayoutParams(params);
                //					a20VideoPlayer.setVisibility(View.VISIBLE);
                //					a20VideoPlayer.setX(video.placeInfo.left);
                //					a20VideoPlayer.setY(video.placeInfo.top);
                //					a20VideoPlayer.videoPlayList = video.videoPlayList;
                //					if (a20VideoPlayer.videoPlayList.size() > 0)
                //						a20VideoPlayer.playVideoList();
                //				} else {
                //					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                //							video.placeInfo.right, video.placeInfo.buttom);
                //					mBsqVideoView.setLayoutParams(params);
                //					mBsqVideoView.setVisibility(View.VISIBLE);
                //					mBsqVideoView.setLayout(video.placeInfo.left,
                //							video.placeInfo.top, video.placeInfo.right,
                //							video.placeInfo.buttom);
                //					mBsqVideoView.videoPlayList = video.videoPlayList;
                //					if (mBsqVideoView.videoPlayList.size() > 0) {
                //						if (systemInfo.getSyncType().equals("0"))
                //							mBsqVideoView.playVideoList();
                //						else if (systemInfo.getSyncType().equals("1")) {
                //							// master
                //							mBsqVideoView.enableSync(BsqVideoView.UDP_MASTER);
                //							mBsqVideoView.playVideoList();
                //						} else if (systemInfo.getSyncType().equals("2")) {
                //							// slave
                //							mBsqVideoView.enableSync(BsqVideoView.UDP_SLAVE);
                //						}
                //					}
                //				}

                if (policy == Policy.VIDEO) {
                    if (using_a20) {
                        a20VideoPlayer.addObserver(this);
                    } else {

                    }
                }
            } else if (cur_object.viewType == ViewType.IMAGE) {
                Common image = display.viewList.get(i);

                // player = new ImagePlayer(this);
                // player.initFrom(image);
                // RelativeLayout.LayoutParams params = new
                // RelativeLayout.LayoutParams(image.placeInfo.right,
                // image.placeInfo.buttom);
                // player.setX(image.placeInfo.left);
                // player.setY(image.placeInfo.top);
                // addContentView(player, params);

                FragmentManager fragmentManagerv4 = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManagerv4
                        .beginTransaction();
                ImageAreaFragment fragment = new ImageAreaFragment();
                fragment.setData(image);
                fragmentTransaction.add(R.id.ad_ctl, fragment);
                fragmentTransaction.commit();

                if (policy == Policy.IMAEG) {
                    fragment.addObserver(this);
                }
            } else if (cur_object.viewType == ViewType.TEXT) {
                Common text = display.viewList.get(i);
                MarqueeTextView marqueeTextView = new MarqueeTextView(
                        getApplicationContext(), null);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        text.placeInfo.right, text.placeInfo.buttom);
                addContentView(marqueeTextView, params);

                if (text.filePath == null)
                    continue;
                String text1 = readMessage(text.filePath);
                marqueeTextView.setX(text.placeInfo.left);
                marqueeTextView.setY(text.placeInfo.top);
                marqueeTextView.setGravity(Gravity.CENTER_VERTICAL);
                marqueeTextView.setText(text1);
                marqueeTextView
                        .setTypeface(Typeface.MONOSPACE, Typeface.NORMAL);
                marqueeTextView
                        .setTextSize(text.textSize / 100 * params.height);
                // marqueeTextView.getPaint().setTextSize(text.textSize / 100 *
                // params.height);
                // marqueeTextView.getPaint().setTextSize(text.textSize / 100 *
                // params.height);
                // marqueeTextView.getPaint().setColor(Color.parseColor(text.fgColorStr));
                marqueeTextView.setTextColor(Color.parseColor(text.fgColorStr));
                marqueeTextView.setBackgroundColor(Color
                        .parseColor(text.bgColorStr));
                if (text.speed.equals("1"))
                    marqueeTextView.setScrollSpeed(MarqueeTextView.SPEED_FAST);
                else if (text.speed.equals("8000"))
                    marqueeTextView
                            .setScrollSpeed(MarqueeTextView.SPEED_NORMAL);
                else if (text.speed.equals("30000"))
                    marqueeTextView.setScrollSpeed(MarqueeTextView.SPEED_SLOW);
                marqueeTextView.startScroll(Integer.parseInt(text.mode));

                // Common text = display.viewList.get(i);
                // RelativeLayout.LayoutParams params = new
                // RelativeLayout.LayoutParams(text.placeInfo.right,
                // text.placeInfo.buttom);
                //
                // MarqueeTextFragment fragment = (MarqueeTextFragment)
                // getFragmentManager().findFragmentById(R.id.fragment_marquee);
                //
                // fragment.getView().setLayoutParams(params);
                // fragment.getView().setX(text.placeInfo.left);
                // fragment.getView().setY(text.placeInfo.top);
            } else if (cur_object.viewType == ViewType.CLOCK) {
                Common clock = display.viewList.get(i);

                ClockView clockView = new ClockView(this);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        clock.placeInfo.right, clock.placeInfo.buttom);
                addContentView(clockView, params);

                clockView.setX(clock.placeInfo.left);
                clockView.setY(clock.placeInfo.top);
                clockView.setTypeface(Typeface.SERIF, Typeface.NORMAL);
                clockView.getPaint().setTextSize(
                        clock.textSize / 100 * params.height);
                clockView
                        .setBackgroundColor(Color.parseColor(clock.bgColorStr));
                clockView.getPaint().setColor(
                        Color.parseColor(clock.fgColorStr));
                clockView.setTextColor(Color.parseColor(clock.fgColorStr));
                clockView.setGravity(0x11);
            } else if (cur_object.viewType == ViewType.DATE) {
                Common date = display.viewList.get(i);

                DateView dateView = new DateView(this);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        date.placeInfo.right, date.placeInfo.buttom);
                addContentView(dateView, params);
                dateView.setX(date.placeInfo.left);
                dateView.setY(date.placeInfo.top);
                dateView.setTypeface(Typeface.SERIF, Typeface.NORMAL);
                dateView.getPaint().setTextSize(
                        date.textSize / 100 * params.height);
                dateView.setBackgroundColor(Color.parseColor(date.bgColorStr));
                dateView.getPaint().setColor(Color.parseColor(date.fgColorStr));
                dateView.setTextColor(Color.parseColor(date.fgColorStr));
                dateView.setGravity(0x11);
            } else if (cur_object.viewType == ViewType.WEATHER) {
                Common weather = display.viewList.get(i);

                fragmentWeather.setWeatherLocation(weather.cityName);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        weather.placeInfo.right, weather.placeInfo.buttom);
                fragmentWeather.getView().setVisibility(View.VISIBLE);
                fragmentWeather.getView().setX(weather.placeInfo.left);
                fragmentWeather.getView().setY(weather.placeInfo.top);
                fragmentWeather.getView().setLayoutParams(params);
                fragmentWeather.setTextSize(weather.textSize / 100
                        * params.height / 2);
                fragmentWeather.getView().setBackgroundColor(
                        Color.parseColor(weather.bgColorStr));
                fragmentWeather.setTextColor(Color
                        .parseColor(weather.fgColorStr));
                fragmentWeather.setWeatherLocation(weather.cityName);
            } else if (cur_object.viewType == ViewType.BROWSER) {
                Common browser = display.viewList.get(i);

                BSQBrowser browserview = new BSQBrowser(this);
                browserview.setDownloadListener(new DownloadListener() {

                    @Override
                    public void onDownloadStart(String url, String userAgent,
                                                String contentDisposition, String mimetype,
                                                long contentLength) {
                    }
                });
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        browser.placeInfo.right, browser.placeInfo.buttom);
                addContentView(browserview, params);
                browserview.setX(browser.placeInfo.left);
                browserview.setY(browser.placeInfo.top);
                browserview.loadUrl(browser.browser_url);
            }
        }
    }

    /**
     * Handler of incoming messages from service.
     */
    static class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BSQService.MSG_SET_VALUE:
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service. We are communicating with our
            // service through an IDL interface, so get a client-side
            // representation of that from the raw service object.
            mService = new Messenger(service);
            // mCallbackText.setText("Attached.");

            // We want to monitor the service for as long as we are
            // connected to it.
            try {
                Message msg = Message.obtain(null,
                        BSQService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

                // Give it some value as an example.
                msg = Message.obtain(null, BSQService.MSG_SET_VALUE,
                        this.hashCode(), 0);
                mService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even
                // do anything with it; we can count on soon being
                // disconnected (and then reconnected if it can be restarted)
                // so there is no need to do anything here.
            }

            // As part of the sample, tell the user what happened.
            // Toast.makeText(this, R.string.bsq_service_started,
            // Toast.LENGTH_SHORT).show();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
            // mCallbackText.setText("Disconnected.");

            // As part of the sample, tell the user what happened.
            // Toast.makeText(this, R.string.bsq_service_stopped,
            // Toast.LENGTH_SHORT).show();
        }
    };

    void doBindService() {
        // Establish a connection with the service. We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        bindService(new Intent(this, BSQService.class), mConnection,
                Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null,
                            BSQService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                }
            }

            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            if (mSystemUiHider != null)
                mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    public static String getSdcardPath() {
        return sdcardPath;
    }

    void updateExternalStorageState() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
        handleExternalStorageState(mExternalStorageAvailable,
                mExternalStorageWriteable);
    }

    private void handleExternalStorageState(boolean mExternalStorageAvailable2,
                                            boolean mExternalStorageWriteable2) {
        sdcardPath = "/storage/usbdisk";
        // sdcardPath = "/mnt/usb/sdb1";
        // sdcardPath = "/mnt/usb/sda1";
        // sdcardPath = "/mnt/usbdisk";
        Log.v("cook", "sdcardPath: " + sdcardPath);

        File file = new File(sdcardPath, "resource");
        if (!file.exists()) {
            if (file.mkdirs()) {
                Log.v("cook", "create " + file.getPath() + " successed");
            } else
                Log.v("cook", "create " + file.getPath() + " failed");
        } else {
            Log.v("cook", file.getPath() + " exists");
        }

        // using external sdcard as default
        if (mExternalStorageAvailable2 && mExternalStorageWriteable2) {
            if (Dev_MountInfo.getInstance() != null) {
                if (Dev_MountInfo.getInstance().getExternalInfo() != null)
                    sdcardPath = Dev_MountInfo.getInstance().getExternalInfo()
                            .getPath();
                else if (Dev_MountInfo.getInstance().getInternalInfo() != null)
                    sdcardPath = Dev_MountInfo.getInstance().getInternalInfo()
                            .getPath();
            } else
                sdcardPath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath();

            // sdcardPath = "/storage/usbdisk";
            // sdcardPath = "/mnt/usb/sdb1";
            // sdcardPath = "/mnt/usb/sda1";
            //			sdcardPath = "/mnt/usbdisk";
            Log.v("cook", "sdcardPath: " + sdcardPath);

            // File file = new File(sdcardPath, "resource");
            // if (!file.exists()) {
            // if (file.mkdirs()) {
            // Log.v("cook", "create " + file.getPath() + " successed");
            // } else
            // Log.v("cook", "create " + file.getPath() + " failed");
            // } else {
            // Log.v("cook", file.getPath() + " exists");
            // }
        }
    }

    void startWatchingExternalStorage() {
        mExternalStorageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Log.i("test", "Storage: " + intent.getData());
                updateExternalStorageState();
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        registerReceiver(mExternalStorageReceiver, filter);
        updateExternalStorageState();
    }

    void stopWatchingExternalStorage() {
        if (mExternalStorageReceiver != null)
            unregisterReceiver(mExternalStorageReceiver);
    }

    /**
     * @return the ftpClient
     */
    public FTPClient getFtpClient() {
        return ftpClient;
    }

    /**
     * @param ftpClient the ftpClient to set
     */
    public void setFtpClient(FTPClient ftpClient) {
        this.ftpClient = ftpClient;
        this.ftpClient.addObserver(this);
    }

    /**
     * @return the usingCS
     */
    public static boolean isUsingCS() {
        return usingCS;
    }

    /**
     * @param usingCS the usingCS to set
     */
    public static void setUsingCS(boolean usingCS) {
        AdvertiseController.usingCS = usingCS;
    }

    @Override
    public void update(Observable observable, Object data) {
        Log.d(TAG, "it's time to schedule to next program");
        if (data != null) {
            NotifyData notifyData = (NotifyData) data;
            if (notifyData.getSource() == SOURCE.PROGRAM) {
                programIndex = 0;
            }
        }

        instance.recreate();
    }
}
