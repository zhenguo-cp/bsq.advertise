package com.bsq.advertise;

import android.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

public class MarqueeText extends TextView {

	private float mTextWidth;
	private boolean isMeasured;

	public MarqueeText(Context context) {
		super(context);

		initialize();
	}

	public MarqueeText(Context context, AttributeSet attrs) {
		super(context, attrs);

		initialize();
	}

	public MarqueeText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		initialize();
	}

	private void initialize() {
		isMeasured = false;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (!isMeasured) {
			Paint paint = getPaint();
			String content = getText().toString();
			mTextWidth = paint.measureText(content);
			isMeasured = true;
			start();
		}
	}

	public float getTextWidth() {
		return mTextWidth;
	}

	private void start() {
		Animation a = new TranslateAnimation(getResources().getDisplayMetrics().widthPixels, -getTextWidth(), 0.0f, 0.0f);
		a.setDuration((int) getTextWidth() * 15);
		a.setRepeatMode(Animation.RESTART);
		a.setRepeatCount(Animation.INFINITE);
		a.setInterpolator(getContext(), R.interpolator.accelerate_cubic);
		startAnimation(a);
	}
}
