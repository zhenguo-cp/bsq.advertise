package com.bsq.advertise;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.bsq.schedule.Program;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class GetConfig {
	private int WIDTH, HEIGHT;
	private File resource, config;
	public Program display;
	public ArrayList<Program> displayContexts;
	private Context AppContext;

	public GetConfig(Context context) {
		context = AppContext;
	}

	public boolean configInit(Context context) {
		String sdcardPath = AdvertiseController.getSdcardPath();
		if (sdcardPath == null)
			return false;
		resource = new File(sdcardPath + "/resource");
		if (!resource.exists())
			return false;
		config = new File(resource + "/config.xml");
		if (!config.exists())
			return false;

		WIDTH = context.getResources().getDisplayMetrics().widthPixels;
		HEIGHT = context.getResources().getDisplayMetrics().heightPixels;

		displayContexts = new ArrayList<Program>();
		display = new Program();

		try {
			BufferedInputStream buf = new BufferedInputStream(new FileInputStream(config.getPath()));
			xml_parser(buf, null);
		} catch (FileNotFoundException e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
			return false;
		} catch (XmlPullParserException e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean xml_parser(InputStream arg0, String arg1) throws XmlPullParserException, IOException, NumberFormatException  {
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		xpp.setInput(arg0, arg1);
		int eventType = xpp.getEventType();

		int image_positon = 0;
		int text_positon = 0;

		while (eventType != XmlPullParser.END_DOCUMENT) {
			if (eventType == XmlPullParser.START_DOCUMENT) {

			} else if (eventType == XmlPullParser.START_TAG) {
				if (xpp.getName().equals(new String("program"))) {

				} else if (xpp.getName().equals(new String("time"))) {
					String start = xpp.getAttributeValue(0);
					String end = xpp.getAttributeValue(1);
					Log.v("cook", "start: " + start);
					Log.v("cook", "end: " + end);
					int onHours = Integer.parseInt(start.split(":")[0]);
					int onMinutes = Integer.parseInt(start.split(":")[1]);
					int offHours = Integer.parseInt(end.split(":")[0]);
					int offMinutes = Integer.parseInt(end.split(":")[1]);
					Log.v("cook", "onHours: " + onHours);
					Log.v("cook", "onMinutes: " + onMinutes);
					Log.v("cook", "offHours: " + offHours);
					Log.v("cook", "offMinutes: " + offMinutes);
					onHours = onHours - Calendar.HOUR_OF_DAY;
					offHours = offHours - Calendar.HOUR_OF_DAY;
//					BSQPowerManager.enable_watchdog();
//					BSQPowerManager.feed_watchdog();
//					BSQPowerManager.setPowerOnOff((byte) (onHours * 60), (byte) onMinutes, (byte) offHours, (byte) offMinutes, (byte) 3);
				} else if (xpp.getName().equals("template")) {
					// We should read content from template 
					eventType = xpp.nextTag();
					while (true) {
						if (xpp.getName().equals("bg")) {
							Common bg = new Common();
							bg.viewType = ViewType.BG;
							bg.filePath = resource.getPath() + "/" + xpp.getAttributeValue(0);
							display.viewList.add(bg);

							eventType = xpp.nextTag(); // should return END_TAG
						} else if (xpp.getName().equals("video")) {
							Double x = Double.valueOf(xpp.getAttributeValue(0));
							x = x / 100 * WIDTH;
							Double y = Double.valueOf(xpp.getAttributeValue(1));
							y = y / 100 * HEIGHT;
							Double w = Double.valueOf(xpp.getAttributeValue(2));
							w = w / 100 * WIDTH;
							Double h = Double.valueOf(xpp.getAttributeValue(3));
							h = h / 100 * HEIGHT;

							Common video = new Common();
							video.viewType = ViewType.VIDEO;
							video.placeInfo.left = x.floatValue();
							video.placeInfo.top = y.floatValue();
							video.placeInfo.right = w.intValue();
							video.placeInfo.buttom = h.intValue();
							display.viewList.add(video);

							eventType = xpp.nextTag(); // should return END_TAG
						} else if (xpp.getName().equals("image")) {
							// take care of index
							Double x = Double.valueOf(xpp.getAttributeValue(0));
							//x = x / 100 * WIDTH;
							x = WIDTH * x / 100;
							Double y = Double.valueOf(xpp.getAttributeValue(1));
							y = y / 100 * HEIGHT;
							Double w = Double.valueOf(xpp.getAttributeValue(2));
							w = w / 100 * WIDTH;
							Double h = Double.valueOf(xpp.getAttributeValue(3));
							h = h / 100 * HEIGHT;

							Common image = new Common();
							image.viewType = ViewType.IMAGE;
							image.placeInfo.left = x.floatValue();
							image.placeInfo.top = y.floatValue();
							image.placeInfo.right = w.intValue();
							image.placeInfo.buttom = h.intValue();
							image.placeInfo.position = image_positon;
							image_positon++;
							display.viewList.add(image);

							eventType = xpp.nextTag(); // should return END_TAG
						} else if (xpp.getName().equals("text")) {
							// take care of index
							Double x = Double.valueOf(xpp.getAttributeValue(0));
							//x = x / 100 * WIDTH;
							x = WIDTH * x / 100;
							Double y = Double.valueOf(xpp.getAttributeValue(1));
							y = y / 100 * HEIGHT;
							Double w = Double.valueOf(xpp.getAttributeValue(2));
							w = w / 100 * WIDTH;
							Double h = Double.valueOf(xpp.getAttributeValue(3));
							h = h / 100 * HEIGHT;

							Common text = new Common();
							text.viewType = ViewType.TEXT;
							text.placeInfo.left = x.floatValue();
							text.placeInfo.top = y.floatValue();
							text.placeInfo.right = w.intValue();
							text.placeInfo.buttom = h.intValue();
							text.placeInfo.position = text_positon;
							text_positon++;
							display.viewList.add(text);

							eventType = xpp.nextTag(); // should return END_TAG
						} else if (xpp.getName().equals("clock")) {
							Double x = Double.valueOf(xpp.getAttributeValue(0));
							//x = x / 100 * WIDTH;
							x = WIDTH * x / 100;
							Double y = Double.valueOf(xpp.getAttributeValue(1));
							y = y / 100 * HEIGHT;
							Double w = Double.valueOf(xpp.getAttributeValue(2));
							w = w / 100 * WIDTH;
							Double h = Double.valueOf(xpp.getAttributeValue(3));
							h = h / 100 * HEIGHT;

							Common clock = new Common();
							clock.viewType = ViewType.CLOCK;
							clock.placeInfo.left = x.floatValue();
							clock.placeInfo.top = y.floatValue();
							clock.placeInfo.right = w.intValue();
							clock.placeInfo.buttom = h.intValue();
							display.viewList.add(clock);

							eventType = xpp.nextTag(); // should return END_TAG
						} else if (xpp.getName().equals("date")) {
							Double x = Double.valueOf(xpp.getAttributeValue(0));
							//x = x / 100 * WIDTH;
							x = WIDTH * x / 100;
							Double y = Double.valueOf(xpp.getAttributeValue(1));
							y = y / 100 * HEIGHT;
							Double w = Double.valueOf(xpp.getAttributeValue(2));
							w = w / 100 * WIDTH;
							Double h = Double.valueOf(xpp.getAttributeValue(3));
							h = h / 100 * HEIGHT;

							Common date = new Common();
							date.viewType = ViewType.DATE;
							date.placeInfo.left = x.floatValue();
							date.placeInfo.top = y.floatValue();
							date.placeInfo.right = w.intValue();
							date.placeInfo.buttom = h.intValue();
							display.viewList.add(date);

							eventType = xpp.nextTag(); // should return END_TAG
						} else if (xpp.getName().equals("weather")) {
							Double x = Double.valueOf(xpp.getAttributeValue(0));
							//x = x / 100 * WIDTH;
							x = WIDTH * x / 100;
							Double y = Double.valueOf(xpp.getAttributeValue(1));
							y = y / 100 * HEIGHT;
							Double w = Double.valueOf(xpp.getAttributeValue(2));
							w = w / 100 * WIDTH;
							Double h = Double.valueOf(xpp.getAttributeValue(3));
							h = h / 100 * HEIGHT;

							Common weather = new Common();
							weather.viewType = ViewType.WEATHER;
							weather.placeInfo.left = x.floatValue();
							weather.placeInfo.top = y.floatValue();
							weather.placeInfo.right = w.intValue();
							weather.placeInfo.buttom = h.intValue();
							display.viewList.add(weather);

							eventType = xpp.nextTag(); // should return END_TAG
						} else if (xpp.getName().equals("browser")) {
							Double x = Double.valueOf(xpp.getAttributeValue(0));
							//x = x / 100 * WIDTH;
							x = WIDTH * x / 100;
							Double y = Double.valueOf(xpp.getAttributeValue(1));
							y = y / 100 * HEIGHT;
							Double w = Double.valueOf(xpp.getAttributeValue(2));
							w = w / 100 * WIDTH;
							Double h = Double.valueOf(xpp.getAttributeValue(3));
							h = h / 100 * HEIGHT;

							Common browser = new Common();
							browser.viewType = ViewType.BROWSER;
							browser.placeInfo.left = x.floatValue();
							browser.placeInfo.top = y.floatValue();
							browser.placeInfo.right = w.intValue();
							browser.placeInfo.buttom = h.intValue();
							display.viewList.add(browser);

							eventType = xpp.nextTag(); // should return END_TAG
						} else if (eventType == XmlPullParser.END_TAG && xpp.getName().equals(new String("template")) ) {
							break;
						}

						eventType = xpp.nextTag();
					}
				} else if (xpp.getName().equals("playlist")) {
					Common cur;
					eventType = xpp.nextTag();
					while (true) {
						if (xpp.getName().equals("video")) {
							String file_path = null;
							if (xpp.getAttributeName(0).equals("stream"))
								file_path = new String(xpp.getAttributeValue(0));
							else
								file_path = new String(resource.getPath() + "/" + xpp.getAttributeValue(0));
							for (int i = 0; i < display.viewList.size(); i++) {
								cur = display.viewList.get(i);
								if (cur.viewType == ViewType.VIDEO) {
									cur.videoPlayList.add(file_path);
									break;
								}
							}
							eventType = xpp.nextTag();
						} else if (xpp.getName().equals("image")) {
							// take care of the index
							int index = Integer.parseInt(xpp.getAttributeValue(0), 10);

							for (int i = 0; i < display.viewList.size(); i++) {
								cur = display.viewList.get(i);
								if (cur.viewType == ViewType.IMAGE 
										&& cur.placeInfo.position == index) {
									cur.imagePlayList.add(resource.getPath() + "/" + xpp.getAttributeValue(1));
									cur.animList.add(xpp.getAttributeValue(2));
									cur.intervalList.add(xpp.getAttributeValue(3).toString()) ;
									break;
								}
							}

							eventType = xpp.nextTag();
						} else if (eventType == XmlPullParser.END_TAG && xpp.getName().equals("playlist")) {
							break;
						} else if (xpp.getName().equals("text")) {
							int index = Integer.parseInt(xpp.getAttributeValue(1), 10);
							for (int i = 0; i < display.viewList.size(); i++) {
								cur = display.viewList.get(i);
								if (cur.viewType == ViewType.TEXT
										&& cur.placeInfo.position == index) {
									cur.filePath = resource.getPath() + "/" + xpp.getAttributeValue(0); 
									cur.fgColorStr = xpp.getAttributeValue(2).toString();
									cur.bgColorStr = xpp.getAttributeValue(3).toString();
									cur.textSize = Integer.parseInt(xpp.getAttributeValue(4).toString(), 10);
									cur.mode = xpp.getAttributeValue(5);
									cur.speed = xpp.getAttributeValue(6);
									cur.font = xpp.getAttributeValue(7);

									break;
								}
							}
						} else if (xpp.getName().equals("clock")) {
							for (int i = 0; i < display.viewList.size(); i++) {
								cur = display.viewList.get(i);
								if (cur.viewType == ViewType.CLOCK) {
									cur.textSize = Integer.parseInt(xpp.getAttributeValue(0), 10);
									cur.fgColorStr = xpp.getAttributeValue(1).toString();
									cur.bgColorStr = xpp.getAttributeValue(2).toString();
									break; 
								}
							}
						} else if (xpp.getName().equals("date")) {
							for (int i = 0; i < display.viewList.size(); i++) {
								cur = display.viewList.get(i);
								if (cur.viewType == ViewType.DATE) {
									cur.textSize = Integer.parseInt(xpp.getAttributeValue(0), 10);
									cur.fgColorStr = xpp.getAttributeValue(1).toString();
									cur.bgColorStr = xpp.getAttributeValue(2).toString();
									break;
								}
							}
						} else if (xpp.getName().equals("weather")) {
							for (int i = 0; i < display.viewList.size(); i++) {
								cur = display.viewList.get(i);
								if (cur.viewType == ViewType.WEATHER) {
									cur.language = xpp.getAttributeValue(0);
									cur.textSize = Integer.parseInt(xpp.getAttributeValue(1), 10);
									cur.interval = xpp.getAttributeValue(2);
									cur.fgColorStr = xpp.getAttributeValue(3);
									cur.bgColorStr = xpp.getAttributeValue(4);
									cur.cityName = xpp.getAttributeValue(5);
									break;
								}
							}
						} else if (xpp.getName().equals("browser")) {
							for (int i = 0; i < display.viewList.size(); i++) {
								cur = display.viewList.get(i);
								if (cur.viewType == ViewType.BROWSER) {
									cur.browser_url = xpp.getAttributeValue(0);
									break;
								}
							}
						}

						eventType = xpp.nextTag();
					}
				}

			} else if (eventType == XmlPullParser.END_TAG) {

			} else if (eventType == XmlPullParser.TEXT) {

			}

			eventType = xpp.next();
		}

		return true;
	}
}
