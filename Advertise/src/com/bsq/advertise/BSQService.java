package com.bsq.advertise;

import java.util.ArrayList;

import com.bsq.bs.HttpProcess;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class BSQService extends Service {
	/** For showing and hiding our notification. */
	NotificationManager mNM;
	/** Keeps track of all current registered clients. */
	static ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	/** Holds last value set by a client. */
	static int mValue = 0;

	/* BSQNet  */
	public BSQNet bsqNet = new BSQNet();
	private HttpProcess httpProcess = new HttpProcess();

	/**
	 * Command to the service to register a client, receiving callbacks
	 * from the service.  The Message's replyTo field must be a Messenger of
	 * the client where callbacks should be sent.
	 */
	static final int MSG_REGISTER_CLIENT = 1;

	/**
	 * Command to the service to unregister a client, ot stop receiving callbacks
	 * from the service.  The Message's replyTo field must be a Messenger of
	 * the client as previously given with MSG_REGISTER_CLIENT.
	 */
	static final int MSG_UNREGISTER_CLIENT = 2;

	/**
	 * Command to service to set a new value.  This can be sent to the
	 * service to supply a new value, and will be sent by the service to
	 * any registered clients with the new value.
	 */
	static final int MSG_SET_VALUE = 3;

	/**
	 * Handler of incoming messages from clients.
	 */
	public static class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				mClients.add(msg.replyTo);
				break;
			case MSG_UNREGISTER_CLIENT:
				mClients.remove(msg.replyTo);
				break;
			case MSG_SET_VALUE:
				mValue = msg.arg1;
				for (int i=mClients.size()-1; i>=0; i--) {
					try {
						mClients.get(i).send(Message.obtain(null,
								MSG_SET_VALUE, mValue, 0));
					} catch (RemoteException e) {
						// The client is dead.  Remove it from the list;
						// we are going through the list from back to front
						// so this is safe to do inside the loop.
						mClients.remove(i);
					}
				}
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	/**
	 * Target we publish for clients to send messages to IncomingHandler.
	 */
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	@Override
	public void onCreate() {
		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		// initalize the network
		new Thread(udpRunnable) {
		}.start();

		new Thread(tcpRunnable) {
		}.start();

		new Thread(httpRequestRunnable){
		}.start();

		// Display a notification about us starting.
		showNotification();        
	}

	@Override
	public void onDestroy() {
		// Cancel the persistent notification.

		bsqNet.UDP_Destory();
		bsqNet.TCP_Destory();
		httpProcess.stopHttpProcess();

		// Tell the user we stopped.
		//      Toast.makeText(this, R.string.bsq_service_stopped, Toast.LENGTH_SHORT).show();

	}

	/**
	 * When binding to the service, we return an interface to our messenger
	 * for sending messages to the service.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	Runnable udpRunnable = new Runnable() {

		@Override
		public void run() {
			/* handle the command */
			bsqNet.UDP_Creation();
		}
	};

	Runnable tcpRunnable = new Runnable() {

		@Override
		public void run() {
			/* handle the command */
			bsqNet.TCP_Creation();
		}
	};

	Runnable httpRequestRunnable = new Runnable() {

		@Override
		public void run() {
			httpProcess.startHttpProcess();	
		}
	};

	/**
	 * Show a notification while this service is running.
	 */
	private void showNotification() {
		// In this sample, we'll use the same text for the ticker and the expanded notification
		//        CharSequence text = getText(R.string.bsq_service_started);

		// Set the icon, scrolling text and timestamp
		//        Notification notification = new Notification(R.drawable.ic_launcher, text,
		//                System.currentTimeMillis());

		// The PendingIntent to launch our activity if the user selects this notification
		//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
		//                new Intent(this, Controller.class), 0);
		//
		//        // Set the info for the views that show in the notification panel.
		//        notification.setLatestEventInfo(this, getText(R.string.remote_service_label),
		//                       text, contentIntent);

		// Send the notification.
		// We use a string id because it is a unique number.  We use it later to cancel.
		//        mNM.notify(R.string.bsq_service_started, notification);
	}
}
