package com.bsq.advertise;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class FragmentWeather extends Fragment implements Handler.Callback {
	private Handler handler;
	private Timer timer = new Timer();
	private TimerTask task;
	private long update_period = 1000 * 60 * 30; 
	private static final int MSG_SUCCESS = 0;
	private static final int MSG_FAILURE = 1;
	private static final int MSG_HTML_BITMAP = 2;
	private static final int MSG_HTML_CC = 3;
	private static final int MSG_LOCATION = 4;

	private ImageView weather_gif;
	private TextView weather_location;
	private TextView weather_cc;
	private static String location = "����";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		
		//start();
	}
		
	public void start() {
		handler = new Handler(this);

		task = new TimerTask() {

			@Override
			public void run() {
				new Thread(runnable) {
				}.start();
			}
		};

		timer.schedule(task, 0, getWeatherUpdateInterval());
	}

	@Override
	public void onDestroy() {
		timer.cancel();
		super.onDestroy();
	}

	public void setWeatherUpdateInterval(long period) {
		update_period = period;
	}

	private long getWeatherUpdateInterval() {
		return update_period;
	}

	public void setWeatherLocation(String l) {
		location = l;
		Log.v("cook", "weather location: " + location);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_weather, container);
	}

	Runnable runnable = new Runnable() {

		@Override
		public void run() {
			Log.v("cook", "update weather info");
			URL url;
			try {
				final InputStream in;
				String city = code2CityName(location);
				if (city == null)
					city = "0008";
				url = new URL("http://xml.weather.yahoo.com/forecastrss?p=CHXX" + city);
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setReadTimeout(5 * 1000);
				urlConnection.setRequestMethod("GET");
				in = new BufferedInputStream(urlConnection.getInputStream());

				getCDATASection(in);
			} catch (MalformedURLException e) {
				Log.v("cook", e.getLocalizedMessage());
				handler.obtainMessage(MSG_FAILURE).sendToTarget();
				e.printStackTrace();
			} catch (ProtocolException e) {
				Log.v("cook", e.getLocalizedMessage());
				handler.obtainMessage(MSG_FAILURE).sendToTarget();
				e.printStackTrace();
			} catch (IOException e) {
				//				Log.v("cook", e.getLocalizedMessage());
				handler.obtainMessage(MSG_FAILURE).sendToTarget();
				e.printStackTrace();
			} 			
		}
	};

	private String fToC(int f) {
		return String.valueOf(Math.round((f - 32) / 1.8));
	}

	private String code2String(String code) {
		switch (Integer.parseInt(code)) {
		case 0:
			return "����";
		case 1:
			return "�ȴ�籩";
		case 2:
			return "����";
		case 3:
			return "������";
		case 4:
			return "������";
		case 5:
			return "���ѩ";
		case 6:
			return "��б�";
		case 7:
			return "ѩ�б�";
		case 8:
			return "������";
		case 9:
			return "ϸ��";
		case 10:
			return "����";
		case 11:
			return "����";
		case 12:
			return "����";
		case 13:
			return "��ѩ";
		case 14:
			return "С��ѩ";
		case 15:
			return "�ߴ�ѩ";
		case 16:
			return "ѩ";
		case 17:
			return "��";
		case 18:
			return "����";
		case 19:
			return "�۳�";
		case 20:
			return "��";
		case 21:
			return "����";
		case 22:
			return "����";
		case 23:
			return "���";
		case 24:
			return "��";
		case 25:
			return "��";
		case 26:
			return "��";
		case 27:
			return "����";
		case 28:
			return "����";
		case 29:
			return "�ֲ�����";
		case 30:
			return "�ֲ�����";
		case 31:
			return "��";
		case 32:
			return "��";
		case 33:
			return "ת��";
		case 34:
			return "ת��";
		case 35:
			return "��б�";
		case 36:
			return "��";
		case 37:
			return "�ֲ�����";
		case 38:
			return "ż������";
		case 39:
			return "ż������";
		case 40:
			return "ż������";
		case 41:
			return "��ѩ";
		case 42:
			return "������ѩ";
		case 43:
			return "��ѩ";
		case 44:
			return "�ֲ�����";
		case 45:
			return "������";
		case 46:
			return "��ѩ";
		case 47:
			return "�ֲ�������";
		default:
			return "ˮ�����";
		}
	}

	private Bitmap code2bmp(String code) {
		if (getActivity() == null)
			return null;
		switch (Integer.parseInt(code)) {
		case 0:
			return BitmapFactory.decodeResource(getResources(), R.drawable.shachenbao_0);
		case 1:
			return BitmapFactory.decodeResource(getResources(), R.drawable.shachenbao_0);
		case 2:
			return BitmapFactory.decodeResource(getResources(), R.drawable.shachenbao_0);
		case 3:
			return BitmapFactory.decodeResource(getResources(), R.drawable.dayu_0);
		case 4:
			return BitmapFactory.decodeResource(getResources(), R.drawable.leizhenyu_0);
		case 5:
			return BitmapFactory.decodeResource(getResources(), R.drawable.yujiaxue_0);
		case 6:
			return BitmapFactory.decodeResource(getResources(), R.drawable.bingbao_0);
		case 7:
			return BitmapFactory.decodeResource(getResources(), R.drawable.bingbao_0);
		case 8:
			return BitmapFactory.decodeResource(getResources(), R.drawable.dongyu_0);
		case 9:
			return BitmapFactory.decodeResource(getResources(), R.drawable.xiaoyu_0);
		case 10:
			return BitmapFactory.decodeResource(getResources(), R.drawable.xiaoyu_0);
		case 11:
			return BitmapFactory.decodeResource(getResources(), R.drawable.xiaoyu_0);
		case 12:
			return BitmapFactory.decodeResource(getResources(), R.drawable.xiaoyu_0);
		case 13:
			return BitmapFactory.decodeResource(getResources(), R.drawable.zhongxue_0);
		case 14:
			return BitmapFactory.decodeResource(getResources(), R.drawable.zhongxue_0);
		case 15:
			return BitmapFactory.decodeResource(getResources(), R.drawable.zhongxue_0);
		case 16:
			return BitmapFactory.decodeResource(getResources(), R.drawable.zhongxue_0);
		case 17:
			return BitmapFactory.decodeResource(getResources(), R.drawable.bingbao_0);
		case 18:
			return BitmapFactory.decodeResource(getResources(), R.drawable.dayu_0);
		case 19:
			return BitmapFactory.decodeResource(getResources(), R.drawable.shachenbao_0);
		case 20:
			return BitmapFactory.decodeResource(getResources(), R.drawable.wu_0);
		case 21:
			return BitmapFactory.decodeResource(getResources(), R.drawable.wu_0);
		case 22:
			return BitmapFactory.decodeResource(getResources(), R.drawable.wu_0);
		case 23:
			return BitmapFactory.decodeResource(getResources(), R.drawable.shachenbao_0);
		case 24:
			return BitmapFactory.decodeResource(getResources(), R.drawable.wu_0);
		case 25:
			return BitmapFactory.decodeResource(getResources(), R.drawable.shuangdong_0);
		case 26:
			return BitmapFactory.decodeResource(getResources(), R.drawable.yin_0);
		case 27:
			return BitmapFactory.decodeResource(getResources(), R.drawable.yan_0);
		case 28:
			return BitmapFactory.decodeResource(getResources(), R.drawable.wu_0);
		case 29:
			return BitmapFactory.decodeResource(getResources(), R.drawable.wu_0);
		case 30:
			return BitmapFactory.decodeResource(getResources(), R.drawable.wu_0);
		case 31:
			return BitmapFactory.decodeResource(getResources(), R.drawable.qing_0);
		case 32:
			return BitmapFactory.decodeResource(getResources(), R.drawable.qing_0);
		case 33:
			return BitmapFactory.decodeResource(getResources(), R.drawable.qing_0);
		case 34:
			return BitmapFactory.decodeResource(getResources(), R.drawable.qing_0);
		case 35:
			return BitmapFactory.decodeResource(getResources(), R.drawable.bingbao_0);
		case 36:
			return BitmapFactory.decodeResource(getResources(), R.drawable.qing_0);
		case 37:
			return BitmapFactory.decodeResource(getResources(), R.drawable.dayu_0);
		case 38:
			return BitmapFactory.decodeResource(getResources(), R.drawable.dayu_0);
		case 39:
			return BitmapFactory.decodeResource(getResources(), R.drawable.dayu_0);
		case 40:
			return BitmapFactory.decodeResource(getResources(), R.drawable.dayu_0);
		case 41:
			return BitmapFactory.decodeResource(getResources(), R.drawable.daxue_0);
		case 42:
			return BitmapFactory.decodeResource(getResources(), R.drawable.dayu_0);
		case 43:
			return BitmapFactory.decodeResource(getResources(), R.drawable.dayu_0);
		case 44:
			return BitmapFactory.decodeResource(getResources(), R.drawable.duoyun_0);
		case 45:
			return BitmapFactory.decodeResource(getResources(), R.drawable.leizhenyu_0);
		case 46:
			return BitmapFactory.decodeResource(getResources(), R.drawable.zhongxue_0);
		case 47:
			return BitmapFactory.decodeResource(getResources(), R.drawable.leizhenyu_0);
		default:
			return BitmapFactory.decodeResource(getResources(), R.drawable.mai_0);
		}
	}

	protected String code2CityName(String location2) {
		LinkedHashMap<String, String> hashMap = new LinkedHashMap<String, String>();
		hashMap.put("����", "0008");
		hashMap.put("���", "0133");
		hashMap.put("����", "0044");
		hashMap.put("�Ϸ�", "0448");
		hashMap.put("�Ϻ�", "0116");
		hashMap.put("����", "0031");
		hashMap.put("����", "0017");
		hashMap.put("�ϲ�", "0097");
		hashMap.put("���", "0049");
		hashMap.put("����", "0064");
		hashMap.put("����", "0512");
		hashMap.put("֣��", "0165");
		hashMap.put("���ͺ���", "0249");
		hashMap.put("��³ľ��", "0135");
		hashMap.put("��ɳ", "0013");
		hashMap.put("��", "0259");
		hashMap.put("����", "0037");
		hashMap.put("����", "0080");
		hashMap.put("����", "0502");
		hashMap.put("����", "0100");
		hashMap.put("�ɶ�", "0016");
		hashMap.put("ʯ��ׯ", "0122");
		hashMap.put("����", "0039");
		hashMap.put("̫ԭ", "0129");
		hashMap.put("����", "0076");
		hashMap.put("����", "0119");
		hashMap.put("����", "0141");
		hashMap.put("����", "0010");
		hashMap.put("����", "0079");
		hashMap.put("����", "0236");
		hashMap.put("�Ͼ�", "0099");
		hashMap.put("����", "0120");


		return hashMap.get(location2);
	}

	private String getCDATASection(InputStream in) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		DocumentBuilder builder;
		try {
			/* get the DocumentBuilder object */
			builder = factory.newDocumentBuilder();

			/* Document represents the entire xml context */
			org.w3c.dom.Document document = builder.parse(in);

			/* The entry of the document */
			org.w3c.dom.Element root = document.getDocumentElement();

			/* parse the location and send to target */
			handler.obtainMessage(MSG_LOCATION, location + "��").sendToTarget();

			/* parse the cc and send to target */
			org.w3c.dom.NodeList condition = root.getElementsByTagName("yweather:condition");
			org.w3c.dom.Element cc = (org.w3c.dom.Element) condition.item(0);
			String weather_pattern = code2String(cc.getAttribute("code")) + " " + fToC(Integer.parseInt(cc.getAttribute("temp"))) + " ��";
			handler.obtainMessage(MSG_HTML_CC, weather_pattern).sendToTarget();

			/* get the node by name description */
			//			org.w3c.dom.NodeList nodeList = root.getElementsByTagName("description");

			//			org.w3c.dom.Element element = (org.w3c.dom.Element) nodeList.item(1);
			//			String cdataSection = element.getTextContent();

			/* parser the img src and send to target */
			//			Document doc = Jsoup.parse(cdataSection);
			//			Element link = doc.select("img").first();
			//			String img_src = link.attr("src");

			//			HttpClient client = new DefaultHttpClient();
			//			HttpGet get = new HttpGet(img_src);
			//			final Bitmap bitmap;
			//			HttpResponse httpResponse = client.execute(get);
			//			bitmap = BitmapFactory.decodeStream(httpResponse.getEntity().getContent());
			//			handler.obtainMessage(MSG_HTML_BITMAP, bitmap).sendToTarget();

			Bitmap bitmap = code2bmp(cc.getAttribute("code"));
			if (bitmap == null)
				return null;
			handler.obtainMessage(MSG_HTML_BITMAP, bitmap).sendToTarget();
			handler.obtainMessage(MSG_SUCCESS).sendToTarget();

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public void setTextColor(int color) {
		weather_location = (TextView) getView().findViewById(R.id.weather_location);
		weather_cc = (TextView) getView().findViewById(R.id.weather_cc);

		weather_location.setTextColor(color);
		weather_cc.setTextColor(color);
	}

	public void setTextSize(float size) {
		weather_location = (TextView) getView().findViewById(R.id.weather_location);
		weather_cc = (TextView) getView().findViewById(R.id.weather_cc);

		weather_location.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
		weather_location.setTextSize(size);

		weather_cc.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
		weather_cc.setTextSize(size);
	}

	@Override
	public boolean handleMessage(Message msg) {
		if (getActivity() == null)
			return false;
		switch (msg.what) {
		case MSG_SUCCESS:
			break;
		case MSG_FAILURE:
			break;
		case MSG_HTML_BITMAP:
			BitmapDrawable  bitmapDrawable = new BitmapDrawable(getResources(), (Bitmap) msg.obj);
			weather_gif = (ImageView) getView().findViewById(R.id.weather_gif);
			weather_gif.setImageDrawable(bitmapDrawable);
			break;
		case MSG_LOCATION:
			weather_location = (TextView) getView().findViewById(R.id.weather_location);
			weather_location.setText((String) msg.obj);
		case MSG_HTML_CC:
			weather_cc = (TextView) getView().findViewById(R.id.weather_cc);
			weather_cc.setText((String) msg.obj);
			break;
		}

		return true;
	}
}
