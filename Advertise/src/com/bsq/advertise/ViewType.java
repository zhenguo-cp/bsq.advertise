package com.bsq.advertise;

public enum ViewType {
	BG, VIDEO, IMAGE, TEXT, CLOCK, DATE, WEATHER, BROWSER;
}
