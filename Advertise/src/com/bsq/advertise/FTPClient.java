package com.bsq.advertise;

import it.sauronsoftware.ftp4j.FTPAbortedException;
import it.sauronsoftware.ftp4j.FTPDataTransferException;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.bsq.advertise.NotifyData.SOURCE;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class FTPClient extends it.sauronsoftware.ftp4j.FTPClient implements FTPDataTransferListener {
	private static FTPClient instance;
	private String host;
	private int port;
	private String usrinfo;
	private String usr;
	private String pwd;
	private String fileName;
	public boolean resources_downloaded;
	public boolean download_done;
	public boolean upgrade;
	public boolean download_imcomplete;
	public int resources_total_length;
	private boolean cleanup_current_downloading = false;

	private final static String sdCardPath = AdvertiseController.getSdcardPath() + File.separator;
	private final static String resourcePath = sdCardPath + File.separator + "resource" + File.separator;
	private final static String config = resourcePath + "config_new.xml";
	public static final String PREFS_NAME = "BSQPolicy";
	public final static int MSG_RECREATE = 0;

	private String[] resourceFileNameList;
	private long[] resourceFileTotalList;
	private long[] resourceFileCompletedSizeList;
	private int resourceFileCount;
	public int index; // file index that currently transfered
	private boolean notifyServer;
	private NewProgramObservable mObservable;
	
	public class NewProgramObservable extends Observable {
		public void ProgramExpired() {
			setChanged();
		}
	}

	public FTPClient() {
		setPassive(true);
		resources_downloaded = false;
		download_done = false;
		upgrade = true;
		instance = this;
		index = 0;
		notifyServer = false;

		if (new File(config).exists()) {
			download_imcomplete = true;
			// TODO 处理未完成下载的节目单
		}
		
		mObservable = new NewProgramObservable();
	}

	public static FTPClient getInstance() { // should init class first
		if (instance != null)
			return instance;
		else
			return null;
	}

	public void addObserver(Observer observer) {
		mObservable.addObserver(observer);
	}
	
	private void download3times(String remoteFileName, File localFile, FTPDataTransferListener listener) {
		if (cleanup_current_downloading) {
			Log.v("cook", "New mission, abort.");
			return;
		}

		if (!isConnected()) {
			BSQNet.instance().TCP_reply("Can't connect to FTP server");
			return;
		}

		int retry_times = 3;
		boolean download_failed = false;

		String Parent = localFile.getParent();
		if (Parent != null) {
			new File(Parent).mkdirs();
		}

		while (true) {
			try {
				download(remoteFileName, localFile, listener);
				Runtime.getRuntime().exec("sync");
				download_failed = false;
			} catch (IllegalStateException e) {
				download_failed = true;
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				Log.v("cook", "File not found on FTP");
				download_failed = true;
				e.printStackTrace();
			} catch (IOException e) {
				download_failed = true;
				e.printStackTrace();
			} catch (FTPIllegalReplyException e) {
				download_failed = true;
				e.printStackTrace();
			} catch (FTPException e) {
				download_failed = true;
				e.printStackTrace();
			} catch (FTPDataTransferException e) {
				download_failed = true;
				e.printStackTrace();
			} catch (FTPAbortedException e) {
				download_failed = true;
				e.printStackTrace();
			}

			if (download_failed == false)
				break;
			retry_times--;
			if (retry_times == 0) {
				Log.v("cook", "Something failed on downloading...");
				upgrade = false;
				break;
			}
		}		
	}

	public String downloadAPK(String info) {
		try {
			URL url = new URL(info);
			host = url.getHost();
			port = url.getPort();
			usrinfo = url.getUserInfo();
			if (usrinfo != null) {
				usr = usrinfo.substring(0, usrinfo.indexOf(":"));
				pwd = usrinfo.substring(usrinfo.indexOf(":"), usrinfo.length());
			}
			fileName = url.getFile();

			Log.v("cook", "host: " + host);
			Log.v("cook", "port: " + port);
			Log.v("cook", "usr: " + usrinfo);
			Log.v("cook", "pwd: " + pwd);

			if (isConnected()) {
				Log.v("cook", "FTP connection is already established, skip the connection progress");
			} else {

				try {
					if (port != -1)
						connect(host, port);
					else
						connect(host);

					if (usr != null && pwd != null) {
						login(usr, pwd);
					} else 
						login("anonymous", "bsq");
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (FTPIllegalReplyException e) {
					e.printStackTrace();
				} catch (FTPException e) {
					e.printStackTrace();
				} finally {

				}
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		String apkfile = sdCardPath + "Advertise.apk";
		File file = new File(apkfile);
		if (file.exists())
			file.delete();

		download3times(fileName, file, null);

		try {
			disconnect(true);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FTPIllegalReplyException e) {
			e.printStackTrace();
		} catch (FTPException e) {
			e.printStackTrace();
		}

		return apkfile;
	}

	public void abortDownloading() {
		BSQNet.instance().TCP_reply("Download aborted");
		try {
			abortCurrentDataTransfer(true);
			cleanup_current_downloading = true;
			while (true) {
				if (!cleanup_current_downloading)
					break; // cleanup done
				Thread.sleep(1000);
				abortCurrentDataTransfer(true);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FTPIllegalReplyException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void startProcess(String info) { // info is from tcp data, only called from jni call
		try {
			URL url = new URL(info);
			host = url.getHost();
			port = url.getPort();
			usrinfo = url.getUserInfo();
			if (usrinfo != null) {
				usr = usrinfo.substring(0, usrinfo.indexOf(":"));
				pwd = usrinfo.substring(usrinfo.indexOf(":"), usrinfo.length());
			}
			fileName = url.getFile();

			Log.v("cook", "host: " + host);
			Log.v("cook", "port: " + port);
			Log.v("cook", "usr: " + usrinfo);
			Log.v("cook", "pwd: " + pwd);

			if (isConnected()) {
				Log.v("cook", "FTP connection is already established, skip the connection progress");
			} else {

				try {
					if (port != -1)
						connect(host, port);
					else
						connect(host);

					if (usr != null && pwd != null) {
						login(usr, pwd);
					} else 
						login("anonymous", "bsq");
				} finally {

				}
			}

			File res_check = new File(resourcePath);
			if (!res_check.exists()) 
				if (!res_check.mkdir()) {
					BSQNet.instance().TCP_reply("create " + resourcePath + " Failed");
					return;
				}

			SharedPreferences settings = AdvertiseController.instance().getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_MULTI_PROCESS);
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean("usingCS", true);
			AdvertiseController.setUsingCS(true);
			editor.commit();

			Log.v("cook", "Set play CS as default");

			download3times(fileName, new File(config), null);

			index = 0;
			downloadResources(config);
			Log.v("cook", "logout");
			//			logout(); // May case exception, because of some ftp server didn't implement this method
			Log.v("cook", "disconnect");
			disconnect(true);
			Log.v("cook", "Cleanup done");
			
			mObservable.ProgramExpired();
			NotifyData notifyData = new NotifyData();
			notifyData.setSource(SOURCE.PROGRAM);
			mObservable.notifyObservers();
		} catch (IllegalStateException e) {
			e.printStackTrace();
			Log.v("cook", "" + "" + e.getLocalizedMessage());
		} catch (IOException e) {
			e.printStackTrace();
			Log.v("cook", "" + "" + e.getLocalizedMessage());
		} catch (FTPIllegalReplyException e) {
			e.printStackTrace();
			Log.v("cook", "" + "" + e.getLocalizedMessage());
		} catch (FTPException e) {
			e.printStackTrace();
			Log.v("cook", "" + "" + e.getLocalizedMessage());
		} catch (FTPDataTransferException e) {
			e.printStackTrace();
			Log.v("cook", "" + "" + e.getLocalizedMessage());
		} catch (FTPAbortedException e) {
			e.printStackTrace();
			Log.v("cook", "" + "" + e.getLocalizedMessage());
		}
	}

	public void stopProcess() {

	}

	public void downloadResources(String filePath) throws FTPDataTransferException, FTPAbortedException {
		File input = new File(filePath);

		try {
			Document document = Jsoup.parse(input, "UTF-8");
			Elements elements = document.getElementsByAttribute("file");
			Elements bg = document.getElementsByAttribute("picture");

			setResourceFileCount(elements.size() + bg.size());

			Log.v("cook", "resource file count: " + getResourceFileCount());

			resourceFileNameList = new String[getResourceFileCount()];
			resourceFileTotalList = new long[getResourceFileCount()];
			resourceFileCompletedSizeList = new long[getResourceFileCount()];

			for (int i = 0; i < elements.size(); i++) {
				resourceFileNameList[i] = elements.get(i).attr("file");
				try {
					resourceFileTotalList[i] = fileSize(elements.get(i).attr("file"));
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (FTPIllegalReplyException e) {
					e.printStackTrace();
				} catch (FTPException e) {
					e.printStackTrace();
				}
			}

			if (bg.size() != 0) {
				resourceFileNameList[getResourceFileCount() - 1] = bg.get(0).attr("picture");
				try {
					resourceFileTotalList[getResourceFileCount() - 1] = fileSize(bg.get(0).attr("picture"));
				} catch (IllegalStateException e1) {
					e1.printStackTrace();
				} catch (FTPIllegalReplyException e1) {
					e1.printStackTrace();
				} catch (FTPException e1) {
					e1.printStackTrace();
				}
			}

			for (Element element : elements) {
				Log.v("cook", "file: " + element.attr("file"));
				try {
					File currentFile = new File(resourcePath + element.attr("file"));
					if (!currentFile.exists()) {
						resources_downloaded = true;
						Log.v("cook", "index: " + index);
						download3times(element.attr("file"), currentFile, this);
						index++;
					} else {
						long sizeCmp = fileSize(element.attr("file")) - new File(resourcePath, element.attr("file")).length();
						if (sizeCmp > 0) {
							// If the file size on ftp larger than the file on sdcard, we put an command of REST to the FTP server
							long restartAt = new File(resourcePath, element.attr("file")).length(); 
							if (isResumeSupported()) {
								resources_downloaded = true;
								download(element.attr("file"), currentFile, restartAt, this);
								resourceFileCompletedSizeList[index] += restartAt;
								index++;
							}
						} else if (sizeCmp < 0) {
							resources_downloaded = true;
							download3times(element.attr("file"), currentFile, this);
							index++;
						} else {
							Log.v("cook", element.attr("file") + " is exists");
							resourceFileCompletedSizeList[index] = resourceFileTotalList[index];
							index++;
						}
					}
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (FTPIllegalReplyException e) {
					e.printStackTrace();
				} catch (FTPException e) {
					e.printStackTrace();
				} catch (FTPDataTransferException e) {
					e.printStackTrace();
				} catch (FTPAbortedException e) {
					e.printStackTrace();
				}
			}

			for (Element element : bg) {
				Log.v("cook", "FileName: " + element.attr("picture"));
				try {
					File currentFile = new File(resourcePath + element.attr("picture"));
					if (!currentFile.exists()) {
						resources_downloaded = true;
						Log.v("cook", "index: " + index);
						download3times(element.attr("picture"), currentFile, this);
						index++;
					} else {
						Log.v("cook", element.attr("picture") + " is exists");
						resourceFileCompletedSizeList[index] = resourceFileTotalList[index];
						index++;
					}
				} catch (IllegalStateException e) {
					e.printStackTrace();
				}
			}

			try {
				download_done = true;
				download3times("config.xml", new File(resourcePath, ".bsq"), this);
				cleanup_current_downloading = false;
				Log.v("cook", "download done");
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void started() {
		if (resources_downloaded && ! notifyServer)
			BSQNet.instance().TCP_reply("Download started"); // after this, the end socket is closed
		notifyServer = true;
	}

	@Override
	public void transferred(int length) {
		if (!download_done)
			resourceFileCompletedSizeList[index] += length;
	}

	@Override
	public void completed() {
		if (download_done && resources_downloaded && upgrade) {
			download_done = false;
			AdvertiseController.setUsingCS(true);
			File file = new File(config);
			if (file.exists())
				file.renameTo(new File(resourcePath + "config.xml"));
			BSQNet.instance().TCP_reply("Download completed");
			AdvertiseController.ftpMSG.obtainMessage(MSG_RECREATE).sendToTarget();
		} else if (download_done && !resources_downloaded && upgrade) {
			// nothing was downloaded
			File file = new File(config);
			if (file.exists())
				file.renameTo(new File(resourcePath + "config.xml"));
			BSQNet.instance().TCP_reply("Download already completed");
			AdvertiseController.ftpMSG.obtainMessage(MSG_RECREATE).sendToTarget();
		} else if (!upgrade) {
			// Downloading is failed
			upgrade = true;
			BSQNet.instance().TCP_reply("Download failed");
		}
	}

	@Override
	public void aborted() {
		Log.v("cook", "Download aborted");
	}

	@Override
	public void failed() {
		Log.v("cook", "Download failed");
	}

	/**
	 * @return the resourceFileCount
	 */
	public int getResourceFileCount() {
		return resourceFileCount;
	}

	/**
	 * @param resourceFileCount the resourceFileCount to set
	 */
	public void setResourceFileCount(int resourceFileCount) {
		this.resourceFileCount = resourceFileCount;
	}

	public String[] getResourceFileNameList() {
		return resourceFileNameList;
	}

	public long[] getResourceFileTotalList() {
		return resourceFileTotalList;
	}

	public long[] getResourceFileCompletedSizeList() {
		return resourceFileCompletedSizeList;
	}
}
