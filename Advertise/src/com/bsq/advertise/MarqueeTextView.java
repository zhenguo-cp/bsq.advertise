package com.bsq.advertise;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

public class MarqueeTextView extends TextView implements Runnable {
	private int contentWidth = 0;
	private int scrollToX = 0;
	private int scrollToY = 0;
	private boolean isMeasureContentWidth = false;
	public final static int SPEED_FAST = 4;
	public final static int SPEED_NORMAL = 3;
	public final static int SPEED_SLOW = 2;
	private int scrollSpeed = SPEED_NORMAL;
	private int scrollMode = 0;

	public MarqueeTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setSingleLine(true);
		setSelected(true);
		setFocusable(true);
		setLayerType(View.LAYER_TYPE_HARDWARE, null);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if (!isMeasureContentWidth) {
			Paint paint = this.getPaint();
			String content = this.getText().toString();
			contentWidth = (int) paint.measureText(content);
			isMeasureContentWidth = true;
		}
	}

	public void run() {
		postInvalidate();
		switch (scrollMode) {
		case 0: // static 
			break;
		case 1: // right2left
			if (scrollToX >= contentWidth)
				scrollToX = -getWidth();
			scrollTo(scrollToX, 0);
			scrollToX += getScrollSpeed();
			break;
		case 2: // static center
			break;
		case 3: // up2top
			if (scrollToY >= contentWidth) 
				scrollToX = -getWidth();
			scrollTo(0, scrollToY);
			scrollToY += getScrollSpeed();
			break;
		case 4: // form 
			break;
		}
		
		post(this);
	}

	public void startScroll(int mode) {
		scrollMode = mode;

		switch (mode) {
		case 0: // static 
			scrollToX = 0;
			break;
		case 1: // right2left
			scrollToX = 0;
			break;
		case 2: // static center
			scrollToX = 0;
			break;
		case 3: // up2top
			scrollToY = 0;
			break;
		case 4: // form
			break;
		}
		postDelayed(this, 2000);
	}

	/**
	 * @return the scrollSpeed
	 */
	public int getScrollSpeed() {
		return scrollSpeed;
	}

	/**
	 * @param scrollSpeed the scrollSpeed to set
	 */
	public void setScrollSpeed(int scrollSpeed) {
		this.scrollSpeed = scrollSpeed;
	}
}
