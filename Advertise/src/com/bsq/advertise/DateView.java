package com.bsq.advertise;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;

public class DateView extends TextView {

	Calendar mCalendar;
	private final static String df = "yyyy-MM-dd E"; 
	private Runnable mTicker;
	private Handler mHandler;
	private FormatChangeObserver mFormatChangeObserver;

	private boolean mTickerStopped = false;

	public DateView(Context context) {
		super(context);
		initDate(context);
	}

	public DateView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initDate(context);
	}

	public DateView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initDate(context);
	}

	private void initDate(Context context) {

		if (mCalendar == null) {
			mCalendar = Calendar.getInstance();
		}

		mFormatChangeObserver = new FormatChangeObserver();
		getContext().getContentResolver().registerContentObserver(
				Settings.System.CONTENT_URI, true, mFormatChangeObserver);
		
		setGravity(Gravity.CENTER);
	}

	@Override
	protected void onAttachedToWindow() {
		Log.v("cook", "onAttachedToWindow()");
		mTickerStopped = false;
		super.onAttachedToWindow();
		mHandler = new Handler();

		/**
		 * requests a tick on the next hard-second boundary
		 */
		mTicker = new Runnable() {
			public void run() {
				if (mTickerStopped) return;
				mCalendar.setTimeInMillis(System.currentTimeMillis());
				SimpleDateFormat format = new SimpleDateFormat(df, Locale.CHINESE);
				format.setCalendar(mCalendar);
				//setText(format.format(new Date(0)));
				setText(DateFormat.format(df, mCalendar));
				invalidate();
				long now = SystemClock.uptimeMillis();
				long next = now + (1000 - now % 1000);
				mHandler.postAtTime(mTicker, next);
			}
		};
		mTicker.run();
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		mTickerStopped = true;
	}

	private class FormatChangeObserver extends ContentObserver {
		public FormatChangeObserver() {
			super(new Handler());
		}

		@Override
		public void onChange(boolean selfChange) {

		}
	}

}
