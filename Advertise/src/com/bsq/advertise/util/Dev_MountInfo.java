package com.bsq.advertise.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.bsq.advertise.util.Dev_MountInfo.DevInfo;

import android.os.Environment;

public class Dev_MountInfo implements IDev {
	/**
	 * ***
	 */
	public final String HEAD = " dev_mount";
	public final String LABEL = "<label>";
	public final String MOUNT_POINT = "<mount_point>";
	public final String PATH = "<part>";
	public final String SYSFS_PATH = "<sysfs_path1...>";

	/**
	 * Label for the volume
	 */
	private final int NLABEL = 2;
	/**
	 * Partition
	 */
	private final int NPATH = 3;
	/**
	 * Where the volume will be mounted
	 */
	private final int NMOUNT_POINT = 4;
	private final int NSYSFS_PATH = 5;

	private final int DEV_INTERNAL = 0;
	private final int DEV_EXTERNAL = 1;

	private ArrayList<String> cache = new ArrayList<String>();

	private static Dev_MountInfo dev;
	private DevInfo info;

	private final File VOLD_FSTAB = new File(Environment.getRootDirectory()
			.getAbsoluteFile()
			+ File.separator
			+ "etc"
			+ File.separator
			+ "vold.fstab");
	
	public static Dev_MountInfo getInstance() {
		if (null == dev)
			dev = new Dev_MountInfo();
		if (!dev.VOLD_FSTAB.exists())
			return null;
		return dev;
	}

	private DevInfo getInfo(final int device) {

		if (null == info)
			info = new DevInfo();

		try {
			initVoldFstabToCache();
		} catch (IOException e) {
			e.printStackTrace();
		}

//		if (device == 0) 
//			Log.v("cook", "get internal");
//		else 
//			Log.v("cook", "get_external");
		if (device >= cache.size())
			return null;
		String[] sinfo = cache.get(device).split("[ 	]"); // black board
		
//		Log.v("cook", "sysinfo size: " + sinfo.length);
		if (sinfo.length == 1) {
			sinfo = cache.get(device).split("\t");

//			for (int i = 0; i < sinfo.length; i++) {
//				Log.v("cook", "sinfo[" + i + "]: " + sinfo[i]);
//			}
			info.setLabel(sinfo[NLABEL - 1]);
			info.setMount_point(sinfo[NMOUNT_POINT - 1]);
			info.setPath(sinfo[NPATH - 1]);
			info.setSysfs_path(sinfo[NSYSFS_PATH - 1]);
		} else {
//			for (int i = 0; i < sinfo.length; i++) {
//				Log.v("cook", "sinfo[" + i + "]: " + sinfo[i]);
//			}
			info.setLabel(sinfo[NLABEL - 1]);
			info.setMount_point(sinfo[NMOUNT_POINT - 1]);
			info.setPath(sinfo[NPATH - 1]);
			info.setSysfs_path(sinfo[NSYSFS_PATH - 1]);
		}

		return info;
	}

	private void initVoldFstabToCache() throws IOException {
		cache.clear();
		BufferedReader br = new BufferedReader(new FileReader(VOLD_FSTAB));
		String tmp = null;
//		int index = 0;
		while (true) {
			tmp = br.readLine();
			if (tmp == null)
				break;
//			Log.v("cook", tmp + ": " + index++);
			if (tmp.startsWith(HEAD) && (tmp.contains("sdcard") || tmp.contains("extsd"))) {
				cache.add(tmp);
			} else if (tmp.startsWith("dev_mount") && (tmp.contains("sdcard") || tmp.contains("extsd"))) {
				cache.add(tmp);
			}
		}
		
		br.close();
//		Log.v("cook", "cache size: " + cache.size());
		cache.trimToSize();
	}

	public class DevInfo {
		private String label, mount_point, path, sysfs_path;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getMount_point() {
			return mount_point;
		}

		public void setMount_point(String mount_point) {
			this.mount_point = mount_point;
		}

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public String getSysfs_path() {
			return sysfs_path;
		}

		public void setSysfs_path(String sysfs_path) {
			this.sysfs_path = sysfs_path;
		}

	}

	@Override
	public DevInfo getInternalInfo() {
		return getInfo(DEV_INTERNAL);
	}

	@Override
	public DevInfo getExternalInfo() {
		return getInfo(DEV_EXTERNAL);
	}
}

interface IDev {
	DevInfo getInternalInfo();

	DevInfo getExternalInfo();
}
