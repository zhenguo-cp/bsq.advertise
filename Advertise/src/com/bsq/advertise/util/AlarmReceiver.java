package com.bsq.advertise.util;

import com.bsq.advertise.AdvertiseController;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context arg0, Intent arg1) {
		Log.v("cook", "PowerSchedule received");
		int action = arg1.getIntExtra("action", -1);

		switch (action) {
		case AdvertiseController.POWERSCHEDULEON:
			AdvertiseController.instance().rebootDevice();
			break;
		case AdvertiseController.POWERSCHEDULEOFF:
			AdvertiseController.instance().sleepDevice();
			break;
		default:
			break;
		}
	}

}
