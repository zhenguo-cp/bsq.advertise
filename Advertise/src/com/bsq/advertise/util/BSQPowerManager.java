package com.bsq.advertise.util;

import com.xboot.stdcall.posix;

import android.util.Log;

public class BSQPowerManager {

	public BSQPowerManager() {
		// TODO Auto-generated constructor stub
	}
	
	public static int setPowerOnOff(byte on_h, byte on_m, byte off_h, byte off_m, byte enable) {
		int fd,ret;

		fd = posix.open("/dev/McuCom", posix.O_RDWR, 0666);
		if (fd < 0) {
			Log.v("cook", "Set power on/off failed");
		}
		ret = posix.poweronoff(off_h, off_m, on_h, on_m, enable ,fd);
		if (ret != 0) {
			Log.v("cook", "Set power on/off failed");
		}
		posix.close(fd);       
		return 0;  	
	}

	public static int enable_watchdog() {
		int fd,ret;
		fd = posix.open("/dev/McuCom", posix.O_RDWR, 0666);
		if (fd < 0) {
			Log.v("cook", "Failed to open watchdog");
		}
		ret = posix.watchdogenable((byte)1, fd);
		if (ret != 0) {
			Log.v("cook", "Failed to open watchdog");
		}    
		posix.close(fd);	 
		return 0;
	}

	public static int disable_watchdog() {
		int fd,ret;
		fd = posix.open("/dev/McuCom",posix.O_RDWR, 0666);
		if (fd < 0) {
			Log.v("cook", "Failed to close watchdog");
		}
		ret = posix.watchdogenable((byte)0, fd);
		if (ret != 0) {
			Log.v("cook", "Failed to close watchdog");
		}       
		posix.close(fd);	
		return 0;
	}

	public static int feed_watchdog() {
		int fd,ret;
		fd = posix.open("/dev/McuCom",posix.O_RDWR, 0666);
		if (fd < 0)  {
			Log.v("cook", "Failed to feed watchdog");
		}
		ret = posix.watchdogfeed(fd);
		if (ret != 0) {
			Log.v("cook", "Failed to feed watchdog");
		}       
		posix.close(fd);	
		return 0;
	}
}
