package com.bsq.advertise.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import org.apache.http.conn.util.InetAddressUtils;
import com.bsq.advertise.AdvertiseController;
import com.xzw.jni.PWMJni;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.util.Log;

public class SystemInfo {
	public static SystemInfo instance = null;
	private static Context appContext;
	private final static String sdcardPath = AdvertiseController.getSdcardPath();
	private final static String configPath = sdcardPath + "/bsq.cfg";
	private final static String screenShotPath = sdcardPath + "/screenShot.png";
	private final static String screenShotPathJPEG = sdcardPath + "/screen.jpg";
	private static final DateFormat MDTM_DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.CHINESE);
	private static final DateFormat MDTM_DATE_FORMAT_DIFF = new SimpleDateFormat("HH:mm:ss", Locale.US);
	private final static long G = 1024 * 1024 * 1024;
	private final static long M = 1024 * 1024;
	private final static long K = 1024;

	public final static int DEVICE_SHUTDOWN = 4;
	public final static int DEVICE_REBOOT = 3;
	public final static int DEVICE_SLEEP = 2;
	public final static int DEVICE_WAKEUP = 1;
	public static final String POWER_SCHEDULE = "power_schedule";

	private String name = null;
	private String position = null;
	private String httpServer = null;
	private String proxyServer = null;
	private String asp = null;
	private String weatherPos = null;
	private String ssid = null;
	private String password = null;
	private int is3g = 0;
	private String remoteControl = null;
	private String language = null;
	private String poweron = null;
	private String poweroff = null;
	private String port = null;
	private String ip = null;
	private String netmask = null;
	private String gateway = null;
	private String dns = null;
	private String syncType = null;
	private static int volume_fake = 0;
	private String tvModel = null;
	private String tvMark = null;
	private String tvProId = null;
	private String tvProName = null;
	private String groupId = null;

	private final static PWMJni pwmJni = new PWMJni();

	public SystemInfo(Context context) {
		appContext = context;
		instance = this;
		initConfig();
	}

	public static SystemInfo getInstance() {
		if (instance != null)
			return instance;
		else
			return null;
	}

	private void initConfig() {
		convertCode(configPath);
	}

	public void convertCode(String str_filepath) {//ת����� 

		File file = new File(str_filepath);
		if (!file.exists()) {
			writeConfig();
		}

		BufferedReader reader;  

		try {  

			FileInputStream fis = new FileInputStream(file);  
			BufferedInputStream in = new BufferedInputStream(fis);

			in.mark(4);  
			byte[] first3bytes = new byte[3];  
			in.read(first3bytes);  
			in.reset();

			if (first3bytes[0] == (byte) 0xEF && first3bytes[1] == (byte) 0xBB  
					&& first3bytes[2] == (byte) 0xBF) {// utf-8  
				reader = new BufferedReader(new InputStreamReader(in, "utf-8"));
			} else if (first3bytes[0] == (byte) 0xFF  
					&& first3bytes[1] == (byte) 0xFE) {  
				reader = new BufferedReader(new InputStreamReader(in, "unicode"));
			} else if (first3bytes[0] == (byte) 0xFE && first3bytes[1] == (byte) 0xFF) {  
				reader = new BufferedReader(new InputStreamReader(in,  "utf-16be"));
			} else if (first3bytes[0] == (byte) 0xFF  && first3bytes[1] == (byte) 0xFF) {  
				reader = new BufferedReader(new InputStreamReader(in,  "utf-16le"));
			} else {  
				reader = new BufferedReader(new InputStreamReader(in, "utf-8"));
			}

			String str = reader.readLine(); 

			while (str != null) {  
				if (str.contains("name=")) {
					name = str.substring(str.indexOf("=") + 1);
				} else if (str.contains("position=")) {
					position = str.substring(str.indexOf("=") + 1);
				} else if (str.contains("httpserver=")) {
					httpServer = str.substring(str.indexOf("=") + 1).replace('\n', '\0').trim();
				} else if (str.contains("proxyserver=")) {
					setProxyServer(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("asp=")) {
					setAsp(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("volume=")) {
					setVolume(Integer.parseInt(str.substring(str.indexOf("=") + 1)));
				} else if (str.contains("weatherpos=")) {
					setWeatherPos(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("ssid=")) {
					setSsid(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("password=")) {
					setPassword(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("3g=")) {
					setIs3g(Integer.parseInt(str.substring(str.indexOf("=") + 1)));
				} else if (str.contains("remotecontrol=")) {
					setRemoteControl(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("language=")) {
					setLanguage(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("poweron=")) {
					setPoweron(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("poweroff=")) {
					setPoweroff(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("port=")) {
					setPort(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("syncType=")) {
					setSyncType(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("tvModel=")) {
					setTvModel(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("tvMark=")) {
					setTvMark(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("tvProId=")) {
					setTvProId(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("tvProName=")) {
					setTvProName(str.substring(str.indexOf("=") + 1));
				} else if (str.contains("groupId")) {
					setGroupId(str.substring(str.indexOf("=") + 1));
				}

				str = reader.readLine(); 
			}

			reader.close();
			in.close();
			fis.close();
		} catch (FileNotFoundException e) {  
			e.printStackTrace();  
		} catch (IOException e) {  
			e.printStackTrace();  
		} 		
	}

	public static String getSdcardPath() {
		return sdcardPath;
	}

	public static void setStatus(int status) {
		switch (status) {
		case DEVICE_WAKEUP:
			AdvertiseController.instance().wakeUpDevice();
			pwmJni.SetPWMON();
			break;
		case DEVICE_SLEEP:
			AdvertiseController.instance().sleepDevice();
			pwmJni.SetPWMOFF();
			break;
		case DEVICE_REBOOT:
			AdvertiseController.instance().rebootDevice();
			break;
		case DEVICE_SHUTDOWN:
			AdvertiseController.instance().shutdownDevice();
			pwmJni.SetPWMOFF();
		default:
			break;
		}
	}

	public static long takeScreenShot() {
		try {
			Process process = Runtime.getRuntime().exec("screencap -p " + screenShotPath);
			process.waitFor();
			process.destroy();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		try {
			OutputStream os = new BufferedOutputStream(new FileOutputStream(screenShotPathJPEG));
			Bitmap bm = BitmapFactory.decodeFile(screenShotPath);
			bm = Bitmap.createScaledBitmap(bm, 480, 270, true);
			bm.compress(Bitmap.CompressFormat.JPEG, 100, os);
			os.flush();
			bm.recycle();
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new File(screenShotPathJPEG).length();
	}

	public void setName(String s) {
		name = s;
	}

	public void setPosition(String s) {
		position = s;
	}

	public static void setSystemTime(int year, int month, int day, int hourOfDay, int minute, int second) {
		Calendar set = Calendar.getInstance();
		set.clear();
		set.set(year, month - 1, day, hourOfDay, minute, second);
		SystemClock.setCurrentTimeMillis(set.getTimeInMillis());
	}

	public static String getSystemTime() {
		Calendar get = Calendar.getInstance();
		return MDTM_DATE_FORMAT.format(get.getTime());
	}

	public static String getDiskInfo() {
		String diskInfo = new String();
		long freeSpace, totalSpace;

		File file = new File(sdcardPath);
		freeSpace = file.getFreeSpace();
		totalSpace = file.getTotalSpace();
		DecimalFormat df = new DecimalFormat(".##");

		if (freeSpace > G) {
			diskInfo += df.format((freeSpace * 1.00) / (G * 1.00)) + "GB(free)";
		} else if (freeSpace > M) {
			diskInfo += df.format((freeSpace * 1.00) / (M * 1.00)) + "MB(free)";
		} else if (freeSpace > K) {
			diskInfo += df.format((freeSpace * 1.00) / (K * 1.00)) + "KB(free)";
		} else {
			diskInfo += df.format((freeSpace * 1.00)) + "B(free)";
		}

		if (totalSpace > G) {
			diskInfo += df.format((totalSpace * 1.00) / (G * 1.00)) + "GB(total)";
		} else if (totalSpace > M) {
			diskInfo += df.format((totalSpace * 1.00) / (M * 1.00)) + "MB(total)";
		} else if (totalSpace > K) {
			diskInfo += df.format((freeSpace * 1.00) / (K * 1.00)) + "KB(total)";
		} else {
			diskInfo += df.format((totalSpace * 1.00)) + "B(free)";
		}

		return diskInfo;
	}

	public void setInfoR(String info_name, String info_position) {
		setName(info_name);
		setPosition(info_position);
		writeConfig();
	}

	public void writeConfig() {
		try {
			FileOutputStream fos = new FileOutputStream(configPath);
			BufferedOutputStream out = new BufferedOutputStream(fos);

			out.write(new String("name=").getBytes("utf-8"));
			if (getName() != null)
				out.write(getName().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("position=").getBytes("utf-8"));
			if (getPosition() != null)
				out.write(getPosition().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("httpserver=").getBytes("utf-8"));
			if (getHttpAddress() != null)
				out.write(getHttpAddress().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("proxyserver=").getBytes("utf-8"));
			if (getProxyServer() != null)
				out.write(getProxyServer().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("asp=").getBytes("utf-8"));
			if (getAsp() != null)
				out.write(getAsp().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("volume=").getBytes("utf-8"));
			out.write(String.valueOf(getVolume()).getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("weatherpos=").getBytes("utf-8"));
			if (getWeatherPos() != null)
				out.write(getWeatherPos().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("ssid=").getBytes("utf-8"));
			if (getSsid() != null)
				out.write(getSsid().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("password=").getBytes("utf-8"));
			if (getPassword() != null)
				out.write(getPassword().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("3g=").getBytes("utf-8"));
			out.write(String.valueOf(getIs3g()).getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("remotecontrol=").getBytes("utf-8"));
			if (getRemoteControl() != null)
				out.write(getRemoteControl().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("language=").getBytes("utf-8"));
			if (getLanguage() != null)
				out.write(getLanguage().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("poweron=").getBytes("utf-8"));
			if (getPoweron() != null)
				out.write(getPoweron().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("poweroff=").getBytes("utf-8"));
			if (getPoweroff() != null)
				out.write(getPoweroff().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("port=").getBytes("utf-8"));
			if (getPort() != null)
				out.write(getPort().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("sync").getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("syncType=").getBytes("utf-8"));
			if (getSyncType() != null)
				out.write(getSyncType().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("tvModel=").getBytes("utf-8"));
			if (getTvModel() != null)
				out.write(getTvModel().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("tvMark=").getBytes("utf-8"));
			if (getTvMark() != null)
				out.write(getTvMark().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("tvProId=").getBytes("utf-8"));
			if (getTvProId() != null)
				out.write(getTvProId().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("tvProName=").getBytes("utf-8"));
			if (getTvProName() != null)
				out.write(getTvProName().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.write(new String("groupId=").getBytes("utf-8"));
			if (getTvProName() != null)
				out.write(getGroupId().getBytes("utf-8"));
			out.write(new String("\n").getBytes("utf-8"));

			out.flush();
			out.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean deleteFile(String fileName) {
		File file = new File(sdcardPath + File.separator + "resource", fileName);
		if (!file.exists() || file.isDirectory())
			return false;

		return file.delete();
	}

	public static String getFilesList() {
		String result = "";
		File file = new File(sdcardPath, "resource");
		if (!file.exists() || !file.isDirectory())
			return null;

		File list[] = file.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				if (pathname.getName().startsWith(".") || pathname.isDirectory())
					return false;
				return true;
			}
		});

		for (int i = 0; i < list.length; i++) {
			if (list[i].isDirectory())
				continue;
			result += list[i].getName();
			result += "<";
			result += list[i].length();
			result += ">";
			result += "|";
		}

		Log.v("cook", "FileList: " + result);
		return result;
	}

	public String getHttpAddress() {
		return httpServer;
	}

	public void setHttpServer(String http) {
		this.httpServer = http;
	}

	public static boolean isWifiConnected() {
		ConnectivityManager cm = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED ? true : false;
	}

	private static String byte2hex(byte[] b) {
		if (b == null)
			return null;

		StringBuffer buffer = new StringBuffer(b.length);
		int len = b.length;

		for (int i = 0; i < len; i++)
			buffer = buffer.append(String.format("%02x", b[i]));

		return String.valueOf(buffer);
	}

	public static String getHardwareAddress() throws SocketException {

		if (isWifiConnected()) {
			WifiManager wifiManager = (WifiManager) appContext.getSystemService(Context.WIFI_SERVICE);
			if (!wifiManager.isWifiEnabled()) 
				wifiManager.setWifiEnabled(true);

			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			return wifiInfo.getMacAddress();
		} else {
			for (Enumeration<NetworkInterface> nif = NetworkInterface.getNetworkInterfaces(); 
					nif.hasMoreElements(); ) {
				NetworkInterface cur = nif.nextElement();
				if (cur.getName().equals("eth0")) {
					return byte2hex(cur.getHardwareAddress());
				}
			}
		}

		return null;
	}

	public static String getHostAddress() throws IOException {
		if (isWifiConnected()) {
			WifiManager wifiManager = (WifiManager) appContext.getSystemService(Context.WIFI_SERVICE);
			if (!wifiManager.isWifiEnabled()) 
				wifiManager.setWifiEnabled(true);

			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int ipAddress = wifiInfo.getIpAddress();

			String ipAddressStr = null;

			if (ipAddress != 0) {
				ipAddressStr = ((ipAddress & 0xff) + "." + (ipAddress >> 8 & 0xff) + "."   
						+ (ipAddress >> 16 & 0xff) + "." + (ipAddress >> 24 & 0xff));
			}

			return ipAddressStr;

		} else {
			for (Enumeration<NetworkInterface> nif = NetworkInterface.getNetworkInterfaces(); 
					nif.hasMoreElements(); ) {
				NetworkInterface nif_cur = nif.nextElement();

				for (Enumeration<InetAddress> ia = nif_cur.getInetAddresses(); 
						ia.hasMoreElements();) {
					InetAddress ia_cur = ia.nextElement();
					if (!ia_cur.isLoopbackAddress() && InetAddressUtils.isIPv4Address(ia_cur.getHostAddress())) {
						return ia_cur.getHostAddress();
					}
				}
			}
		}

		return null;
	}

	public static String getVersion() {
		try {
			PackageManager packageManager = AdvertiseController.instance().getPackageManager();
			PackageInfo packageInfo = packageManager.getPackageInfo(AdvertiseController.instance().getPackageName(), 0);
			String version = packageInfo.versionName;
			return version;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		return null;
	}

	public String getName() {
		return name;
	}

	public String getPosition() {
		return position;
	}

	/**
	 * @return the proxyServer
	 */
	public String getProxyServer() {
		return proxyServer;
	}

	/**
	 * @param proxyServer the proxyServer to set
	 */
	public void setProxyServer(String proxyServer) {
		this.proxyServer = proxyServer;
	}

	/**
	 * @return the asp
	 */
	public String getAsp() {
		return asp;
	}

	/**
	 * @param asp the asp to set
	 */
	public void setAsp(String asp) {
		this.asp = asp;
	}

	/**
	 * @return the volume
	 */
	public static int getVolume() {
		//		AudioManager audioManager = (AudioManager) (appContext.getSystemService(Context.AUDIO_SERVICE));
		//		return (int) ((double) (audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)) / 
		//				(double) (audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC))
		//				* 100);
		return volume_fake;
	}

	/**
	 * @param volume the volume to set
	 */
	public static void setVolume(int volume) {
		volume_fake = volume;
		AudioManager audioManager = (AudioManager) (appContext.getSystemService(Context.AUDIO_SERVICE));		
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * volume / 100, AudioManager.FLAG_PLAY_SOUND);
	}

	/**
	 * @return the weatherPos
	 */
	public String getWeatherPos() {
		return weatherPos;
	}

	/**
	 * @param weatherPos the weatherPos to set
	 */
	public void setWeatherPos(String weatherPos) {
		this.weatherPos = weatherPos;
	}

	/**
	 * @return the ssid
	 */
	public String getSsid() {
		return ssid;
	}

	/**
	 * @param ssid the ssid to set
	 */
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the is3g
	 */
	public int getIs3g() {
		return is3g;
	}

	/**
	 * @param is3g the is3g to set
	 */
	public void setIs3g(int is3g) {
		this.is3g = is3g;
	}

	/**
	 * @return the remoteControl
	 */
	public String getRemoteControl() {
		return remoteControl;
	}

	/**
	 * @param remoteControl the remoteControl to set
	 */
	public void setRemoteControl(String remoteControl) {
		this.remoteControl = remoteControl;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	public void CancelPowerSchedule() {
		//		BSQPowerManager.disable_watchdog();
		//		BSQPowerManager.setPowerOnOff((byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0);
	}

	/**
	 * @return the poweron
	 */
	public String getPoweron() {
		return poweron;
	}

	/**
	 * @param poweron the poweron to set
	 */
	public void setPoweron(String poweron) {
		this.poweron = poweron;
	}

	/**
	 * @return the poweroff
	 */
	public String getPoweroff() {
		return poweroff;
	}

	/**
	 * @param poweroff the poweroff to set
	 */
	@SuppressLint("WorldWriteableFiles")
	public void setPoweroff(String poweroff) {
		this.poweroff = poweroff;

		if (poweroff.isEmpty() || this.poweron.isEmpty()) {
			Log.v("cook", "Cancel the schedule of power on/off");
			//			BSQPowerManager.disable_watchdog();
			//			BSQPowerManager.setPowerOnOff((byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0);
			return;
		}

		String timeOn = null;
		String timeOff = null;

		int onHours = 0;
		int onMinutes = 0;
		int offHours = 0;
		int offMinutes = 0;

		try {
			Date now = MDTM_DATE_FORMAT_DIFF.parse(Calendar.getInstance().getTime().toString().split(" ")[3]);
			Date time_on = MDTM_DATE_FORMAT_DIFF.parse(this.poweron);
			Date time_off = MDTM_DATE_FORMAT_DIFF.parse(this.poweroff);

			long on_dif = time_on.getTime() - now.getTime();
			long off_dif = time_off.getTime() - now.getTime();

			if (on_dif < 0) {
				on_dif = (24 - Calendar.HOUR_OF_DAY) * (1000 * 60 * 60) + Math.abs(on_dif);
			}

			if (off_dif < 0) {
				off_dif = (24 - Calendar.HOUR_OF_DAY) * (1000 * 60 * 60) + Math.abs(off_dif);
			}

			AdvertiseController.instance().setPowerOnSchedule(on_dif);
			AdvertiseController.instance().setPowerOffSchedule(off_dif);

			onHours = (int) (on_dif / (1000 * 60 * 60));
			onMinutes = (int) ((on_dif - onHours * (1000 * 60 * 60)) / (1000 * 60));
			offHours = (int) (off_dif / (1000 * 60 * 60));
			offMinutes = (int) ((off_dif -offHours * (1000 * 60 * 60)) / (1000 * 60));

		} catch (ParseException e) {
			Log.v("cook", "format: " + e.getLocalizedMessage());
			e.printStackTrace();
		}

		Log.v("cook", "scheduled poweron after: " + onHours + " Hours" + " " + onMinutes + " Minutes");
		Log.v("cook", "scheduled poweroff after: " + offHours + " Hours" + " " + offMinutes + " Minutes");

		SharedPreferences settings = appContext.getSharedPreferences(POWER_SCHEDULE, Context.MODE_WORLD_WRITEABLE);
		timeOn = settings.getString("poweron", "");
		timeOff = settings.getString("poweroff", "");

		Log.v("cook", "previous powerOn: " + timeOn);
		Log.v("cook", "previous powerOff: " + timeOff);

		if ((!timeOn.equals(this.poweron)) || (!timeOff.equals(this.poweroff))) {
			//			BSQPowerManager.enable_watchdog();
			//			BSQPowerManager.feed_watchdog();
			//			// enable poweronoff
			//			BSQPowerManager.setPowerOnOff((byte) (onHours * 60), (byte) onMinutes, (byte) offHours, (byte) offMinutes, (byte) 3);

			SharedPreferences.Editor editor = settings.edit();
			editor.putString("poweron", this.poweron);
			editor.putString("poweroff", this.poweroff);
			editor.commit();
			Log.v("cook", "update power schedule info");
		}
	}

	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the netmask
	 */
	public String getNetmask() {
		return netmask;
	}

	/**
	 * @param netmask the netmask to set
	 */
	public void setNetmask(String netmask) {
		this.netmask = netmask;
	}

	/**
	 * @return the gateway
	 */
	public String getGateway() {
		return gateway;
	}

	/**
	 * @param gateway the gateway to set
	 */
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	/**
	 * @return the dns
	 */
	public String getDns() {
		return dns;
	}

	/**
	 * @param dns the dns to set
	 */
	public void setDns(String dns) {
		this.dns = dns;
	}

	public String getSyncType() {
		return syncType;
	}

	public void setSyncType(String syncType) {
		this.syncType = syncType;
	}

	public String getTvModel() {
		return tvModel;
	}

	public void setTvModel(String tvModel) {
		this.tvModel = tvModel;
	}

	public String getTvMark() {
		return tvMark;
	}

	public void setTvMark(String tvMark) {
		this.tvMark = tvMark;
	}

	public String getTvProId() {
		return tvProId;
	}

	public void setTvProId(String tvProId) {
		this.tvProId = tvProId;
	}

	public String getTvProName() {
		return tvProName;
	}

	public void setTvProName(String tvProName) {
		this.tvProName = tvProName;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
}
