package com.bsq.advertise.util;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SelfUpdated extends BroadcastReceiver {

	private boolean isAdRunning = false;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.v("cook", "apk updated");
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = manager.getRunningTasks(1).get(0).topActivity;
		String currentPackageName = cn.getPackageName();
		if (currentPackageName != null && currentPackageName.equals("com.bsq.advertise"))
			isAdRunning = true;
		else
			isAdRunning = false;

		if (!isAdRunning) {
			Intent ad = context.getPackageManager().getLaunchIntentForPackage("com.bsq.advertise");
			if (ad != null)
				context.startActivity(ad);
		}
	}
}
