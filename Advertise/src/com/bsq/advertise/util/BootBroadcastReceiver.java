package com.bsq.advertise.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent ad = context.getPackageManager().getLaunchIntentForPackage("com.bsq.advertise");
		if (ad != null)
			context.startActivity(ad);
	}
}
