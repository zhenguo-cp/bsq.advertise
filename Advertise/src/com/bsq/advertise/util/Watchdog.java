package com.bsq.advertise.util;

import java.util.Timer;
import java.util.TimerTask;

import android.app.ActivityManager;
import android.app.IntentService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Watchdog extends IntentService {

	private final static Timer timer = new Timer(true);
	private boolean isAdRunning = false;

	public Watchdog() {
		super("Watchdog");
		setIntentRedelivery(true);
	}

	@Override
	public void onCreate() {
		super.onCreate();

		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				Log.v("cook", "Watchdog is running");
				ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
				ComponentName cn = manager.getRunningTasks(1).get(0).topActivity;
				String currentPackageName = cn.getPackageName();
				if (currentPackageName != null && currentPackageName.equals("com.bsq.advertise"))
					isAdRunning = true;
				else
					isAdRunning = false;

				if (!isAdRunning) {
					Intent ad = getPackageManager().getLaunchIntentForPackage("com.bsq.advertise");
					startActivity(ad);
				}

			}
		}, 0, 1000 * 60);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		timer.cancel();
		
		Log.v("cook", "Watchdog destroyed");
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		while (true) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}
	
	@Override
	public void onTaskRemoved(Intent rootIntent) {
		super.onTaskRemoved(rootIntent);
	}
}
