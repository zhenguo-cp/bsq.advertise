package com.bsq.udp;

/**
 * Created by macro on 15/4/17.
 */
public class Constants {
    public static final int PORT = 23867;
    public static final String BROADCAST_ACTION = "com.bsq.spoan.action.BROADCAST_ACTION";
    public static final String EXTENDED_DATA_STATUS = "extended_data_status";
}
