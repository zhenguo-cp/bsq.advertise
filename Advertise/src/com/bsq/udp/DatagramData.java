package com.bsq.udp;

/**
 * Created by macro on 15/4/16.
 */
public class DatagramData extends AbstractBaseObject {
    private int msec;
    private String fileName;
    private long ts;

    public void setMsec(int msec) {
        this.msec = msec;
    }

    public int getMsec() {
        return msec;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public String toString() {
        return "FileName: " + fileName + " msec: " + msec + " ts: " + ts;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }
}
