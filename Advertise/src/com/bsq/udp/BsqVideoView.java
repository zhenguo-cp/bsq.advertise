package com.bsq.udp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by macro on 15/4/22.
 */
public class BsqVideoView extends VideoView {
	private static final String TAG = BsqVideoView.class.getSimpleName();
	private int width, height;
	private Timer mTimer = new Timer("udp_master");
	public static final int UDP_MASTER = 0x1000;
	public static final int UDP_SLAVE = 0x1001;
	private boolean slaveInitialized = false;
	private final static int START_PLAYBACK = 0x1002;
	private final static int SLAVE_SYNC = 0x1003;
	private int fileIndex = 0;
	public ArrayList<String> videoPlayList;
	private int mRole;
	private long lastSuccess;
	private String mMasterFileName;
	private final static int EOF = -1;
	private final static int EXIT = -2;
	private final static String resourcePath = Environment.getExternalStorageDirectory().getPath() + "/resource/";
	private String mGroupId = null;

	private Handler mHandler = new Handler(new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case START_PLAYBACK:
				String fileName = ((String) msg.obj).substring(((String) msg.obj).lastIndexOf("/") + 1);
				setVideoPath(resourcePath + fileName);

				//                    setVideoPath((String) msg.obj);
				start();
				break;
			case SLAVE_SYNC:
				DatagramData data = (DatagramData) msg.obj;
				Log.d("SPOAN", "received data: " + data.toString());
				slaveSync(data);
			default:
				break;
			}

			return true;
		}
	});

	private class SlaveThread extends Thread {
		public SlaveThread(Runnable runnable) {
			super(runnable);
		}

		public void quit() {
			Looper looper = Looper.myLooper();
			if (looper != null && looper != Looper.getMainLooper()) {
				looper.quit();
			}
		}
	}

	private SlaveThread mSlaveThread = new SlaveThread(new Runnable() {
		@Override
		public void run() {
			Looper.prepare();
			UDPSync.receiveMulticastBroadcast(getContext(), mGroupId, new UDPSync.Callback() {
				@Override
				public void onDataReceived(DatagramData data) {
					if (data.getMsec() == EOF) {
						post(new Runnable() {
							@Override
							public void run() {
								stopPlayback();
							}

						});

						slaveInitialized = false;
					} else
						sendMsg(SLAVE_SYNC, data);
				}
			});
			Looper.loop();
		}
	});

	private MediaPlayer.OnErrorListener mOnErrorListener = new MediaPlayer.OnErrorListener() {

		@Override
		public boolean onError(MediaPlayer mp, int what, int extra) {
			return true;
		}
	};

	private MediaPlayer.OnPreparedListener mMasterListener = new MediaPlayer.OnPreparedListener() {
		@Override
		public void onPrepared(final MediaPlayer mp) {

			if (mTimer == null) {
				mTimer = new Timer("udp_master");
			}
			mTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					DatagramData data = new DatagramData();
					data.setFileName(mMasterFileName);
					data.setMsec(getCurrentPosition());

					if (UDPSync.SendMulticastBroadcast(getContext(), mGroupId, data)) {
						String msg = "Broadcast packet sent to: " + UDPSync.getBroadcastAddress(getContext()).getHostAddress() + " " + data.toString();
						Log.d(TAG, msg);
						sendMsg(msg);
					}
				}
			}, 0, 500);
		}
	};

	private void masterExit() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				DatagramData data = new DatagramData();
				data.setFileName(mMasterFileName);
				data.setMsec(EXIT);

				if (UDPSync.SendMulticastBroadcast(getContext(), mGroupId, data)) {
					String msg = "Broadcast packet sent to: " + UDPSync.getBroadcastAddress(getContext()).getHostAddress() + " " + data.toString();
					sendMsg(msg);
				}
			}
		}).start();
	}

	private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
		@Override
		public void onCompletion(MediaPlayer mp) {
			if (mRole == UDP_MASTER) {
				// send EOF to slave

				new Thread(new Runnable() {
					@Override
					public void run() {
						DatagramData data = new DatagramData();
						data.setFileName(mMasterFileName);
						data.setMsec(EOF);

						if (UDPSync.SendMulticastBroadcast(getContext(), mGroupId, data)) {
							String msg = "Broadcast packet sent to: " + UDPSync.getBroadcastAddress(getContext()).getHostAddress() + " " + data.toString();
							sendMsg(msg);
						}
					}
				}).start();

				// cancel the timer
				mTimer.cancel();
				mTimer.purge();
				mTimer = null;

				playVideoList();
			}

			if (mRole == UDP_SLAVE) {
				lastSuccess = 0;

				if (slaveInitialized) { // Indicate the Master's EOF missed, but we can still receive
					slaveInitialized = false;
					stopPlayback();
					playVideoList();
				}
			}
		}
	};

	public BsqVideoView(Context context) {
		super(context);

		init(context);
	}

	public BsqVideoView(Context context, AttributeSet attrs) {
		super(context, attrs);

		init(context);
	}

	public BsqVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		init(context);
	}

	private void init(Context context) {
		setOnErrorListener(mOnErrorListener);
		setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startDefaultBrowser(getContext(), "http://www.baidu.com");
			}
		});
	}

	private void startDefaultBrowser(Context context, String url) {
		Intent intent = new Intent();
		intent.setData(Uri.parse(url));
		List<ResolveInfo> list = context.getApplicationContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

		for (ResolveInfo info : list) {
			if ((info.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) > 0) {
				intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
				context.startActivity(intent);
			}
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		width = getDefaultSize(width, widthMeasureSpec);
		height = getDefaultSize(height, heightMeasureSpec);

		setMeasuredDimension(width, height);
	}

	public void enableSync(int mode, String groupId) {
		UDPSync.multicastInit(getContext());
		mGroupId = "225.1.1." + groupId;

		Log.d("SPOAN", "group id: " + mGroupId);

		switch (mode) {
		case UDP_MASTER:
			mRole = UDP_MASTER;
			setOnPreparedListener(mMasterListener);
			setOnCompletionListener(mCompletionListener);
			break;
		case UDP_SLAVE:
			sendMsg("Waiting for master...");
					mRole = UDP_SLAVE;
					UDPSync.setInterrupt(false);
					setOnCompletionListener(mCompletionListener);
					mSlaveThread.start();
					break;
		default:
			break;
		}
	}

	public void disableSync() {
		UDPSync.multicastDestroy(getContext());

		if (mRole == UDP_MASTER) {
			masterExit();
		}

		if (mTimer != null) {
			mTimer.cancel();
			mTimer.purge();
			mTimer = null;
		}

		if (mSlaveThread != null) {
			mSlaveThread.quit();
			mSlaveThread = null;
		}

		UDPSync.setInterrupt(true);
		slaveInitialized = false;
	}

	private synchronized void slaveSync(final DatagramData data) {
		if (!isPlaying()) {
			if (!slaveInitialized) {
				slaveInitialized = true;

				sendMsg("start playback: " + data.getFileName());
				sendMsg(START_PLAYBACK, data.getFileName());

				setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {

						lastSuccess = System.currentTimeMillis();
					}
				});
			}
		} else {
			int position = getCurrentPosition();
			int duration = getDuration();

			if (data.getMsec() > duration) {
				return;
			}

			if (data.getMsec() - position > 800 || data.getMsec() == 0) {
				sendMsg("gap: " + (position - data.getMsec()));

				int seek = (int) (data.getMsec() + (System.currentTimeMillis() - lastSuccess));

				if (data.getMsec() == 0) { // master restart or master ahead of us
					seek = data.getMsec(); // same video and the master restart anyway

					if (slaveInitialized) { // Maybe the master ahead of us
						slaveInitialized = false;
						stopPlayback();
						playVideoList();
					}
				}

				if (seek > getDuration()) {
					seekTo(getDuration());
				} else {
					seekTo(seek);
				}

			} else {
				//                sendMsg("gap1: " + (getCurrentPosition() - data.getMsec()));
				Log.d(TAG, "gap: " + (position - data.getMsec()));
			}

			if (position - data.getMsec() > 600)
				seekTo(data.getMsec());
		}

		lastSuccess = System.currentTimeMillis();
	}

	public void setLayout(float x, float y, int w, int h) {
		setX(x);
		setY(y);
		getHolder().setFixedSize(w, h);
	}

	public void playVideoList() {
		if (videoPlayList.size() == 0)
			return;

		mMasterFileName = videoPlayList.get(fileIndex);
		setVideoPath(mMasterFileName);

		fileIndex++;
		if (fileIndex >= videoPlayList.size())
			fileIndex = 0;

		start();
	}

	//    private TestActivity mTestActivity;
	//
	//    public void setActivity(TestActivity activity) {
	//        mTestActivity = activity;
	//    }

	private void sendMsg(String msg) {
		//        mTestActivity.sendMsg(TestActivity.UPDATE_UI, msg);
	}

	private void sendMsg(int what, Object obj) {
		Message message = mHandler.obtainMessage();
		message.what = what;
		message.obj = obj;
		message.sendToTarget();
	}
}
