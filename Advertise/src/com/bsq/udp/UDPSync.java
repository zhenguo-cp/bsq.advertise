package com.bsq.udp;

import android.content.Context;
import android.content.Intent;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

/**
 * Created by macro on 15/4/15.
 */
public class UDPSync {
	private static final int MULTICAST_PORT = 5050;
	private static boolean mInterrupt = false;

	public static synchronized boolean SendMulticastBroadcast(Context context, String groupIp, DatagramData data) {
		MulticastSocket multicastSocket = null;

		try {
			InetAddress group = InetAddress.getByName(groupIp);

			multicastSocket = new MulticastSocket(null);
			multicastSocket.setReuseAddress(true);
			multicastSocket.bind(new InetSocketAddress(MULTICAST_PORT));
			multicastSocket.setTimeToLive(32);
			multicastSocket.joinGroup(group); 
			multicastSocket.setLoopbackMode(false);

			JSONObject object = new JSONObject();
			object.put("msec", data.getMsec());
			object.put("fileName", data.getFileName());
			object.put("ts", System.currentTimeMillis());

			byte[] sendData = object.toString().getBytes();
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, group, MULTICAST_PORT);
			multicastSocket.send(sendPacket);
			return true;

		} catch (JSONException | IOException e) {
			e.printStackTrace();
		} finally {
			if (multicastSocket != null)
				multicastSocket.close();
		}

		return false;
	}

	public static synchronized void receiveMulticastBroadcast(Context context, String groupIp, Callback callback) {
		MulticastSocket multicastSocket = null;

		try {
			InetAddress group = InetAddress.getByName(groupIp);

			multicastSocket = new MulticastSocket(null);
			multicastSocket.setReuseAddress(true);
			multicastSocket.bind(new InetSocketAddress(MULTICAST_PORT));
			multicastSocket.setTimeToLive(32);
			multicastSocket.joinGroup(group); 
			multicastSocket.setLoopbackMode(false);

			Log.i("SPOAN", " Ready to receive broadcast packets!!!");

			while (true) {
				if (mInterrupt) {
					break;
				}
				// Receive a packet
				byte[] recvBuf = new byte[2048];
				DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
				multicastSocket.receive(packet);

				// Packet received
				//                Log.i("SPOAN", "Packet received from: " + packet.getAddress().getHostAddress());
				String s = new String(packet.getData());
				JSONObject object = new JSONObject(s);
				DatagramData data = new DatagramData();
				data.setMsec(object.getInt("msec"));
				data.setFileName(object.getString("fileName"));
				data.setTs(object.getLong("ts"));

				if (callback != null) {
					callback.onDataReceived(data);
				}

				Log.i("SPOAN", "Packet received; data: " + data.toString());

				//                Intent intent = new Intent(Constants.BROADCAST_ACTION).putExtra(Constants.EXTENDED_DATA_STATUS, data);
				//                context.sendBroadcast(intent);
			}
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		} finally {
			if (multicastSocket != null)
				multicastSocket.close();
		}
	}

	public static void multicastInit(Context context) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		MulticastLock multicastLock = wifiManager.createMulticastLock("bsq_multicast_lock");
		multicastLock.acquire();
	}

	public static void multicastDestroy(Context context) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		MulticastLock multicastLock = wifiManager.createMulticastLock("bsq_multicast_lock");
		if (multicastLock.isHeld())
			multicastLock.release();
	}

	public static InetAddress getBroadcastAddress(Context context) {
		WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		DhcpInfo dhcp = wifi.getDhcpInfo();
		// handle null somehow

		int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
		byte[] quads = new byte[4];
		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
		try {
			return InetAddress.getByAddress(quads);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static synchronized boolean SendBroadcast(Context context, DatagramData data) {
		try {
			DatagramSocket socket = new DatagramSocket();
			socket.setBroadcast(true);

			JSONObject object = new JSONObject();
			object.put("msec", data.getMsec());
			object.put("fileName", data.getFileName());
			object.put("ts", System.currentTimeMillis());

			byte[] sendData = object.toString().getBytes();
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, getBroadcastAddress(context), Constants.PORT);
			socket.send(sendPacket);
			return true;

		} catch (JSONException | IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static void receiveBroadcast(Context context) {
		DatagramSocket socket = null;

		try {
			socket = new DatagramSocket(Constants.PORT, InetAddress.getByName("0.0.0.0"));
			socket.setReuseAddress(true);
			socket.setBroadcast(true);

			Log.i("SPOAN", "Ready to receive broadcast packets!");

			while (true) {
				// Receive a packet
				byte[] recvBuf = new byte[2048];
				DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
				socket.receive(packet);

				// Packet received
				Log.i("SPOAN", "Packet received from: " + packet.getAddress().getHostAddress());
				String s = new String(packet.getData());
				JSONObject object = new JSONObject(s);
				DatagramData data = new DatagramData();
				data.setMsec(object.getInt("msec"));
				data.setFileName(object.getString("fileName"));
				data.setTs(object.getLong("ts"));

				Log.i("SPOAN", "Packet received; data: " + data.toString());

				Intent intent = new Intent(Constants.BROADCAST_ACTION).putExtra(Constants.EXTENDED_DATA_STATUS, data);
				context.sendBroadcast(intent);
			}
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		} finally {
			if (socket != null)
				socket.close();
		}
	}

	public static synchronized void receiveBroadcast(Context context, Callback callback) {
		DatagramSocket socket = null;

		try {
			socket = new DatagramSocket(Constants.PORT, InetAddress.getByName("0.0.0.0"));
			socket.setReuseAddress(true);
			socket.setBroadcast(true);

			Log.i("SPOAN", "Ready to receive broadcast packets!");

			while (true) {
				if (mInterrupt) {
					break;
				}
				// Receive a packet
				byte[] recvBuf = new byte[2048];
				DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
				socket.receive(packet);

				// Packet received
				//                Log.i("SPOAN", "Packet received from: " + packet.getAddress().getHostAddress());
				String s = new String(packet.getData());
				JSONObject object = new JSONObject(s);
				DatagramData data = new DatagramData();
				data.setMsec(object.getInt("msec"));
				data.setFileName(object.getString("fileName"));
				data.setTs(object.getLong("ts"));

				if (callback != null) {
					callback.onDataReceived(data);
				}

				Log.i("SPOAN", "Packet received; data: " + data.toString());

				//                Intent intent = new Intent(Constants.BROADCAST_ACTION).putExtra(Constants.EXTENDED_DATA_STATUS, data);
				//                context.sendBroadcast(intent);
			}
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		} finally {
			if (socket != null)
				socket.close();
		}
	}

	public static void setInterrupt(boolean interrupt) {
		mInterrupt = interrupt;
	}

	public interface Callback {
		void onDataReceived(DatagramData data);
	}
}
