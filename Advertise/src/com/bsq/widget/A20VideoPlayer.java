package com.bsq.widget;

import java.util.ArrayList;
import java.util.Observer;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

public class A20VideoPlayer extends VideoView {
	private int fileIndex;
	public ArrayList<String> videoPlayList;
	private int width, height;
	private VideoPlayerObservable mObservable;
	
	public A20VideoPlayer(Context context) {
		super(context);
		initlalize();
	}
	
	public A20VideoPlayer(Context context, AttributeSet attrs) {
		super(context, attrs);
		initlalize();
	}

	public A20VideoPlayer(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initlalize();
	}
	
	private void initlalize() {
		fileIndex = 0;
		mObservable = new VideoPlayerObservable();
	}
	
	public void addObserver(Observer observer) {
		mObservable.addObserver(observer);
	}
	
	public void setDimension(int w, int h) {
		width = w;
		height = h;
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		//super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		width = getDefaultSize(width, widthMeasureSpec);
		height = getDefaultSize(height, heightMeasureSpec);
		
		setMeasuredDimension(width, height);
	}
	
	public void playVideoList() {
//		Log.v("cook", "index: " + fileIndex + "  fileName: " + videoPlayList.get(fileIndex));
		
		setVideoPath(videoPlayList.get(fileIndex));
		start();
		fileIndex++;
		if (fileIndex >= videoPlayList.size()) {
			mObservable.videoExpired();
			mObservable.notifyObservers();
			fileIndex = 0;
		}
	}
}
