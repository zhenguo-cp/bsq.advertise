package com.bsq.widget;

import java.util.Observable;

public class VideoPlayerObservable extends Observable {
	public void videoExpired() {
		setChanged();
	}
}
