package com.bsq.widget;

import java.util.ArrayList;
import java.util.Observer;
import java.util.Random;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.view.View.OnClickListener;

import com.bsq.advertise.AdvertiseController;
import com.bsq.advertise.Common;
import com.bsq.bs.SmilParser.LayoutDetails;


public class ImagePlayer extends ImageView implements AnimatorListener, OnClickListener {

	private int fileIndex;
	private ArrayList<String> imagePlayList;
	private ArrayList<String> intervalList;
	private ArrayList<String> animList;
	private static final int OPEN_GALLERY = 0x12345;
	private static final String TAG = ImagePlayer.class.getSimpleName();
	private LruCache<String, Bitmap> mLruCache;
	private int mWidth;
	private int mHeight;
	private boolean isMeasured;
	private Bitmap mCurrentBitmap;
	private ImagePlayerObservable mObservable;

	public ImagePlayer(Context context) {
		super(context);

		initialize();
	}

	public ImagePlayer(Context context, Common cn) {
		super(context);

		initialize();
		initFrom(cn);
	}

	public ImagePlayer(Context context, AttributeSet attrs) {
		super(context, attrs);

		initialize();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (!isMeasured) {
			mWidth = getMeasuredWidth();
			mHeight = getMeasuredHeight();
			isMeasured = true;

			Start();
		}
	}

	private void initialize() {
		fileIndex = 0;
		setOnClickListener(this);
		setScaleType(ImageView.ScaleType.CENTER_CROP);
		isMeasured = false;

		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 8;

		mLruCache = new LruCache<String, Bitmap>(4 * 1024 * 1024) {
			@Override
			protected int sizeOf(String key, Bitmap value) {
				return value.getByteCount();
			}
		};

		mObservable = new ImagePlayerObservable();
	}

	public void addObserver(Observer observer) {
		mObservable.addObserver(observer);
	}

	private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemoryCache(key) == null)
			mLruCache.put(key, bitmap);
	}

	private Bitmap getBitmapFromMemoryCache(String key) {
		return mLruCache.get(key);
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
	}

	public void initFrom(Common cn) {
		imagePlayList = cn.imagePlayList;
		intervalList = cn.intervalList;
		animList = cn.animList;
	}

	public void initFrom(LayoutDetails ld) {
		imagePlayList = ld.imgSrc;
		intervalList = ld.imgDur;
		animList = ld.imgMode;
	}

	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	private Bitmap decodeSampledBitmapFromFile(String fileName, int reqWidth, int reqHeight) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(fileName, options);

		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		options.inJustDecodeBounds = false;

		Log.d(TAG, "decodeFile: " + fileName + ", " + reqWidth + ", " + reqHeight);
		return BitmapFactory.decodeFile(fileName, options);
	}

	private class BitmapInfo {
		int mWidth;
		int mHeight;
		String mFileName;
	}

	private class BitmapWorkTask extends AsyncTask<BitmapInfo, Integer, Bitmap> {

		@Override
		protected Bitmap doInBackground(BitmapInfo... params) {
			BitmapInfo info = params[0];

			Log.v("cook", "fileName: " + info.mFileName);

			mCurrentBitmap = getBitmapFromMemoryCache(info.mFileName);
			if (mCurrentBitmap == null) {
				mCurrentBitmap = decodeSampledBitmapFromFile(info.mFileName, info.mWidth, info.mHeight);
				addBitmapToMemoryCache(info.mFileName, mCurrentBitmap);
			}	

			return mCurrentBitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			if (result != null) {
				setImageBitmap(result);
				setScaleType(ScaleType.CENTER_CROP);

				long duration = Long.parseLong(intervalList.get(fileIndex)) * 1000 + 4000;
				int mode = Integer.parseInt(animList.get(fileIndex));

				ObjectAnimator anim_default = ObjectAnimator.ofFloat(ImagePlayer.this, "alpha", 1, 1f, 1);
				anim_default.setStartDelay(duration);
				anim_default.setDuration(duration);
				anim_default.addListener(ImagePlayer.this);

				Log.v("cook", "mode: " + mode);
				if (mode == 0) {
					mode = 25 % (new Random(25).nextInt());
					Log.v("cook", "ramdom: " + mode);
				}

				switch (mode) {
				case 24:
					ObjectAnimator rotationX = ObjectAnimator.ofFloat(ImagePlayer.this, "rotationX", 0f, 180f, 0f).setDuration(4000);
					rotationX.start();
					anim_default.start();
					break;
				case 25:
					ObjectAnimator rotationX1 = ObjectAnimator.ofFloat(ImagePlayer.this, "rotationX", 180f, 0f).setDuration(4000);
					rotationX1.start();
					anim_default.start();
					break;
				case 22:
					ObjectAnimator rotationY = ObjectAnimator.ofFloat(ImagePlayer.this, "rotationY", 0f, 180f, 0f).setDuration(4000);
					rotationY.start();
					anim_default.start();
					break;
				case 23:
					ObjectAnimator rotationY1 = ObjectAnimator.ofFloat(ImagePlayer.this, "rotationY", 180f, 0f).setDuration(4000);
					rotationY1.start();
					anim_default.start();
					break;
				case 12:
					ObjectAnimator scaleX = ObjectAnimator.ofFloat(ImagePlayer.this, "scaleX", 0.5f, 1f).setDuration(4000);

					scaleX.start();
					anim_default.start();
					break;
				case 13:
					ObjectAnimator scaleX1 = ObjectAnimator.ofFloat(ImagePlayer.this, "scaleX", 1f, 0.5f, 1f).setDuration(4000);
					scaleX1.start();
					anim_default.start();
					break;
				case 11:
					ObjectAnimator scaleY = ObjectAnimator.ofFloat(ImagePlayer.this, "scaleY", 0.5f, 1f).setDuration(4000);
					scaleY.start();
					anim_default.start();
					break;
				case 9:
					ObjectAnimator scaleY1 = ObjectAnimator.ofFloat(ImagePlayer.this, "scaleY", 1f, 0.5f, 1f).setDuration(4000);
					scaleY1.start();
					anim_default.start();
					break;
				case 21:
					ObjectAnimator alpha = ObjectAnimator.ofFloat(ImagePlayer.this, "alpha", 1, 0.25f, 1).setDuration(4000);
					alpha.start();
					anim_default.start();
					break;
				case 8:
					ObjectAnimator translationX = ObjectAnimator.ofFloat(ImagePlayer.this, "translationX", -getWidth(), 0f).setDuration(4000);
					translationX.start();
					anim_default.start();
					break;
				case 6:
					ObjectAnimator translationX1 = ObjectAnimator.ofFloat(ImagePlayer.this, "translationX", getWidth(), 0f).setDuration(4000);
					translationX1.start();
					anim_default.start();
					break;
				case 5:
					ObjectAnimator translationY = ObjectAnimator.ofFloat(ImagePlayer.this, "translationY", getHeight(), 0f).setDuration(4000);
					translationY.start();
					anim_default.start();
					break;
				case 7:
					ObjectAnimator translationY1 = ObjectAnimator.ofFloat(ImagePlayer.this, "translationY", -getHeight(), 0f).setDuration(4000);
					translationY1.start();
					anim_default.start();
					break;
				default:
					anim_default.start();
					break;
				}
			} else 
				Log.v("cook", "result is null");
			
			fileIndex++;
			if (fileIndex >= imagePlayList.size()) {
				mObservable.imageExpired();
				mObservable.notifyObservers();
				fileIndex = 0;
			}
		}
	}

	public void Start() {
		if (imagePlayList.size() == 0)
			return;

		BitmapInfo params = new BitmapInfo();
		params.mFileName = imagePlayList.get(fileIndex);
		params.mWidth = mWidth;
		params.mHeight = mHeight;

		new BitmapWorkTask().execute(params);
	}

	@Override
	public void onAnimationStart(Animator animation) {
	}

	@Override
	public void onAnimationEnd(Animator animation) {
		System.gc();
		
		Start();		
	}

	@Override
	public void onAnimationCancel(Animator animation) {
	}

	@Override
	public void onAnimationRepeat(Animator animation) {
	}

	public void cleanup() {
		clearAnimation();
		imagePlayList.clear();
		intervalList.clear();
		animList.clear();
		mLruCache.evictAll();
		mObservable.deleteObservers();
		
		System.gc();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return super.onTouchEvent(event);
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(Intent.ACTION_VIEW, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		AdvertiseController.instance().startActivityForResult(intent, OPEN_GALLERY);		
	}
}
