package com.bsq.widget;

import java.util.Observer;

import com.bsq.advertise.Common;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class ImageAreaFragment extends Fragment {

	private static final String TAG = ImageAreaFragment.class.getSimpleName();
	private Common data;
	private ImagePlayer player;
	private Observer mObserver;

	public ImageAreaFragment() {

	}

	public void setData(Common cn) {
		data = cn;
	}

	public void addObserver(Observer observer) {
		this.mObserver = observer;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (data == null) {
			Log.v("cook", "data is null");
			return null;
		}

		player = new ImagePlayer(container.getContext(), data);
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(data.placeInfo.right,
				data.placeInfo.buttom);
		layoutParams.setMargins(
				(int) data.placeInfo.left, 
				(int) data.placeInfo.top, 
				0, 
				0);
		player.setLayoutParams(layoutParams);
		player.invalidate();
		player.requestLayout();

		if (mObserver != null)
			player.addObserver(mObserver);

		return player;
	}

	@Override
	public void onInflate(Activity activity, AttributeSet attrs,
			Bundle savedInstanceState) {
		getView().setX(data.placeInfo.left);
		super.onInflate(activity, attrs, savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();

		if (player != null) {
			player.cleanup();
			player = null;
			Log.d(TAG, "cleanup");
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy");
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}
}
