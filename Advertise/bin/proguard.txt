# view AndroidManifest.xml #generated:74
-keep class com.bsq.advertise.AdvertiseController { <init>(...); }

# view AndroidManifest.xml #generated:86
-keep class com.bsq.advertise.BSQService { <init>(...); }

# view res/layout/advertise_controller.xml #generated:30
-keep class com.bsq.advertise.FragmentWeather { <init>(...); }

# view res/layout/fragment_marquee_text.xml #generated:6
-keep class com.bsq.advertise.MarqueeText { <init>(...); }

# view AndroidManifest.xml #generated:66
-keep class com.bsq.advertise.util.AlarmReceiver { <init>(...); }

# view AndroidManifest.xml #generated:52
-keep class com.bsq.advertise.util.BootBroadcastReceiver { <init>(...); }

# view AndroidManifest.xml #generated:59
-keep class com.bsq.advertise.util.SelfUpdated { <init>(...); }

# view AndroidManifest.xml #generated:87
-keep class com.bsq.advertise.util.Watchdog { <init>(...); }

# view res/layout/advertise_controller.xml #generated:6
-keep class com.bsq.udp.BsqVideoView { <init>(...); }

# view res/layout/advertise_controller.xml #generated:13
-keep class com.bsq.widget.A20VideoPlayer { <init>(...); }

# view AndroidManifest.xml #generated:68
-keep class io.vov.vitamio.activity.InitActivity { <init>(...); }

