/*   Version V1.2  			 */
/*		   			 */
/*   PC 为server锟剿ｏ拷Sigma 锟斤拷锟斤拷为Client锟斤拷 */
/*   通锟脚端匡拷: 9897			 */

#ifndef _SMP_COMMAND_DEF_
#define _SMP_COMMAND_DEF_


//-------------------------------------------------
// 	锟斤拷萁峁癸拷锟斤拷锟�
//-------------------------------------------------
#pragma pack(1)
//锟斤拷头锟斤拷锟斤拷
typedef struct
{
unsigned short m_nCommand;//锟斤拷锟斤拷锟斤拷
unsigned int  m_nDataLen;//锟斤拷荻纬锟斤拷锟�
}PACKET_HEAD;

typedef struct {
	unsigned short m_nCommand;//锟斤拷锟斤拷锟斤拷
	unsigned int  m_nDataLen;//锟斤拷荻纬锟斤拷锟�
	unsigned int volume;
} PACKET_SET_VOLUME;

//锟斤拷锟斤拷锟斤拷息
typedef struct
{
	long nErrorCode;//锟斤拷锟斤拷锟斤拷:1为失锟杰ｏ拷0为锟缴癸拷
	char szErrorString[260];//锟斤拷锟斤拷
}BSQ_ERROR_INFO;

//锟芥储锟斤拷锟斤拷锟斤拷锟�
enum BSQ_DISK_TYPE
{
	NDT_U_PAN         = 1,//U锟教伙拷硬锟斤拷
	NDT_SD_CARD       = 2,//SD锟斤拷
};

//锟侥硷拷锟斤拷锟�式
enum BSQ_RES_MANAGE_TYPE
{
        BSQ_RES_AUTO        = 1,//锟皆讹拷锟斤拷锟斤拷
        BSQ_RES_MANUAL      = 2,//锟街讹拷锟斤拷锟斤拷
};

//锟侥硷拷锟斤拷锟斤拷锟斤拷
enum BSQ_FILE_TYPE
{
    BSQ_DIR,
    BSQ_FILE,
};

typedef struct
{
    enum BSQ_FILE_TYPE type;//锟侥硷拷锟斤拷锟斤拷
    long long size;
    char lastChangeTime[20];//锟斤拷锟斤拷薷锟绞憋拷锟�
    char fullFileName[80];//锟斤拷锟斤拷锟侥硷拷路锟斤拷
}BSQ_FILE_INFO;



//U锟教★拷SATA硬锟教★拷SD锟斤拷锟饺存储锟斤拷锟斤拷锟较�
typedef struct
{
        long nType;//锟芥储锟斤拷锟斤拷锟斤拷锟�
	long nTotalStorageSize;//锟杰的存储锟秸硷拷(锟斤拷位锟斤拷M Byte)
	long nFreeSize;//锟斤拷锟矫空硷拷(锟斤拷位锟斤拷M Byte)
	char szDickName[260];//锟斤拷锟�锟斤拷锟斤拷/U_PAN/)
}BSQ_DISK_INFO_ITEM;


//Sigma锟斤拷锟斤拷系统状态
enum BSQ_SYSTEM_STATE
{
	NSS_WORKING         = 1,//锟斤拷锟斤拷状态
	NSS_HIBERNATING     = 2,//锟斤拷锟斤拷状态
        NSS_REBOOT          = 3,//锟斤拷锟斤拷锟斤拷佣锟�

};

//锟斤拷幕时锟斤拷锟绞�
enum BSQ_OSD_TIME_FORMAT
{
	TIME_ANALOG                = 1,//模锟斤拷时锟斤拷
	TIME_DIGITAL	           = 2,//锟斤拷锟斤拷时锟斤拷
        NONE			   = 3 //锟斤拷锟斤拷示
};

//系统时锟斤拷
typedef struct
{
	long nType;//锟斤拷锟酵ｏ拷锟轿匡拷NP_OSD_TIME_FORMAT
        long setTime;//锟角凤拷锟斤拷锟矫帮拷锟斤拷时锟斤拷 0 -- 锟斤拷 1 -- 锟斤拷
	long Year;//锟斤拷
	long Month;//锟斤拷
	long Day;//锟斤拷
	long Hour;//时
	long Minute;//锟斤拷
	long Second;//锟斤拷
}BSQ_SYSTEM_TIME;

//锟斤拷锟截伙拷时锟斤拷
typedef struct
{
    long startHour;//锟斤拷始时锟斤拷小时
    long startMinute;//锟斤拷始时锟斤拷锟斤拷锟�
    long startSecond;//锟斤拷始时锟斤拷锟斤拷
    long endHour;//锟斤拷锟斤拷时锟斤拷小时
    long endMinute;//锟斤拷锟斤拷时锟斤拷锟斤拷锟�
    long endSecond;//锟斤拷锟斤拷时锟斤拷锟斤拷
}BSQ_POWER_TIME;

//锟斤拷锟剿匡拷锟斤拷息
typedef struct
{
        int width;
        int height;
	char szPort[10];//锟剿匡拷
	char szOurputPara[80];//锟剿口诧拷锟斤拷(锟斤拷锟斤拷HDMI_1080p60)
}BSQ_OUTPUT_PORT_INFO;

//锟斤拷锟轿伙拷锟斤拷锟较�
typedef struct
{
    char szName[80];//锟斤拷锟�
    char szPosition[128];//位锟斤拷锟斤拷锟斤拷
}BSQ_NAMEPOSITION;

//锟斤拷锟斤拷频
typedef struct
{
    _Bool onoff; //0 --锟街革拷锟斤拷 1--锟斤拷锟脚斤拷锟斤拷频
    int  num; //锟斤拷锟斤拷目锟侥憋拷锟�
}BSQ_EMER_VIDEO;

//Sigma锟斤拷锟斤拷锟斤拷息
typedef struct
{   
        BSQ_NAMEPOSITION namePos; //锟斤拷锟轿伙拷锟斤拷锟较�
        char szIPAddress[20];//IP锟斤拷址
	long szStatus; //系统状态
        char time[20];//系统时锟斤拷
        int volume;//锟斤拷
        BSQ_OUTPUT_PORT_INFO portInfo;//锟剿匡拷锟斤拷息
        char mac[20];//MAC
        char version[10];//锟芥本
        char diskInfo[30];//锟斤拷锟斤拷锟斤拷锟斤拷锟较�
}BSQ_ADVERTISE_INFO;

typedef struct
{
	char file_name[80];  //锟侥硷拷锟斤拷
        long long totle_size; //锟侥硷拷锟斤拷小
        long long download_size; //锟窖撅拷锟斤拷锟截达拷小
}BSQ_TRANSFER_INFO;

typedef struct
{
    char place; //要锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷谋锟斤拷
    char mode;  //锟斤拷锟斤拷锟斤拷示锟斤拷模式
    char row;   //锟斤拷态锟斤拷锟斤拷时锟斤拷示锟斤拷锟斤拷锟斤拷
    char sound; //锟角否播凤拷锟斤拷锟斤拷 0-锟斤拷 1-锟斤拷
    char txt[1024];//锟斤拷锟斤拷锟斤拷锟斤拷
}BSQ_INSERT_TXT;

//锟斤拷锟斤拷锟斤拷锟�
typedef struct
{
    char ip[20];    //ip锟斤拷址
    char mask[20];  //锟斤拷锟斤拷
    char gateway[20];//锟斤拷锟�
    char dns[20]; //DNS
    char ssid[30];  //锟斤拷锟竭广播ID
    char pwd[60];   //锟斤拷锟斤拷锟斤拷锟斤拷
    char server[50];//锟斤拷锟斤拷锟斤拷
    int threeG;     //3G锟斤拷锟斤拷
}BSQ_NETWORK_INFO;

#pragma pack()

//-----------------------------------------------
//锟斤拷锟斤拷锟斤拷锟矫的端匡拷
//-----------------------------------------------
#define COMMUNICATIONPORT   9897
#define COMMUNICATIONPORTSTR "9897"
#define UDP_SEND_PORT 9898

//-------------------------------------------------
//	PC锟斤拷锟狡端凤拷锟斤拷Sigma锟斤拷锟斤拷TCP锟斤拷锟斤拷
//-------------------------------------------------
#define SC_GET_SYS_INFO		0x1000	   //锟斤拷取Sigma锟斤拷锟斤拷锟斤拷息锟斤拷系统锟斤拷锟斤拷状态
#define SC_GET_LOG	        0x1001     //PC锟斤拷取每锟斤拷锟斤拷拥牟锟斤拷锟斤拷占锟絣og
#define SC_FTP_DOWN_STATUS      0x1002     //询锟斤拷Sigma锟斤拷锟斤拷锟斤拷锟截斤拷锟�
#define SC_GET_DIR              0x1003     //询锟绞达拷锟斤拷锟借备指锟斤拷路锟斤拷锟斤拷锟侥硷拷锟叫憋拷
#define SC_DELETE_FILE          0x1004     //删锟斤拷锟斤拷璞钢革拷锟铰凤拷锟斤拷锟斤拷募锟�
#define SC_MANAGE_RES_TYPE      0x1005     //锟斤拷锟斤拷锟斤拷源锟斤拷式
#define SC_KEEP_LIVE            0x1006     //锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷
#define SC_GET_COLOR            0x1007     //锟斤拷取色锟斤拷值
#define SC_GET_CHANNEL_LIST     0x1008     //锟斤拷取锟斤拷锟斤拷频锟斤拷锟叫憋拷
#define SC_GET_SCREEN           0x1009     //锟斤拷取锟斤拷前锟斤拷锟斤拷锟斤拷锟捷的斤拷锟斤拷

#define SC_SET_INFOR            0x1100     //锟斤拷锟斤拷锟斤拷锟轿伙拷锟斤拷锟较�
#define SC_SET_STATUS		0x1101	   //锟斤拷锟斤拷系统锟斤拷锟脚伙拷停止状态
#define SC_FRESH_XML		0x1102     //锟斤拷锟斤拷XML锟斤拷锟斤拷
#define SC_EMERGENCY_TXT	0x1103	   //锟斤拷锟斤拷锟斤拷锟斤拷息
#define SC_SET_CLOCK		0x1104     //锟斤拷锟斤拷时锟接★拷锟斤拷锟斤拷模锟斤拷锟斤拷锟斤拷锟斤拷锟绞斤拷锟斤拷锟绞臼憋拷踊锟截憋拷时锟斤拷
#define SC_SET_VOLUME		0x1105     //锟斤拷锟斤拷锟斤拷
#define SC_SET_OUTPORTS		0x1106	   //锟斤拷锟斤拷锟斤拷频锟斤拷锟剿口诧拷锟斤拷
#define SC_INSERT_TXT           0x1107     //锟节碉拷前锟斤拷目锟叫碉拷某锟斤拷锟斤拷锟街诧拷锟斤拷锟斤拷锟斤拷锟叫诧拷锟斤拷锟斤拷锟斤拷
#define SC_SET_NETWORK          0x1108     //锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟�
#define SC_SET_BAUDRATE         0x1109     //锟斤拷锟矫达拷锟节诧拷锟斤拷锟斤拷
#define SC_SET_POWERONOFF       0x110a     //锟斤拷锟矫匡拷锟截伙拷时锟斤拷
#define SC_EMERGENCY_ONOFF      0x110b     //锟斤拷锟矫斤拷锟斤拷锟�0 -- off 1 -- on
#define SC_SET_COLOR            0x110c     //锟斤拷锟斤拷色锟斤拷值
#define SC_SET_CHANNEL_LIST     0x110d     //锟斤拷锟矫碉拷锟斤拷频锟斤拷
#define SC_INSERT_PIC           0x110e     //锟节碉拷前锟斤拷目锟藉播图片


#define SC_UPGRADE_PROGRAM      0x1200     //锟斤拷

//-------------------------------------------------
//	PC锟斤拷锟斤拷FTP锟斤拷锟斤拷
//-------------------------------------------------
//#define SC_FTP...



//-------------------------------------------------
//	PC锟姐播锟斤拷锟斤拷
//-------------------------------------------------  
#define BS_FIND_CLIENT    	3001	 //锟斤拷锟斤拷Sigma锟斤拷锟斤拷IP
#define SC_HIBERNATING    	3002     //锟姐播锟藉卡锟斤拷锟斤拷
#define SC_WORKING              3003     //锟姐播锟藉卡锟斤拷锟斤拷

//-------------------------------------------------
//	Sigma锟斤拷锟接凤拷!锟斤拷锟斤拷
//-------------------------------------------------
#define CS_RETURN		2001
#define CS_RETURN_STR   "2001"

#endif

