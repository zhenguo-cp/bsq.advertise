#ifndef __BSQ_NET_H__
#define __BSQ_NET_H__

#define _BSD_SOURCE             /* See feature_test_macros(7) */
#define _SVID_SOURCE
#define _XOPEN_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netdb.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <endian.h>
#include <pthread.h>

#include "log.h"
#include "com_bsq_advertise_BSQNet.h"
#include "openssl/md5.h"

#ifdef ANDROID
#include <android/log.h>
#endif

#define true 1
#define false 0

#define IFHWADDRLEN     6
#define IFNAMSIZ        16

#define handle_jni_error(how) \
		do { \
			if ((*env)->ExceptionOccurred(env) != NULL) { \
				(*env)->ExceptionDescribe(env); \
				(*env)->ExceptionClear(env); \
				how; \
			} \
		} while(0)

#define MIN(a, b) ((a) < (b)) ? (a) : (b)

#ifdef __cplusplus
extern "C" {
#endif

#ifdef ANDROID
JavaVM *ad_get_jvm(void);
void ad_set_jvm(JavaVM *vm);
JNIEnv *ad_get_jni_env(void);

typedef struct {
	jclass _class;
	jobject object;
	jmethodID getInstance;
	jmethodID getName;
	jmethodID getPosition;
	jmethodID getAddress;
	jmethodID getStatus;
	jmethodID getTime;
	jmethodID getVersion;
	jmethodID setVolume;
	jmethodID getVolume;
	jmethodID setInfoR;
	jmethodID Setip;
	jmethodID SetNetmask;
	jmethodID SetGateway;
	jmethodID SetDNS;
	jmethodID setSSID;
	jmethodID setPassword;
	jmethodID setHttpServer;
	jmethodID setIs3G;
	jmethodID writeConfig;
	jmethodID setSystemTime;
	jmethodID getSystemTime;
	jmethodID getDiskInfo;
	jmethodID setStatus;
	jmethodID getScreenShot;
	jmethodID getSdcardPath;
} SYSINFO;

typedef struct {
	jclass _class;
	jobject object;
	jmethodID getInstance;
	jmethodID startProcess;
	jmethodID abortDownloading;
	jmethodID connect;
	jmethodID login;
	jmethodID download;
	jmethodID getResourceFileCount;
	jmethodID getResourceFileNameList;
	jmethodID getResourceFileTotalList;
	jmethodID getResourceFileCompletedSizeList;
	jmethodID downloadAPK;
} FTPClient;

#endif

int udp_creation();
void udp_destory();
int tcp_server_creation();
void tcp_destory();
int tcp_reply(const char *msg, size_t msg_len);
char *get_cipher();
int verify_cipher(const char *cipher);
int apk_install(const char *path);

#ifdef __cplusplus
}
#endif

#endif
