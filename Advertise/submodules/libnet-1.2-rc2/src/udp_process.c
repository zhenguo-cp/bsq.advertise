#include "../include/bsq_net.h"
#include "../include/smpcmd.h"

#define RECV_BUF_SIZE 1024
#define SEND_BUF_SIZE 1024

static int datagram_packing(int sfd, struct sockaddr *dest_addr, socklen_t addrlen);

static int udp_holding = true;

#define MAX(a, b) (((a) > (b)) ? (a) : (b))

extern SYSINFO sys_info;

#define handle_jni_error(how) \
		do { \
			if ((*env)->ExceptionOccurred(env) != NULL) { \
				(*env)->ExceptionDescribe(env); \
				(*env)->ExceptionClear(env); \
				how; \
			} \
		} while(0)

void udp_destory()
{
	udp_holding = false;
}

int udp_creation()
{
	struct addrinfo hints;
	struct addrinfo *result, *rp;
	int sfd, s;
	struct sockaddr_storage peer_addr;
	socklen_t peer_addr_len;
	ssize_t nread;
	PACKET_HEAD  packet_head = { 0 };
	fd_set rsfds;
	struct timeval tv;
	int reuseaddr = 1;
	int keepalive = 1;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET;    /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
	hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
	hints.ai_protocol = 0;          /* Any protocol */
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;

	s = getaddrinfo(NULL, COMMUNICATIONPORTSTR, &hints, &result);
	if (s != 0) {
		LOGE("getaddrinfo: %s\n", gai_strerror(s));
		return -1;
	}

	/* getaddrinfo() returns a list of address structures.
       Try each address until we successfully bind(2).
       If socket(2) (or bind(2)) fails, we (close the socket
       and) try the next address. */

	for (rp = result; rp != NULL; rp = rp->ai_next) {
		sfd = socket(rp->ai_family, rp->ai_socktype,
				rp->ai_protocol);
		if (sfd == -1)
			continue;

		if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
			break;                  /* Success */

		close(sfd);
	}

	if (rp == NULL) {               /* No address succeeded */
		LOGE("Could not bind\n");
		freeaddrinfo(result);
		return -1;
	}

	freeaddrinfo(result);           /* No longer needed */

	if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (const void *) &reuseaddr, sizeof(int)) == -1) {
		LOGE("setsockopt(): %s\n", strerror(errno));
	}

	if (setsockopt(sfd, SOL_SOCKET, SO_KEEPALIVE, (const void *) &keepalive, sizeof(int)) == -1) {
		LOGE("setsockopt(): %s\n", strerror(errno));
	}

	/* Read datagrams and echo them back to sender */
	udp_holding = true;
	while (udp_holding) {
		FD_ZERO(&rsfds);
		FD_SET(sfd, &rsfds);

		/* Wait up to one seconds */
		tv.tv_sec = 0;
		tv.tv_usec = 10;

		if ( (s = select(sfd + 1, &rsfds, NULL, NULL, &tv)) == -1) {
			if (s == EINTR)
				continue;
			LOGE("select(): %s\n", strerror(errno));
			return -1;
		}

		if (FD_ISSET(sfd, &rsfds)) {
			LOGV("UDP data is available now");
			memset(&packet_head, 0, sizeof(PACKET_HEAD));
			peer_addr_len = sizeof(struct sockaddr_storage);
			nread = recvfrom(sfd, (void *) &packet_head, sizeof(PACKET_HEAD), 0,
					(struct sockaddr *) &peer_addr, &peer_addr_len);
			if (nread == -1) {
				LOGV("nread == -1");
				continue;               /* Ignore failed request */
			}

			if (nread < sizeof(PACKET_HEAD)) {
				LOGV("Something is wrong");
				continue;
			}

			char host[NI_MAXHOST], service[NI_MAXSERV];

			s = getnameinfo((struct sockaddr *) &peer_addr,
					peer_addr_len, host, NI_MAXHOST,
					service, NI_MAXSERV, NI_NUMERICHOST | NI_NUMERICSERV);
			if (s == 0)
				LOGV("Received %ld bytes from %s:%s\n",
						(long) nread, host, service);
			else
				LOGE("getnameinfo: %s\n", gai_strerror(s));

			packet_head.m_nCommand = ntohs(packet_head.m_nCommand);

			LOGV("UDP %d\n", packet_head.m_nCommand);
			if (packet_head.m_nCommand == BS_FIND_CLIENT) {
				LOGV("UDP received: packet_head.m_nCommand == BS_FIND_CLIENT.");
				if (datagram_packing(sfd, (struct sockaddr *) &peer_addr, peer_addr_len) == -1)
					continue;
				else
					LOGV("UDP replied.");
			} else if (packet_head.m_nCommand == SC_HIBERNATING) {

			} else if (packet_head.m_nCommand == SC_WORKING) {

			}
		}
	}

	close(sfd);

	return 0;
}

static int is_lookback_interface(int sfd, struct ifreq *ifreq)
{
	if (ioctl(sfd, SIOCGIFFLAGS, ifreq) == -1) {
		LOGE("ioctl: %s\n", strerror(errno));
		return -1;
	}

	if ( ((ifreq->ifr_flags & IFF_LOOPBACK) == 0) && (ifreq->ifr_flags & IFF_UP))
		return 0;

	return -1;
}

static char *get_hwaddr_str(int sfd)
{
#define MAX_IF 6
	struct ifconf ifc;
	struct ifreq ifr[MAX_IF];
	uint32_t if_num;

	ifc.ifc_len = sizeof(ifr);
	ifc.ifc_buf = (void *) ifr;

	if ( (ioctl(sfd, SIOCGIFCONF, &ifc)) == -1)
		return NULL;

	if_num = ifc.ifc_len / sizeof(struct ifreq);

	while (if_num-- > 0) {
		if (ifr[if_num].ifr_addr.sa_family == AF_INET) {
			LOGV("name: %s\n", ifr[if_num].ifr_name);
			if (!is_lookback_interface(sfd, &ifr[if_num])) {
				if (ioctl(sfd, SIOCGIFHWADDR, &ifr[if_num]) != -1) {
					char *hwaddr = (char *) malloc(sizeof(ifr[if_num].ifr_hwaddr.sa_data));
					if (hwaddr == NULL)
						return NULL;
					memset(hwaddr, 0, sizeof(*hwaddr));
					snprintf(hwaddr, 18, "%02x:%02x:%02x:%02x:%02x:%02x",
							(uint8_t)ifr[if_num].ifr_hwaddr.sa_data[0],
							(uint8_t)ifr[if_num].ifr_hwaddr.sa_data[1],
							(uint8_t)ifr[if_num].ifr_hwaddr.sa_data[2],
							(uint8_t)ifr[if_num].ifr_hwaddr.sa_data[3],
							(uint8_t)ifr[if_num].ifr_hwaddr.sa_data[4],
							(uint8_t)ifr[if_num].ifr_hwaddr.sa_data[5]);

					LOGV("hwaddr: %s\n", hwaddr);
					return hwaddr;
				}
				break;
			}
		}
	}

	return NULL;
}

static int datagram_packing(int sfd, struct sockaddr *dest_addr, socklen_t addrlen)
{
	ssize_t send_num;
	char send_data[SEND_BUF_SIZE];
	BSQ_NAMEPOSITION info;
	char *index;
	PACKET_HEAD packet_head = { 0 };
	JNIEnv *env = NULL;
	jstring _name = NULL;
	jstring _position = NULL;
	const char *name = NULL;
	const char *position = NULL;
	char *hwaddr = NULL;

	env = ad_get_jni_env();

	sys_info.getInstance = (*env)->GetStaticMethodID(env, sys_info._class, "getInstance", "()Lcom/bsq/advertise/util/SystemInfo;");
	if (sys_info.getInstance == NULL) {
		LOGE("could not find getInstance method");
		return -1;
	}

	sys_info.object = (*env)->CallStaticObjectMethod(env, sys_info._class, sys_info.getInstance);
	if (sys_info.object == NULL) {
		LOGE("could not find object");
		return -1;
	}

	sys_info.getName = (*env)->GetMethodID(env, sys_info._class, "getName", "()Ljava/lang/String;");
	if (sys_info.getName == NULL) {
		LOGE("could not find getName method");
		return -1;
	}

	sys_info.getPosition = (*env)->GetMethodID(env, sys_info._class, "getPosition", "()Ljava/lang/String;");
	if (sys_info.getName == NULL) {
		LOGE("could not find getPosition method");
		return -1;
	}

	_name = (*env)->CallObjectMethod(env, sys_info.object, sys_info.getName);
	handle_jni_error(goto error);
	_position = (*env)->CallObjectMethod(env, sys_info.object, sys_info.getPosition);
	handle_jni_error(goto error);

	if (_name != NULL)
		name = (*env)->GetStringUTFChars(env, _name, NULL);
	if (_position != NULL)
		position = (*env)->GetStringUTFChars(env, _position, NULL);

	memset(send_data, 0, SEND_BUF_SIZE);
	memset(&info, 0, sizeof(BSQ_NAMEPOSITION));

	index = send_data;
	packet_head.m_nCommand = htobe16(CS_RETURN);
	packet_head.m_nDataLen = htobe32(sizeof(BSQ_NAMEPOSITION) + 18);

	if (name != NULL)
		memcpy(info.szName, name, strlen(name));
	if (position != NULL)
		memcpy(info.szPosition, position, strlen(position));

	memcpy(send_data, &packet_head, sizeof(PACKET_HEAD));
	index += sizeof(PACKET_HEAD);
	memcpy(index, &info, sizeof(info));
	index += sizeof(info);


	hwaddr = get_hwaddr_str(sfd);
	if (!hwaddr) {
		LOGV("Can't get hwaddr.");
		goto error;
	}
	strncpy(index, hwaddr, strlen(hwaddr) + 1);
	index += (strlen(hwaddr) + 1);

	((struct sockaddr_in *) dest_addr)->sin_port = htons(UDP_SEND_PORT);
	if ( (send_num = sendto(sfd, (const void *) send_data, index - send_data, 0, dest_addr, addrlen)) == -1) {
		LOGE("send(): %s\n", strerror(errno));
		goto error;
	}

	LOGV("udp send bytes: %u\n", send_num);

	if (sys_info.object)
		(*env)->DeleteLocalRef(env, sys_info.object);
	if (_name != NULL && name != NULL) {
		(*env)->ReleaseStringUTFChars(env, _name, name);
		(*env)->DeleteLocalRef(env, _name);
	}
	if (_position != NULL && position != NULL) {
		(*env)->ReleaseStringUTFChars(env, _position, position);
		(*env)->DeleteLocalRef(env, _position);
	}
	if (hwaddr)
		free(hwaddr);


	return 0;

	error:
	if (sys_info.object)
		(*env)->DeleteLocalRef(env, sys_info.object);
	if (_name != NULL && name != NULL) {
		(*env)->ReleaseStringUTFChars(env, _name, name);
		(*env)->DeleteLocalRef(env, _name);
	}
	if (_position != NULL && position != NULL) {
		(*env)->ReleaseStringUTFChars(env, _position, position);
		(*env)->DeleteLocalRef(env, _position);
	}
	if (hwaddr)
		free(hwaddr);

	return -1;
}

int get_eth0_hwaddr(char hwaddr[])
{
	int sfd;

	sfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sfd == -1) {
		LOGE("socket(): %s\n", strerror(errno));
		return -1;
	}

	struct ifreq ifr;
	memset(&ifr, 0, sizeof(ifr));
	strcpy(ifr.ifr_name, "eth0");

	if (ioctl(sfd, SIOCGIFHWADDR, &ifr) == -1) {
		LOGE("ioctl(): %s\n", strerror(errno));
		return -1;
	}

	snprintf(hwaddr, 18, "%02x:%02x:%02x:%02x:%02x:%02x",
			(uint8_t)ifr.ifr_hwaddr.sa_data[0],
			(uint8_t)ifr.ifr_hwaddr.sa_data[1],
			(uint8_t)ifr.ifr_hwaddr.sa_data[2],
			(uint8_t)ifr.ifr_hwaddr.sa_data[3],
			(uint8_t)ifr.ifr_hwaddr.sa_data[4],
			(uint8_t)ifr.ifr_hwaddr.sa_data[5]);

	//	LOGV("hwaddr: %s\n", hwaddr);

	if (close(sfd) == -1) {
		LOGE("close(): %s\n", strerror(errno));
		return -1;
	}

	return 0;
}

ssize_t getline(char **lineptr, size_t *n, FILE *stream)
{
	char *ptr;

	ptr = fgetln(stream, n);

	if (ptr == NULL) {
		return -1;
	}

	/* Free the original ptr */
	if (*lineptr != NULL) free(*lineptr);

	/* Add one more space for '\0' */
	size_t len = n[0] + 1;

	/* Update the length */
	n[0] = len;

	/* Allocate a new buffer */
	*lineptr = malloc(len);

	/* Copy over the string */
	memcpy(*lineptr, ptr, len-1);

	/* Write the NULL character */
	(*lineptr)[len-1] = '\0';

	/* Return the length of the new buffer */
	return len;
}

char *get_cipher()
{
	MD5_CTX ctx;
	char cipher_data[64] = {'\0'};
	uint8_t cipher[MD5_DIGEST_LENGTH] = {'\0'};
	int i;
	char tmp[3] = {'\0'};
	char hwaddr[18] = {'\0'}; // include '\0'
	FILE *fp;
	char *line = NULL;
	size_t len = 0;
	char cpuid[33] = {'\0'};

	fp = fopen("/proc/cpuinfo", "r");
	if (fp == NULL)
		return NULL;

	while (getline(&line, &len, fp) != -1) {
		if (strncmp(line, "Serial", strlen("Serial")) == 0) {
			strncat(cpuid, strchr(line, ':'), 32); // Not standard cpuid, include sundries for security
			break;
		}
	}

	free(line);
	if (get_eth0_hwaddr(hwaddr) != -1)
		;//strncpy(cipher_data, hwaddr, strlen(hwaddr));
	else
		return NULL;
	strncat(cipher_data, cpuid, strlen(cpuid));

	char *ret = (char *) malloc(33);
	if (ret == NULL) {
		LOGE("OutOfMemory");
		return NULL;
	}
	memset(ret, 0, 33);

	MD5_Init(&ctx);
	MD5_Update(&ctx, cipher_data, strlen(cipher_data));
	MD5_Final(cipher, &ctx);

	for (i = 0; i < 16; i++) {
		sprintf(tmp, "%2.2X", cipher[i]);
		strcat(ret, tmp);
	}

	return ret;
}

int verify_cipher(const char *cipher)
{
	MD5_CTX ctx;
	char re_encrypt[64] = {'\0'};
	uint8_t md[MD5_DIGEST_LENGTH] = {'\0'};
	char tmp[3] = {'\0'};
	int i;

	const char *org = get_cipher();
	if (org != NULL) {
		strncat(re_encrypt, " ", 1);
		strncat(re_encrypt, org, strlen(org));
		strncat(re_encrypt, " ", 1);

		MD5_Init(&ctx);
		MD5_Update(&ctx, re_encrypt, strlen(re_encrypt));
		MD5_Final(md, &ctx);

		memset(re_encrypt, 0, 64);

		for (i = 0; i < 16; i++) {
			sprintf(tmp, "%2.2X", md[i]);
			strcat(re_encrypt, tmp);
		}

		return strncasecmp(cipher, re_encrypt, strlen(re_encrypt));
	} else
		return -1;
}
