#include <jni.h>

extern "C" {
#include "../include/bsq_net.h"
}

#ifdef ANDROID
#include <android/log.h>
#endif

static JavaVM *jvm=0;

#ifdef ANDROID
FTPClient ftp_client = { 0 };
SYSINFO sys_info = { 0 };
char sdcardPath[32] = { 0 };

static int sys_info_init(void)
{
	JNIEnv *jenv = ad_get_jni_env();
	if (jenv == NULL) {
		LOGE("ad_get_jni_env failed\n");
		return -1;
	}

	sys_info._class = jenv->FindClass("com/bsq/advertise/util/SystemInfo");
	if (sys_info._class == 0) {
		LOGE("could not find com/bsq/advertise/util/SystemInfo");
		return -1;
	}
	sys_info._class = (jclass)(jenv->NewGlobalRef(sys_info._class));

	sys_info.getSdcardPath = jenv->GetStaticMethodID(sys_info._class, "getSdcardPath", "()Ljava/lang/String;");
	if (sys_info.getSdcardPath == NULL) {
		LOGE("Could't find getSdcardPath method");
		return -1;
	}
	jstring sp = (jstring)jenv->CallStaticObjectMethod(sys_info._class, sys_info.getSdcardPath);
	if (sp != NULL) {
		const char *sp_str = jenv->GetStringUTFChars(sp, NULL);
		memcpy(sdcardPath, sp_str, strlen(sp_str));
		jenv->ReleaseStringUTFChars(sp, sp_str);
	}

	return 0;
}

static int ftp_client_init(void)
{
	JNIEnv *jenv = ad_get_jni_env();
	if (jenv == NULL) {
		LOGE("ad_get_jni_env failed\n");
		return -1;
	}

	ftp_client._class = jenv->FindClass("com/bsq/advertise/FTPClient");
	if (ftp_client._class == 0) {
		LOGE("could not find com/bsq/advertise/FTPClient class !");
		return -1;
	}
	ftp_client._class = (jclass)(jenv->NewGlobalRef(ftp_client._class));

	return 0;
}
#endif

JNIEXPORT jint JNICALL  JNI_OnLoad(JavaVM *ajvm, void *reserved)
{
#ifdef ANDROID
	ad_set_jvm(ajvm);
#endif
	jvm=ajvm;
	ftp_client_init();
	sys_info_init();
	return JNI_VERSION_1_4;
}

#ifdef __cplusplus
extern "C" {
#endif

#ifdef ANDROID

JNIEXPORT jint JNICALL Java_com_bsq_advertise_BSQNet_UDP_1Creation(JNIEnv *env, jobject obj)
{
	if (udp_creation() == -1) {
		LOGE("udp_creation() failed\n");
		return -1;
	}

	return 0;
}

JNIEXPORT void JNICALL Java_com_bsq_advertise_BSQNet_UDP_1Destory
(JNIEnv *, jobject)
{
	udp_destory();
}

JNIEXPORT jint JNICALL Java_com_bsq_advertise_BSQNet_TCP_1Creation
(JNIEnv *, jobject)
{
	if (tcp_server_creation() == -1) {
		LOGE("tcp_creation() failed\n");
		return -1;
	}

	return 0;
}

JNIEXPORT void JNICALL Java_com_bsq_advertise_BSQNet_TCP_1Destory
(JNIEnv *, jobject)
{
	tcp_destory();
}

JNIEXPORT void JNICALL Java_com_bsq_advertise_BSQNet_TCP_1reply
(JNIEnv *env, jobject object, jstring msg)
{
	const char *tmp = env->GetStringUTFChars(msg, NULL);
	if (tmp == NULL) {
		LOGE("Can't get tcp reply msg\n");
		return;
	}

	jsize len = env->GetStringUTFLength(msg);

	if (tcp_reply(tmp, len) == -1) {
		LOGE("Can't send tcp reply msg\n");
	}
	// cleanup
	env->ReleaseStringUTFChars(msg, tmp);
}

/*
 * Class:     com_bsq_advertise_BSQNet
 * Method:    getCipher
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_bsq_advertise_BSQNet_getCipher
(JNIEnv *env, jobject obj)
{
	char *cipher_cstr = get_cipher();
	jstring ret = env->NewStringUTF(cipher_cstr);
	free(cipher_cstr);

	return ret;
}

/*
 * Class:     com_bsq_advertise_BSQNet
 * Method:    verifyCipher
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_com_bsq_advertise_BSQNet_verifyCipher
(JNIEnv *env, jobject obj, jstring cipher)
{
	if (cipher == NULL)
		return false;

	const char *cipher_cstr = env->GetStringUTFChars(cipher, NULL);
	if (cipher_cstr == NULL) {
		LOGE("unregistered\n");
		return false;
	}

	if (verify_cipher(cipher_cstr) != 0) {
		LOGE("unregisteredd\n");
		return false;
	}

	env->ReleaseStringUTFChars(cipher, cipher_cstr);

	return true;
}

JNIEXPORT jboolean JNICALL Java_com_bsq_advertise_BSQNet_updateAPK
  (JNIEnv *env, jobject obj, jstring path)
{
	if (path == NULL)
		return false;

	const char *apk_path = env->GetStringUTFChars(path, NULL);
	if (apk_path == NULL) {
		LOGE("apk_path is NULL");
		return false;
	}

	if (apk_install(apk_path) == -1) {
		LOGE("install %s failed", apk_path);
		return false;
	}

	env->ReleaseStringUTFChars(path, apk_path);

	return true;
}

#endif

#ifdef __cplusplus
}
#endif
