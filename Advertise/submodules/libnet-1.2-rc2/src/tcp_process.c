#include "../include/bsq_net.h"
#include "../include/smpcmd.h"

typedef struct {
	int sfd;
	struct sockaddr peer_addr;
	socklen_t peer_addr_len;
} CONNECTIONN_INFO;

#define FTP_DEFAULT_PORT 21
#define FTPS_DEFAULT_PORT 990

static int tcp_holding = true;
static int global_sfd = -1;

#define TCP_SEND_BUF_SIZE 1024

extern FTPClient ftp_client;
extern SYSINFO sys_info;
extern char sdcardPath[];
static int rm_dir(const char *pathname);

int tcp_reply(const char *msg, size_t msg_len)
{
	ssize_t len;
	uint8_t send_data[TCP_SEND_BUF_SIZE] = { 0 };
	uint8_t *index = NULL;
	PACKET_HEAD packet_head = { 0 };
	BSQ_ERROR_INFO bsq_error_info = { 0 };

	index = send_data;
	packet_head.m_nCommand = htobe16(CS_RETURN);
	packet_head.m_nDataLen = htobe32(sizeof(BSQ_ERROR_INFO));
	memcpy(index, &packet_head, sizeof(PACKET_HEAD));
	index += sizeof(PACKET_HEAD);

	bsq_error_info.nErrorCode = htobe64(0);
	strncpy(bsq_error_info.szErrorString, msg, msg_len);
	memcpy(index, &bsq_error_info, sizeof(BSQ_ERROR_INFO));
	index += sizeof(BSQ_ERROR_INFO);

	if ((len = send(global_sfd, (void *) send_data, index - send_data, 0)) == -1) {
		LOGE("send(): %s\n", strerror(errno));
		return -1;
	}

	LOGV("tcp send: %ld\n", len);

	return 0;
}

static int is_lookback_interface(int sfd, struct ifreq *ifreq)
{
	if (ioctl(sfd, SIOCGIFFLAGS, ifreq) == -1) {
		LOGE("ioctl: %s\n", strerror(errno));
		return -1;
	}

	if ( ((ifreq->ifr_flags & IFF_LOOPBACK) == 0) && (ifreq->ifr_flags & IFF_UP))
		return 0;

	return -1;
}

char *get_ifr_name(void)
{
	int sfd;

	sfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sfd == -1) {
		LOGE("socket(): %s\n", strerror(errno));
		return NULL;
	}

#define MAX_IF 6
	struct ifconf ifc;
	struct ifreq ifr[MAX_IF];
	uint32_t if_num;

	ifc.ifc_len = sizeof(ifr);
	ifc.ifc_buf = (void *) ifr;

	if ( (ioctl(sfd, SIOCGIFCONF, &ifc)) == -1)
		return NULL;

	if_num = ifc.ifc_len / sizeof(struct ifreq);

	while (if_num-- > 0) {
		if (ifr[if_num].ifr_addr.sa_family == AF_INET) {
			if (!is_lookback_interface(sfd, &ifr[if_num])) {
				if ( (ioctl(sfd, SIOCGIFINDEX, &ifr[if_num])) == -1) {
					LOGE("ioctl(): %s\n", strerror(errno));
					return NULL;
				}

				if ( (ioctl(sfd, SIOCGIFNAME, &ifr[if_num])) == -1) {
					LOGE("ioctl(): %s\n", strerror(errno));
					return NULL;
				}

				char *ifr_name1 = (char *) malloc(IFNAMSIZ);
				if (ifr_name1 == NULL) {
					LOGE("OutOfMemory");
					return NULL;
				} // TODO free by the user
				memset(ifr_name1, 0, IFNAMSIZ);
				memcpy(ifr_name1, ifr[if_num].ifr_name, strlen(ifr[if_num].ifr_name));
				return ifr_name1;
			}
		}
	}

	return NULL;
}

int set_network_info(const char *ip_addr, const char *netmask)
{
	int sfd;
	struct ifreq ifr;

	sfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sfd == -1) {
		LOGE("socket(): %s\n", strerror(errno));
		return -1;
	}

	char *ifr_name1 = get_ifr_name();
	memcpy(ifr_name1, ifr.ifr_name, strlen(ifr_name1));
	free(ifr_name1);

	struct sockaddr_in *sin = (struct sockaddr_in *) &ifr.ifr_addr;
	inet_pton(AF_INET, ip_addr, &sin->sin_addr.s_addr);

	if ( (ioctl(sfd, SIOCSIFADDR, &ifr)) == -1) {
		LOGE("ioctl(): %s\n", strerror(errno));
		return -1;
	}

	sin = (struct sockaddr_in *) &ifr.ifr_netmask;
	inet_pton(AF_INET, netmask, &sin->sin_addr.s_addr);
	if ( (ioctl(sfd, SIOCSIFNETMASK, &ifr)) == -1) {
		LOGE("ioctl(): %s\n", strerror(errno));
		return -1;
	}

	sin = (struct sockaddr_in *) &ifr.ifr_netmask;
	inet_pton(AF_INET, netmask, &sin->sin_addr.s_addr);
	if ( (ioctl(sfd, SIOCSIFNETMASK, &ifr)) == -1) {
		LOGE("ioctl(): %s\n", strerror(errno));
		return -1;
	}

	close(sfd);

	return 0;
}

int set_netmask(const char *netmask)
{
	return 0;
}

int set_gateway(const char *gateway)
{
	return 0;
}

int set_dns(int8_t *dns)
{
	return 0;
}

static void *apk_install_routine(void *arg)
{
	if (pthread_detach(pthread_self()) != 0) {
		LOGE("pthread_detach() failed\n");
		return NULL;
	}

	if (system("sync") == -1)
		LOGE("system(): %s\n", strerror(errno));
	LOGV("sync done");

	const char *apk_path = (const char *) arg;
	pid_t pid;

	pid = fork();
	if (pid == 0) {
		if (execlp("pm", "pm", "install", "-r", apk_path, NULL) == -1) {
			LOGE("execv(): %s\n", strerror(errno));
			return NULL;
		}
	} else if (pid == -1) {
		LOGE("fork(): %s\n", strerror(errno));
		return NULL;
	}

	wait(NULL);

	return NULL;
}

int apk_install(const char *path)
{
	pthread_t pid;

	char *apk_path = (char *) malloc(strlen(path) + 1);
	if (apk_path == NULL) {
		LOGE("OutOfMemory");
		return -1;
	}
	memset(apk_path, 0, strlen(path) + 1);

	strncpy(apk_path, path, strlen(path));


	if (pthread_create(&pid, NULL, &apk_install_routine, (void *) apk_path) != 0) {
		LOGE("Unknown error");
	}

	return 0;
}

static int reply_client(int sfd, const void *received_data, ssize_t received_data_len)
{
	uint16_t cmd;
	PACKET_HEAD packet_head = { 0 };
	const char *data = NULL;
	JNIEnv *env = NULL;
	PACKET_HEAD packet_head_send = { 0 };
	char *send_data = NULL;
	size_t send_data_len = 0;
	int i;

	if (received_data_len < sizeof(PACKET_HEAD))
		return -1;

	memcpy((void *) &packet_head, received_data, sizeof(PACKET_HEAD));
	data = (const char *) received_data + sizeof(PACKET_HEAD);

	packet_head.m_nCommand = ntohs(packet_head.m_nCommand);
	packet_head.m_nDataLen = ntohl(packet_head.m_nDataLen);

	LOGV("received cmd: 0x%x  datalen: %u\n", packet_head.m_nCommand, packet_head.m_nDataLen);

	cmd = packet_head.m_nCommand;
	switch (cmd) {
	case SC_GET_SYS_INFO:
		LOGV("SC_GET_SYS_INFO");
		jstring _version = NULL;
		jstring _host_address = NULL;
		jstring _name = NULL;
		jstring _position = NULL;
		jint _volume = 0;
		jstring _system_time = NULL;
		jstring _diskinfo = NULL;
		const char *version = NULL;
		const char *host_address = NULL;
		const char *name = NULL;
		const char *position = NULL;
		const char *system_time = NULL;
		const char *diskinfo = NULL;
		BSQ_NAMEPOSITION np = { {0} };
		BSQ_ADVERTISE_INFO *system_info = NULL;

		global_sfd = sfd;
		env = ad_get_jni_env();
		sys_info.getVersion = (*env)->GetStaticMethodID(env, sys_info._class, "getVersion", "()Ljava/lang/String;");
		if (sys_info.getVersion == NULL) {
			LOGE("could not find getVersion method");
			goto get_system_info_cleanup;
		}

		sys_info.getVolume = (*env)->GetStaticMethodID(env, sys_info._class, "getVolume", "()I");
		if (sys_info.getVolume == NULL) {
			LOGE("could not find getVolume method");
			goto get_system_info_cleanup;
		}

		sys_info.getAddress = (*env)->GetStaticMethodID(env, sys_info._class, "getHostAddress", "()Ljava/lang/String;");
		if (sys_info.getAddress == NULL) {
			LOGE("could not find getAddress method");
			goto get_system_info_cleanup;
		}

		sys_info.getInstance = (*env)->GetStaticMethodID(env, sys_info._class, "getInstance", "()Lcom/bsq/advertise/util/SystemInfo;");
		if (sys_info.getInstance == NULL) {
			LOGE("could not find getInstance method");
			goto get_system_info_cleanup;
		}

		sys_info.getSystemTime = (*env)->GetStaticMethodID(env, sys_info._class, "getSystemTime", "()Ljava/lang/String;");
		if (sys_info.getSystemTime == NULL) {
			LOGE("Could not find getSystemTime method");
			goto get_system_info_cleanup;
		}

		sys_info.getDiskInfo = (*env)->GetStaticMethodID(env, sys_info._class, "getDiskInfo", "()Ljava/lang/String;");
		if (sys_info.getDiskInfo == NULL) {
			LOGE("Could not find getDiskInfo method");
			goto get_system_info_cleanup;
		}

		sys_info.object = (*env)->CallStaticObjectMethod(env, sys_info._class, sys_info.getInstance);
		if (sys_info.object == NULL) {
			LOGE("could not find object");
			goto get_system_info_cleanup;
		}

		sys_info.getName = (*env)->GetMethodID(env, sys_info._class, "getName", "()Ljava/lang/String;");
		if (sys_info.getName == NULL) {
			LOGE("could not find getName method");
			goto get_system_info_cleanup;
		}

		sys_info.getPosition = (*env)->GetMethodID(env, sys_info._class, "getPosition", "()Ljava/lang/String;");
		if (sys_info.getName == NULL) {
			LOGE("could not find getPosition method");
			goto get_system_info_cleanup;
		}

		_name = (*env)->CallObjectMethod(env, sys_info.object, sys_info.getName);
		handle_jni_error(goto get_system_info_cleanup);
		_position = (*env)->CallObjectMethod(env, sys_info.object, sys_info.getPosition);
		handle_jni_error(goto get_system_info_cleanup);
		_version = (*env)->CallStaticObjectMethod(env, sys_info._class, sys_info.getVersion);
		handle_jni_error(goto get_system_info_cleanup);
		_host_address = (*env)->CallStaticObjectMethod(env, sys_info._class, sys_info.getAddress);
		handle_jni_error(goto get_system_info_cleanup);
		_volume = (*env)->CallStaticIntMethod(env, sys_info._class, sys_info.getVolume);
		handle_jni_error(goto get_system_info_cleanup);
		_system_time = (*env)->CallStaticObjectMethod(env, sys_info._class, sys_info.getSystemTime);
		handle_jni_error(goto get_system_info_cleanup);
		_diskinfo = (*env)->CallStaticObjectMethod(env, sys_info._class, sys_info.getDiskInfo);
		handle_jni_error(goto get_system_info_cleanup);

		if (_name != NULL)
			name = (*env)->GetStringUTFChars(env, _name, NULL);
		if (_position != NULL)
			position = (*env)->GetStringUTFChars(env, _position, NULL);
		if (_version != NULL)
			version = (*env)->GetStringUTFChars(env, _version, NULL);
		if (_host_address != NULL)
			host_address = (*env)->GetStringUTFChars(env, _host_address, NULL);
		if (_system_time != NULL)
			system_time = (*env)->GetStringUTFChars(env, _system_time, NULL);
		if (_diskinfo != NULL)
			diskinfo = (*env)->GetStringUTFChars(env, _diskinfo, NULL);

		memset(&packet_head_send, 0, sizeof(PACKET_HEAD));
		packet_head_send.m_nCommand = htobe16(CS_RETURN);
		packet_head_send.m_nDataLen = htobe32(sizeof(BSQ_ADVERTISE_INFO));

		system_info = (BSQ_ADVERTISE_INFO *) malloc(sizeof(BSQ_ADVERTISE_INFO));
		if (system_info == NULL) {
			LOGE("Out of memory.");
			goto get_system_info_cleanup;
		}
		memset(system_info, 0, sizeof(BSQ_ADVERTISE_INFO));

		if (name != NULL)
			strncpy(np.szName, name, strlen(name));
		if (position != NULL)
			strncpy(np.szPosition, position, strlen(position));
		memcpy(&system_info->namePos, &np, sizeof(BSQ_NAMEPOSITION));
		if (version != NULL)
			strncpy(system_info->version, version, strlen(version));
		if (host_address != NULL)
			strncpy(system_info->szIPAddress, host_address, strlen(host_address));
		system_info->volume = htonl(_volume);
		if (system_time != NULL)
			strncpy(system_info->time, system_time, strlen(system_time));
		if (diskinfo != NULL)
			strncpy(system_info->diskInfo, diskinfo, strlen(diskinfo));

		send_data_len = sizeof(BSQ_ADVERTISE_INFO) + sizeof(PACKET_HEAD);
		send_data = (char *) malloc(send_data_len);
		if (send_data == NULL) {
			LOGE("Out of memory.");
			goto get_system_info_cleanup;
		}

		memset(send_data, 0, send_data_len);
		memcpy(send_data, &packet_head_send, sizeof(PACKET_HEAD));
		memcpy(send_data + sizeof(PACKET_HEAD), system_info, sizeof(BSQ_ADVERTISE_INFO));

		if (send(sfd, (const char *) send_data, send_data_len, 0) == -1) {
			LOGE("send(): %s\n", strerror(errno));
			goto get_system_info_cleanup;
		}

		if (sys_info.object)
			(*env)->DeleteLocalRef(env, sys_info.object);
		if (_name != NULL && name != NULL) {
			(*env)->ReleaseStringUTFChars(env, _name, name);
			(*env)->DeleteLocalRef(env, _name);
		}
		if (_position != NULL && position != NULL) {
			(*env)->ReleaseStringUTFChars(env, _position, position);
			(*env)->DeleteLocalRef(env, _position);
		}
		if (_version != NULL && version != NULL) {
			(*env)->ReleaseStringUTFChars(env, _version, version);
			(*env)->DeleteLocalRef(env, _version);
		}
		if (_host_address != NULL && host_address != NULL) {
			(*env)->ReleaseStringUTFChars(env, _host_address, host_address);
			(*env)->DeleteLocalRef(env, _host_address);
		}
		if (_system_time != NULL && system_time != NULL) {
			(*env)->ReleaseStringUTFChars(env, _system_time, system_time);
			(*env)->DeleteLocalRef(env, _system_time);
		}
		if (_diskinfo != NULL && diskinfo != NULL) {
			(*env)->ReleaseStringUTFChars(env, _diskinfo, diskinfo);
			(*env)->DeleteLocalRef(env, _system_time);
		}

		if (system_info != NULL)
			free(system_info);
		if (send_data != NULL)
			free(send_data);

		break;

		get_system_info_cleanup:
		if (sys_info.object)
			(*env)->DeleteLocalRef(env, sys_info.object);
		if (_name != NULL && name != NULL) {
			(*env)->ReleaseStringUTFChars(env, _name, name);
			(*env)->DeleteLocalRef(env, _name);
		}
		if (_position != NULL && position != NULL) {
			(*env)->ReleaseStringUTFChars(env, _position, position);
			(*env)->DeleteLocalRef(env, _position);
		}
		if (_version != NULL && version != NULL) {
			(*env)->ReleaseStringUTFChars(env, _version, version);
			(*env)->DeleteLocalRef(env, _version);
		}
		if (_host_address != NULL && host_address != NULL) {
			(*env)->ReleaseStringUTFChars(env, _host_address, host_address);
			(*env)->DeleteLocalRef(env, _host_address);
		}
		if (_system_time != NULL && system_time != NULL) {
			(*env)->ReleaseStringUTFChars(env, _system_time, system_time);
			(*env)->DeleteLocalRef(env, _system_time);
		}
		if (_diskinfo != NULL && diskinfo != NULL) {
			(*env)->ReleaseStringUTFChars(env, _diskinfo, diskinfo);
			(*env)->DeleteLocalRef(env, _system_time);
		}
		if (system_info != NULL)
			free(system_info);
		if (send_data != NULL)
			free(send_data);
		tcp_reply("Get info failed, retry", strlen("Get info failed, retry"));

		return -1;
	case SC_SET_INFOR:
		LOGV("SC_SET_INFOR");
		global_sfd = sfd;
		env = ad_get_jni_env();

		sys_info.getInstance = (*env)->GetStaticMethodID(env, sys_info._class, "getInstance", "()Lcom/bsq/advertise/util/SystemInfo;");
		if (sys_info.getInstance == NULL) {
			LOGE("could not find getInstance method");
			goto set_infor_cleanup;
		}

		sys_info.object = (*env)->CallStaticObjectMethod(env, sys_info._class, sys_info.getInstance);
		if (sys_info.object == NULL) {
			LOGE("could not find object");
			goto set_infor_cleanup;
		}

		sys_info.setInfoR = (*env)->GetMethodID(env, sys_info._class, "setInfoR", "(Ljava/lang/String;Ljava/lang/String;)V");
		if (sys_info.setInfoR == NULL) {
			LOGE("could not find getName method");
			goto set_infor_cleanup;
		}

		memset(&np, 0, sizeof(BSQ_NAMEPOSITION));
		memcpy(&np, data, sizeof(BSQ_NAMEPOSITION));

		(*env)->CallVoidMethod(env, sys_info.object, sys_info.setInfoR, (*env)->NewStringUTF(env, np.szName), (*env)->NewStringUTF(env, np.szPosition));
		handle_jni_error(goto set_infor_cleanup);

		if (sys_info.object)
			(*env)->DeleteLocalRef(env, sys_info.object);

		tcp_reply("Set successed", strlen("Set successed"));
		break;

		set_infor_cleanup:
		if (sys_info.object)
			(*env)->DeleteLocalRef(env, sys_info.object);
		tcp_reply("Set failed, retry", strlen("Set failed, retry"));
	case SC_SET_VOLUME:
		LOGV("SC_SET_VOLUME");
		global_sfd = sfd;
		env = ad_get_jni_env();

		sys_info.getInstance = (*env)->GetStaticMethodID(env, sys_info._class, "getInstance", "()Lcom/bsq/advertise/util/SystemInfo;");
		if (sys_info.getInstance == NULL) {
			LOGE("could not find getInstance method");
			goto set_infor_cleanup;
		}

		sys_info.setVolume = (*env)->GetStaticMethodID(env, sys_info._class, "setVolume", "(I)V");
		if (sys_info.setVolume == NULL) {
			LOGE("could not find setVolume method");
			goto set_volume_cleanup;
		}

		sys_info.object = (*env)->CallStaticObjectMethod(env, sys_info._class, sys_info.getInstance);
		if (sys_info.object == NULL) {
			LOGE("could not find object");
			goto set_network_cleanup;
		}

		sys_info.writeConfig = (*env)->GetMethodID(env, sys_info._class, "writeConfig", "()V");
		if (sys_info.writeConfig == NULL) {
			LOGE("Could not find writeConfig method.");
			goto set_network_cleanup;
		}

		PACKET_SET_VOLUME recv_data = { 0 };
		memcpy(&recv_data, received_data, received_data_len);
		jint volume = ntohl(recv_data.volume);

		(*env)->CallStaticVoidMethod(env, sys_info._class, sys_info.setVolume, volume);
		handle_jni_error(goto set_volume_cleanup);

		(*env)->CallVoidMethod(env, sys_info.object, sys_info.writeConfig);
		handle_jni_error(goto set_network_cleanup);

		if (sys_info.object)
			(*env)->DeleteLocalRef(env, sys_info.object);
		tcp_reply("Set volume successed", strlen("Set volume successed"));
		break;

		set_volume_cleanup:
		if (sys_info.object)
			(*env)->DeleteLocalRef(env, sys_info.object);
		tcp_reply("Set volume failed", strlen("Set volume failed"));
		return -1;
	case SC_SET_NETWORK:
		LOGV("SC_SET_NETWORK");
		global_sfd = sfd;
		env = ad_get_jni_env();
		BSQ_NETWORK_INFO net_info = { { 0 } };

		// Copy the data from controller
		memcpy(&net_info, data, sizeof(BSQ_NETWORK_INFO));

		sys_info.getInstance = (*env)->GetStaticMethodID(env, sys_info._class, "getInstance", "()Lcom/bsq/advertise/util/SystemInfo;");
		if (sys_info.getInstance == NULL) {
			LOGE("could not find getInstance method");
			goto set_network_cleanup;
		}

		sys_info.object = (*env)->CallStaticObjectMethod(env, sys_info._class, sys_info.getInstance);
		if (sys_info.object == NULL) {
			LOGE("could not find object");
			goto set_network_cleanup;
		}
		sys_info.Setip = (*env)->GetMethodID(env, sys_info._class, "setIp", "(Ljava/lang/String;)V");
		if (sys_info.Setip == NULL) {
			LOGE("Could not find Setip method.");
			goto set_network_cleanup;
		}
		sys_info.SetNetmask = (*env)->GetMethodID(env, sys_info._class, "setNetmask", "(Ljava/lang/String;)V");
		if (sys_info.SetNetmask == NULL) {
			LOGE("Could not find SetNetmask method.");
			goto set_network_cleanup;
		}
		sys_info.SetGateway = (*env)->GetMethodID(env, sys_info._class, "setGateway", "(Ljava/lang/String;)V");
		if (sys_info.SetGateway == NULL) {
			LOGE("Could not find SetGateway method.");
			goto set_network_cleanup;
		}
		sys_info.SetDNS = (*env)->GetMethodID(env, sys_info._class, "setDns", "(Ljava/lang/String;)V");
		if (sys_info.SetDNS == NULL) {
			LOGE("Could not find SetDNS method.");
			goto set_network_cleanup;
		}
		sys_info.setSSID = (*env)->GetMethodID(env, sys_info._class, "setSsid", "(Ljava/lang/String;)V");
		if (sys_info.setSSID == NULL) {
			LOGE("Could not find setSSID method.");
			goto set_network_cleanup;
		}
		sys_info.setPassword = (*env)->GetMethodID(env, sys_info._class, "setPassword", "(Ljava/lang/String;)V");
		if (sys_info.setPassword == NULL) {
			LOGE("Could not find setPassword method.");
			goto set_network_cleanup;
		}
		sys_info.setHttpServer = (*env)->GetMethodID(env, sys_info._class, "setHttpServer", "(Ljava/lang/String;)V");
		if (sys_info.setHttpServer == NULL) {
			LOGE("Could not find setHttpServer method.");
			goto set_network_cleanup;
		}
		sys_info.setIs3G = (*env)->GetMethodID(env, sys_info._class, "setIs3g", "(I)V");
		if (sys_info.setIs3G == NULL) {
			LOGE("Could not find setIs3G method.");
			goto set_network_cleanup;
		}
		sys_info.writeConfig = (*env)->GetMethodID(env, sys_info._class, "writeConfig", "()V");
		if (sys_info.writeConfig == NULL) {
			LOGE("Could not find writeConfig method.");
			goto set_network_cleanup;
		}

		(*env)->CallVoidMethod(env, sys_info.object, sys_info.Setip, (*env)->NewStringUTF(env, net_info.ip));
		handle_jni_error(goto set_network_cleanup);
		(*env)->CallVoidMethod(env, sys_info.object, sys_info.SetNetmask, (*env)->NewStringUTF(env, net_info.mask));
		handle_jni_error(goto set_network_cleanup);
		(*env)->CallVoidMethod(env, sys_info.object, sys_info.SetGateway, (*env)->NewStringUTF(env, net_info.gateway));
		handle_jni_error(goto set_network_cleanup);
		(*env)->CallVoidMethod(env, sys_info.object, sys_info.SetDNS, (*env)->NewStringUTF(env, net_info.dns));
		handle_jni_error(goto set_network_cleanup);
		(*env)->CallVoidMethod(env, sys_info.object, sys_info.setSSID, (*env)->NewStringUTF(env, net_info.ssid));
		handle_jni_error(goto set_network_cleanup);
		(*env)->CallVoidMethod(env, sys_info.object, sys_info.setPassword, (*env)->NewStringUTF(env, net_info.pwd));
		handle_jni_error(goto set_network_cleanup);
		(*env)->CallVoidMethod(env, sys_info.object, sys_info.setHttpServer, (*env)->NewStringUTF(env, net_info.server));
		handle_jni_error(goto set_network_cleanup);
		(*env)->CallVoidMethod(env, sys_info.object, sys_info.setIs3G, net_info.threeG);
		handle_jni_error(goto set_network_cleanup);
		(*env)->CallVoidMethod(env, sys_info.object, sys_info.writeConfig);
		handle_jni_error(goto set_network_cleanup);

		set_network_info(net_info.ip, net_info.mask);

		if (sys_info.object)
			(*env)->DeleteLocalRef(env, sys_info.object);
		tcp_reply("Set network info successed", strlen("Set network info successed"));
		break;

		set_network_cleanup:
		if (sys_info.object)
			(*env)->DeleteLocalRef(env, sys_info.object);
		tcp_reply("Set network info failed", strlen("Set network info failed"));
		return -1;
	case SC_SET_CLOCK:
		global_sfd = sfd;
		env = ad_get_jni_env();
		BSQ_SYSTEM_TIME st = { 0 };
		memcpy(&st, data, sizeof(BSQ_SYSTEM_TIME));

		sys_info.setSystemTime = (*env)->GetStaticMethodID(env, sys_info._class, "setSystemTime", "(IIIIII)V");
		if (sys_info.setSystemTime == NULL) {
			LOGE("Could't find setSystemTime method");
			goto set_clock_cleanup;
		}

		(*env)->CallStaticVoidMethod(env, sys_info._class, sys_info.setSystemTime, ntohl(st.Year), ntohl(st.Month), ntohl(st.Day),
				ntohl(st.Hour), ntohl(st.Minute), ntohl(st.Second));
		handle_jni_error(goto set_clock_cleanup);

		tcp_reply("Set system time successed", strlen("Set system time successed"));
		break;

		set_clock_cleanup:
		tcp_reply("Set system time failed", strlen("Set system time failed"));
		return -1;

	case SC_UPGRADE_PROGRAM:
		global_sfd = sfd;
		env = ad_get_jni_env();
		jstring apk_url = (*env)->NewStringUTF(env, data);
		pid_t pid;
		jstring _apk_path = NULL;
		const char *apk_path = NULL;

		ftp_client.getInstance = (*env)->GetStaticMethodID(env, ftp_client._class, "getInstance", "()Lcom/bsq/advertise/FTPClient;");
		if (ftp_client.getInstance == NULL) {
			LOGE("could not find getInstance method");
			goto upgrade_program_cleanup;
		}

		ftp_client.object = (*env)->CallStaticObjectMethod(env, ftp_client._class, ftp_client.getInstance);
		if (ftp_client.object == NULL) {
			LOGE("could not find object");
			goto upgrade_program_cleanup;
		}

		ftp_client.downloadAPK = (*env)->GetMethodID(env, ftp_client._class, "downloadAPK", "(Ljava/lang/String;)Ljava/lang/String;");
		if (ftp_client.downloadAPK == NULL) {
			LOGE("could not find downloadAPK method");
			goto upgrade_program_cleanup;
		}

		_apk_path = (*env)->CallObjectMethod(env, ftp_client.object, ftp_client.downloadAPK, apk_url);
		handle_jni_error(goto set_network_cleanup);

		if (_apk_path != NULL)
			apk_path = (*env)->GetStringUTFChars(env, _apk_path, NULL);

		pid = fork();
		if (pid == 0) {
			tcp_reply("upgrade app successed", strlen("upgrade app successed"));
			if (execlp("pm", "pm", "install", "-r", apk_path, NULL) == -1) {
				LOGE("execv(): %s\n", strerror(errno));
				goto upgrade_program_cleanup;
			}
		} else if (pid == -1) {
			LOGE("fork(): %s\n", strerror(errno));
			goto upgrade_program_cleanup;
		}

		wait(NULL);

		if (_apk_path != NULL && apk_path != NULL) {
			(*env)->ReleaseStringUTFChars(env, _apk_path, apk_path);
			(*env)->DeleteLocalRef(env, _apk_path);
		}

		if (sys_info.object)
			(*env)->DeleteLocalRef(env, sys_info.object);
		tcp_reply("upgrade app successed", strlen("upgrade app successed"));
		break;

		upgrade_program_cleanup:
		if (_apk_path != NULL && apk_path != NULL) {
			(*env)->ReleaseStringUTFChars(env, _apk_path, apk_path);
			(*env)->DeleteLocalRef(env, _apk_path);
		}
		if (sys_info.object)
			(*env)->DeleteLocalRef(env, sys_info.object);
		tcp_reply("upgrade app failed", strlen("upgrade app failed"));
		return -1;
	case SC_FRESH_XML:
		env = ad_get_jni_env();
		jstring url = (*env)->NewStringUTF(env, data);

		ftp_client.startProcess = (*env)->GetMethodID(env, ftp_client._class, "startProcess", "(Ljava/lang/String;)V");
		if (ftp_client.startProcess == NULL) {
			LOGE("could not find startProcess method");
			goto fresh_xml_cleanup;
		}
		ftp_client.getInstance = (*env)->GetStaticMethodID(env, ftp_client._class, "getInstance", "()Lcom/bsq/advertise/FTPClient;");
		if (ftp_client.getInstance == NULL) {
			LOGE("could not find getInstance method");
			goto fresh_xml_cleanup;
		}

		ftp_client.abortDownloading = (*env)->GetMethodID(env, ftp_client._class, "abortDownloading", "()V");
		if (ftp_client.abortDownloading == NULL) {
			LOGE("Couldn't find abortDownloading method");
			goto fresh_xml_cleanup;
		}

		ftp_client.object = (*env)->CallStaticObjectMethod(env, ftp_client._class, ftp_client.getInstance);
		if (ftp_client.object == NULL) {
			LOGE("could not find object");
			goto fresh_xml_cleanup;
		}

		if (global_sfd == -1) { // Currently has no mission
			global_sfd = sfd;
			(*env)->CallVoidMethod(env, ftp_client.object, ftp_client.startProcess, url);
			handle_jni_error(goto fresh_xml_cleanup);
		} else {
			// break mission, cleanup and start the new mission
			tcp_reply("Waiting for cleanup", strlen("Waiting for cleanup"));
			(*env)->CallVoidMethod(env, ftp_client.object, ftp_client.abortDownloading);
			handle_jni_error(goto fresh_xml_cleanup);

			global_sfd = sfd;
			(*env)->CallVoidMethod(env, ftp_client.object, ftp_client.startProcess, url);
			handle_jni_error(goto fresh_xml_cleanup);
		}

		if (ftp_client.object)
			(*env)->DeleteLocalRef(env, ftp_client.object);
		break;

		fresh_xml_cleanup:
		if (ftp_client.object)
			(*env)->DeleteLocalRef(env, ftp_client.object);

		tcp_reply("Fresh xml failed", strlen("Fresh xml failed"));
		return -1;
	case SC_FTP_DOWN_STATUS:
		global_sfd = sfd;
		env = ad_get_jni_env();

		BSQ_TRANSFER_INFO *transfer_info = NULL;
		jobjectArray fileNameArray = NULL;
		jlongArray fileSizeArray = NULL;
		jlongArray completedSizeArray = NULL;
		jint fileCount = 0;

		ftp_client.getInstance = (*env)->GetStaticMethodID(env, ftp_client._class, "getInstance", "()Lcom/bsq/advertise/FTPClient;");
		if (ftp_client.getInstance == NULL) {
			LOGE("could not find getInstance method");
			goto get_download_status_cleanup;
		}

		ftp_client.object = (*env)->CallStaticObjectMethod(env, ftp_client._class, ftp_client.getInstance);
		if (ftp_client.object == NULL) {
			LOGE("could not find object");
			goto get_download_status_cleanup;
		}

		ftp_client.getResourceFileCount = (*env)->GetMethodID(env, ftp_client._class, "getResourceFileCount", "()I");
		if (ftp_client.getResourceFileCount == NULL) {
			LOGE("could not find getResourceFileCount method");
			goto get_download_status_cleanup;
		}
		fileCount = (*env)->CallIntMethod(env, ftp_client.object, ftp_client.getResourceFileCount);
		transfer_info = (BSQ_TRANSFER_INFO *) malloc(sizeof(BSQ_TRANSFER_INFO) * fileCount);
		if (transfer_info == NULL) {
			LOGE("OutOfMemory");
			goto get_download_status_cleanup;
		}
		memset(transfer_info, 0, sizeof(BSQ_TRANSFER_INFO) * fileCount);

		ftp_client.getResourceFileNameList = (*env)->GetMethodID(env, ftp_client._class, "getResourceFileNameList", "()[Ljava/lang/String;");
		if (ftp_client.getResourceFileNameList == NULL) {
			LOGE("could not find getResourceFileNameList method");
			goto get_download_status_cleanup;
		}
		fileNameArray = (*env)->CallObjectMethod(env, ftp_client.object, ftp_client.getResourceFileNameList);
		handle_jni_error(goto get_download_status_cleanup);

		ftp_client.getResourceFileTotalList = (*env)->GetMethodID(env, ftp_client._class, "getResourceFileTotalList", "()[J");
		if (ftp_client.getResourceFileTotalList == NULL) {
			LOGE("could not find getResourceFileTotalList method");
			goto get_download_status_cleanup;
		}
		fileSizeArray = (*env)->CallObjectMethod(env, ftp_client.object, ftp_client.getResourceFileTotalList);
		handle_jni_error(goto get_download_status_cleanup);

		ftp_client.getResourceFileCompletedSizeList = (*env)->GetMethodID(env, ftp_client._class, "getResourceFileCompletedSizeList", "()[J");
		if (ftp_client.getResourceFileCompletedSizeList == NULL) {
			LOGE("could not find getResourceFileCompletedSizeList method");
			goto get_download_status_cleanup;
		}
		completedSizeArray = (*env)->CallObjectMethod(env, ftp_client.object, ftp_client.getResourceFileCompletedSizeList);
		handle_jni_error(goto get_download_status_cleanup);

		memset(&packet_head_send, 0, sizeof(PACKET_HEAD));
		packet_head_send.m_nCommand = htobe16(CS_RETURN);
		packet_head_send.m_nDataLen = htobe32(sizeof(BSQ_TRANSFER_INFO) * fileCount);

		jstring fileName;
		jlong total_size;
		jlong downloaded_size;
		const char *temp = NULL;
		for (i = 0; i < fileCount; i++) {
			fileName = (*env)->GetObjectArrayElement(env, fileNameArray, i);
			temp = (*env)->GetStringUTFChars(env, fileName, NULL);
			memcpy(transfer_info[i].file_name, temp, strlen(temp));
			(*env)->ReleaseStringUTFChars(env, fileName, temp);
			(*env)->DeleteLocalRef(env, fileName);
			//			LOGV("file_name: %s\n", transfer_info[i].file_name);
			total_size = (*env)->GetLongArrayElements(env, fileSizeArray, NULL)[i];
			transfer_info[i].totle_size =  htobe64(total_size);
			(*env)->ReleaseLongArrayElements(env, fileSizeArray, &total_size, 0);
			//			LOGV("totle_size: %lld\n", transfer_info[i].totle_size);
			downloaded_size = (*env)->GetLongArrayElements(env, completedSizeArray, NULL)[i];
			transfer_info[i].download_size = htobe64(downloaded_size);
			(*env)->ReleaseLongArrayElements(env, completedSizeArray, &downloaded_size, 0);
			//			LOGV("download_size: %lld\n", transfer_info[i].download_size);

		}

		send_data_len = sizeof(BSQ_TRANSFER_INFO) * fileCount + sizeof(PACKET_HEAD);
		send_data = (char *) malloc(send_data_len);
		if (send_data == NULL) {
			LOGE("OutOfMemory.");
			goto get_download_status_cleanup;
		}

		memset(send_data, 0, send_data_len);
		memcpy(send_data, &packet_head_send, sizeof(PACKET_HEAD));
		memcpy(send_data + sizeof(PACKET_HEAD), transfer_info, sizeof(BSQ_TRANSFER_INFO) * fileCount);

		if (send(sfd, (const char *) send_data, send_data_len, 0) == -1) {
			LOGE("send(): %s\n", strerror(errno));
			goto get_download_status_cleanup;
		}

		if (ftp_client.object)
			(*env)->DeleteLocalRef(env, ftp_client.object);
		if (transfer_info)
			free(transfer_info);
		if (send_data)
			free(send_data);
		break;

		get_download_status_cleanup:
		if (ftp_client.object)
			(*env)->DeleteLocalRef(env, ftp_client.object);
		if (transfer_info)
			free(transfer_info);
		if (send_data)
			free(send_data);
		tcp_reply("Get progress failed", strlen("Get progress failed"));
		return -1;
	case SC_GET_SCREEN:
		global_sfd = sfd;
		env = ad_get_jni_env();

		char *addr;
		int fd;
		struct stat sb = { 0 };

		addr = NULL;
		fd = -1;

		sys_info.getScreenShot = (*env)->GetStaticMethodID(env, sys_info._class, "takeScreenShot", "()J");
		if (sys_info.getScreenShot == NULL) {
			LOGE("Could't find getScreenShot method");
			goto get_screenshot_cleanup;
		}

		(*env)->CallStaticVoidMethod(env, sys_info._class, sys_info.getScreenShot);
		handle_jni_error(goto get_screenshot_cleanup);

		sync();
		fd = open("/mnt/extsd/screen.jpg", O_RDWR);
		if (fd == -1) {
			LOGE("open(): %s\n", strerror(errno));
			goto get_screenshot_cleanup;
		}

		if (fstat(fd, &sb) == -1) {
			LOGE("fstat(): %s\n", strerror(errno));
			goto get_screenshot_cleanup;
		}

		LOGE("st_size: %lld\n", sb.st_size);
		addr = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
		if (addr == MAP_FAILED) {
			LOGE("mmap(): %s\n", strerror(errno));
			goto get_screenshot_cleanup;
		}

		memset(&packet_head_send, 0, sizeof(PACKET_HEAD));
		packet_head_send.m_nCommand = htobe16(CS_RETURN);
		packet_head_send.m_nDataLen = htobe32(sb.st_size);

		send_data_len = sb.st_size + sizeof(PACKET_HEAD);
		send_data = malloc(send_data_len);
		if (send_data == NULL) {
			LOGE("OutOfMemory");
			goto get_screenshot_cleanup;
		}
		memset(send_data, 0, send_data_len);

		memcpy(send_data, &packet_head_send, sizeof(PACKET_HEAD));
		memcpy(send_data + sizeof(PACKET_HEAD), addr, sb.st_size);

		if (send(sfd, (const char *) send_data, send_data_len, 0) == -1) {
			LOGE("send(): %s\n", strerror(errno));
			goto get_screenshot_cleanup;
		}

		if (addr != NULL) {
			if (munmap(addr, sb.st_size) == -1) {
				LOGE("munmap(): %s\n", strerror(errno));
			}
		}
		if (send_data)
			free(send_data);
		if (fd != -1)
			close(fd);
		break;

		get_screenshot_cleanup:
		if (addr != NULL) {
			if (munmap(addr, sb.st_size) == -1) {
				LOGE("munmap(): %s\n", strerror(errno));
			}
		}
		if (send_data)
			free(send_data);
		if (fd != -1)
			close(fd);
		return -1;
	case SC_SET_STATUS:
		global_sfd = sfd;
		env = ad_get_jni_env();
		int status = ntohl(*(uint32_t *)data);

		sys_info.setStatus = (*env)->GetStaticMethodID(env, sys_info._class, "setStatus", "(I)V");
		if (sys_info.setStatus == NULL) {
			LOGE("Could't find setStatus method");
			goto set_status_cleanup;
		}

		(*env)->CallStaticVoidMethod(env, sys_info._class, sys_info.setStatus, status);
		handle_jni_error(goto set_status_cleanup);

		tcp_reply("set status successed", strlen("set status successed"));
		break;

		set_status_cleanup:
		tcp_reply("set status failed", strlen("set status failed"));
		return -1;
	case SC_GET_DIR:
		LOGV("SC_GET_DIR");
		global_sfd = sfd;
		env = ad_get_jni_env();

		char fullPath[1024] = { 0 };
		char resourcePath[32] = { 0 };
		char *sp = sdcardPath;
		BSQ_FILE_INFO bsq_file_info = { 0 };
		struct stat sb1 = { 0 };

		memcpy(resourcePath, sp, strlen(sdcardPath));
		strncat(resourcePath, "/resource", strlen("/resource"));
		if (data != NULL)
			strncat(resourcePath, data, strlen(data));
		LOGV("data: %s\n", data);
		LOGV("cmd: %s\n", resourcePath);

		struct dirent **namelist;
		int n;

		n = scandir(resourcePath, &namelist, NULL, alphasort);
		if (n == -1) {
			LOGE("scandir(): %s\n", strerror(errno));
			goto get_dir_cleanup;
		} else {
			char *index = NULL;

			send_data_len = sizeof(PACKET_HEAD) + (n - 2) * sizeof(BSQ_FILE_INFO);
			send_data = (char *) malloc(send_data_len);
			if (send_data == NULL) {
				LOGE("OutOfMemory");
				goto get_dir_cleanup;
			}
			memset(send_data, 0, send_data_len);
			index = send_data;

			memset(&packet_head_send, 0, sizeof(PACKET_HEAD));
			packet_head_send.m_nCommand = htobe16(CS_RETURN);
			packet_head_send.m_nDataLen = htobe32((n - 2) * sizeof(BSQ_FILE_INFO));

			memcpy(index, &packet_head_send, sizeof(PACKET_HEAD));
			index += sizeof(PACKET_HEAD);
			while (n--) {
				memset(&bsq_file_info, 0, sizeof(BSQ_FILE_INFO));
				memset(&sb1, 0, sizeof(struct stat));

				if (!strcmp(namelist[n]->d_name, ".") || !strcmp(namelist[n]->d_name, ".."))
					continue;
				memset(fullPath, 0, 1024);
				strncat(fullPath, resourcePath, strlen(resourcePath));
				strncat(fullPath, "/", strlen("/"));
				strncat(fullPath, namelist[n]->d_name, strlen(namelist[n]->d_name));
				//				LOGV("fileName: %s\n", fullPath);
				if (stat(fullPath, &sb1) == -1) {
					LOGE("stat(): %s\n", strerror(errno));
					goto get_dir_cleanup;
				}

				bsq_file_info.type = (S_ISDIR(sb1.st_mode) ? BSQ_DIR : BSQ_FILE);
				if (bsq_file_info.type == BSQ_FILE)
					bsq_file_info.size = htobe64((int64_t) sb1.st_size);
				else {
					bsq_file_info.size = htobe64((int64_t) sb1.st_size); // TODO: need more code
				}

				strncpy(bsq_file_info.lastChangeTime, ctime((const time_t *) &sb1.st_mtime), 19);
				strncpy(bsq_file_info.fullFileName, namelist[n]->d_name, strlen(namelist[n]->d_name));

				memcpy(index, &bsq_file_info, sizeof(bsq_file_info));
				index += sizeof(bsq_file_info);
			}
			free(namelist);
		}

		if (send(sfd, (const char *) send_data, send_data_len, 0) == -1) {
			LOGE("send(): %s\n", strerror(errno));
			goto get_dir_cleanup;
		}

		free(send_data);
		break;

		get_dir_cleanup:
		if (send_data)
			free(send_data);
		if (namelist)
			free(namelist);
		tcp_reply("get dir information failed", strlen("get dir information failed"));
		return -1;
	case SC_DELETE_FILE:
		LOGV("SC_DELETE_FILE");
		global_sfd = sfd;
		env = ad_get_jni_env();

		LOGV("data: %s\n", data);
		char res_path[1024] = {'\0'};
		strncat(res_path, sdcardPath, strlen(sdcardPath));
		strcat(res_path, "/resource");
		strncat(res_path, data, strlen(data));
		LOGV("delete: %s\n", res_path);

		struct stat res_del;
		if (stat(res_path, &res_del) == -1) {
			LOGE("stat(): %s\n", strerror(errno));
			goto delete_file_cleanup;
		}

		if (S_ISDIR(res_del.st_mode)) {
			if (rm_dir(res_path) == -1) {
				goto delete_file_cleanup;
			}
		} else {
			if (unlink(res_path) == -1) {
				LOGE("unlink(): %s\n", strerror(errno));
				goto delete_file_cleanup;
			}
		}

		tcp_reply("Operation successed", strlen("Operation successed"));
		break;

		delete_file_cleanup:
		tcp_reply("Operation failed", strlen("Operation failed"));
		return -1;
	default:
		tcp_reply("Unknown command", strlen("Unknown command"));
		break;
	}

	return 0;
}

static int rm_dir(const char *pathname)
{
	struct dirent **namelist;
	struct stat st;
	int n;
	char fullpath[1024];

	n = scandir(pathname, &namelist, NULL, alphasort);
	if (n == -1) {
		LOGE("scandir(): %s\n", strerror(errno));
		goto cleanup;
	} else {
		while (n--) {
			if (!strcmp(namelist[n]->d_name, ".") || !strcmp(namelist[n]->d_name, ".."))
				continue;

			memset(fullpath, 0, 1024);
			strncat(fullpath, pathname, strlen(pathname));
			strcat(fullpath, "/");
			strncat(fullpath, namelist[n]->d_name, strlen(namelist[n]->d_name));

			if (stat(fullpath, &st) == -1) {
				LOGE("stat(): %s\n", strerror(errno));
				goto cleanup;
			}

			if (S_ISDIR(st.st_mode)) {
				if (rm_dir(fullpath) == -1) {
					LOGE("rm_dir(): failed");
					goto cleanup;
				}
			} else
				unlink(fullpath);
		}
	}

	if (rmdir(pathname) == -1) {
		LOGE("rmdir(): %s\n", strerror(errno));
		goto cleanup;
	}

	if (namelist != NULL)
		free(namelist);
	return 0;

	cleanup:
	if (namelist != NULL)
		free(namelist);
	return -1;
}

static void *start_routine(void *arg)
{
	int newsfd, s;
	struct sockaddr *peer_addr;
	socklen_t peer_addr_len;
	ssize_t nread;

	JavaVM *vm = ad_get_jvm();

	if (pthread_detach(pthread_self()) != 0) {
		LOGE("pthread_detach() failed\n");
		return NULL;
	}

	LOGV("new thread: %lu\n", pthread_self());

	CONNECTIONN_INFO *info = (CONNECTIONN_INFO *) arg;
	newsfd = info->sfd;
	peer_addr = &info->peer_addr;
	peer_addr_len = info->peer_addr_len;

#define MAX_TCP_BUF 2048
	char *buf = (char *) malloc(MAX_TCP_BUF);
	if (!buf) {
		LOGV("Allocate memory failed\n");
		return NULL;
	}

	memset(buf, 0, MAX_TCP_BUF);
	nread = recv(newsfd, (void *) buf, MAX_TCP_BUF, 0);
	if (nread == -1) {
		LOGE("recvfrom(): %s\n", strerror(errno));
		goto cleanup;
	}

	if (nread < sizeof(PACKET_HEAD)) {
		LOGE("something goes wrong");
		goto cleanup;
	}

	LOGV("TCP data is available now");

	char host[NI_MAXHOST], service[NI_MAXSERV];
	s = getnameinfo(peer_addr, peer_addr_len,
			host, NI_MAXHOST,
			service, NI_MAXSERV,
			NI_NUMERICHOST | NI_NUMERICSERV);
	if (s == 0)
		LOGV("Received %ld bytes from %s:%s\n",
				(long) nread, host, service);
	else
		LOGE("getnameinfo: %s\n", gai_strerror(s));

	if (reply_client(newsfd, (const void *) buf, nread) != -1) {
		LOGE("reply_client successed");
	} else {
		LOGE("reply_client failed");
		goto cleanup;
	}

	LOGV("thread ID: %lu exit successful\n", pthread_self());
	(*vm)->DetachCurrentThread(vm);

	LOGV("close socket: %d\n", newsfd);
	if (close(newsfd) == -1)
		LOGE("close(): %s\n", strerror(errno));
	global_sfd = -1;
	if (buf)
		free(buf);
	free(arg);
	return NULL;

	cleanup:
	(*vm)->DetachCurrentThread(vm);

	if (buf)
		free(buf);
	LOGV("close socket: %d\n", newsfd);
	if (close(newsfd) == -1)
		LOGE("close(): %s\n", strerror(errno));
	global_sfd = -1;
	free(arg);
	return NULL;
}

int tcp_server_creation()
{
	struct addrinfo hints;
	struct addrinfo *result, *rp;
	int sfd, s, newsfd;
	int sockopt;
	struct sockaddr_storage peer_addr;
	socklen_t peer_addr_len;
	fd_set rsfds;
	struct timeval tv;
	int reuseaddr = 1;
	int keepalive = 1;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET;    /* Allow IPv4 */
	hints.ai_socktype = SOCK_STREAM; /* Stream socket */
	hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
	hints.ai_protocol = 0;          /* Any protocol */
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;

	s = getaddrinfo(NULL, COMMUNICATIONPORTSTR, &hints, &result);
	if (s != 0) {
		LOGE("getaddrinfo: %s\n", gai_strerror(s));
		return -1;
	}

	/* getaddrinfo() returns a list of address structures.
       Try each address until we successfully bind(2).
       If socket(2) (or bind(2)) fails, we (close the socket
       and) try the next address. */

	for (rp = result; rp != NULL; rp = rp->ai_next) {
		sfd = socket(rp->ai_family, rp->ai_socktype,
				rp->ai_protocol);
		if (sfd == -1)
			continue;

		sockopt = 1;
		if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(int)) == -1) {
			LOGV("setsockopt(): %s\n", strerror(errno));
			return -1;
		}

		if (bind(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
			break;                  /* Success */

		close(sfd);
	}

	if (rp == NULL) {               /* No address succeeded */
		LOGV("bind(): %s\n", strerror(errno));
		freeaddrinfo(result);           /* No longer needed */
		return -1;
	}

	freeaddrinfo(result);           /* No longer needed */

	newsfd = -1;

	if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (const void *) &reuseaddr, sizeof(int)) == -1) {
		LOGE("setsockopt(): %s\n", strerror(errno));
	}

	if (setsockopt(sfd, SOL_SOCKET, SO_KEEPALIVE, (const void *) &keepalive, sizeof(int)) == -1) {
		LOGE("setsockopt(): %s\n", strerror(errno));
	}

	//	LOGE("listening socket: %d\n", sfd);
	if (listen(sfd, SOMAXCONN) == -1) {
		LOGV("listen(): %s\n", strerror(errno));
		return -1;
	}

	tcp_holding = true;
	while (tcp_holding) {
		FD_ZERO(&rsfds);
		FD_SET(sfd, &rsfds);

		/* Wait up to one seconds */
		tv.tv_sec = 0;
		tv.tv_usec = 10;

		if ((s = select(sfd + 1, &rsfds, NULL, NULL, &tv)) == -1) {
			if (s == EINTR)
				continue;
			LOGE("select(): %s\n", strerror(errno));
			return -1;
		}

		if (FD_ISSET(sfd, &rsfds)) {
			peer_addr_len = sizeof(struct sockaddr_storage);
			if ((newsfd = accept(sfd, (struct sockaddr *) &peer_addr, &peer_addr_len)) == -1) {
				LOGV("accept(): %s\n", strerror(errno));
				return -1;
			} else {
				LOGV("New connection is coming\n");
				CONNECTIONN_INFO *info = (CONNECTIONN_INFO *) malloc(sizeof(CONNECTIONN_INFO)); // freed by thread routine
				if (info) {
					memset(info, 0, sizeof(CONNECTIONN_INFO));
					info->sfd = newsfd;
					LOGV("new socket: %d\n", newsfd);
					info->peer_addr = (*((struct sockaddr *) &peer_addr));
					info->peer_addr_len = peer_addr_len;
				} else {
					LOGE("Out of memory");
					break;
				}

				pthread_t pid;
				if (pthread_create(&pid, NULL, &start_routine, (void *) info) != 0) {
					tcp_reply("Unknown error", strlen("Unknown error"));
				}
			}
		}
	}

	//	LOGE("close listenning socket: %d\n", sfd);
	if (close(sfd) == -1)
		LOGE("close(): %s\n", strerror(errno));

	return 0;
}

void tcp_destory()
{
	tcp_holding = false;
}
