#ifdef ANDROID
#include <jni.h>
#include "../include/log.h"
#endif

#include <stdio.h>

static JavaVM *ad_vm = NULL;

#ifndef WIN32
#include <pthread.h>

static pthread_key_t jnienv_key;

void _android_key_cleanup(void *data) {
	LOGV("Thread end, detaching jvm from current thread");
	JNIEnv * env = (JNIEnv *) pthread_getspecific(jnienv_key);
	if (env != NULL) {
		(*ad_vm)->DetachCurrentThread(ad_vm);
		pthread_setspecific(jnienv_key, NULL);
	}
}

#endif

void ad_set_jvm(JavaVM *vm) {
	ad_vm = vm;
#ifndef WIN32
	pthread_key_create(&jnienv_key, _android_key_cleanup);
#endif
}

JavaVM *ad_get_jvm(void) {
	return ad_vm;
}

JNIEnv *ad_get_jni_env(void) {
	JNIEnv *env = NULL;
	if (ad_vm == NULL) {
		LOGE("calling ad_get_jni_env() while no jvm has been set using ms_set_jvm().");
	} else {
#ifndef WIN32
		env = (JNIEnv *) pthread_getspecific(jnienv_key);
		if (env == NULL) {
			if ((*ad_vm)->AttachCurrentThread(ad_vm, &env, NULL) != 0) {
				LOGE("AttachCurrentThread() failed !");
				return NULL;
			}
			pthread_setspecific(jnienv_key, env);
			LOGV("JNIEnv is attached to current Thread");
		}
#else
		LOGE("ad_get_jni_env() not implemented no windows");
#endif
	}

	return env;
}
