#ifndef DISP_PWM_H
#define DISP_PWM_H

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <jni.h>

 int SetPWMON();
 int SetPWMOFF();

#endif
