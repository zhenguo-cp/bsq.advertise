#include "disp_pwm.h"
#include <stdio.h>
#include <android/log.h>

#define DRIVER_NAME "/dev/myled"


int SetPWMON()
{
	int err;
	unsigned long  args[5];
	int fd = open(DRIVER_NAME, O_RDWR);
    if (fd < 0) {
      //  ALOGD("could not open %s", DRIVER_NAME);
    	//__android_log_print(ANDROID_LOG_INFO, "Hello888888888888888", "size int -----   ---");
        return -1;
    }
	
	
	err = ioctl(fd, 0, 0);
    close(fd);
	return err;
}

int SetPWMOFF()
{
	int err;
	unsigned long  args[5];
	int fd = open(DRIVER_NAME, O_RDWR);
    if (fd < 0) {
     //   ALOGD("could not open %s", DRIVER_NAME);
    	//__android_log_print(ANDROID_LOG_INFO, "chen Hello11111555555555", "size int -----   ---");
        return -1;
    }
	
	err = ioctl(fd, 1, 0);
	//__android_log_print(ANDROID_LOG_INFO, "chen Hello11111555555555", "size int -----   ---");
    close(fd);
	return err;
}
