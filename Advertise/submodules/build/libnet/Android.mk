LOCAL_PATH := $(call my-dir)/../../libnet-1.2-rc2

include $(CLEAR_VARS)

LOCAL_ARM_MODE := arm

LOCAL_MODULE := bsq_net
# LOCAL_EXPORT_C_INCLUDES := \
# 	../../openssl/include/ \
# 	../../openssl/crypto/ \
# 	../../openssl/ssl/ \
# 	../../openssl/ \
# 	../../openssl/crypto/asn1 \
# 	../../openssl/crypto/evp

LOCAL_CPP_EXTENSION := .cc

LOCAL_SRC_FILES := \
	$(LOCAL_PATH)/src/udp_process.c \
	$(LOCAL_PATH)/src/tcp_process.c \
	$(LOCAL_PATH)/src/bsq_net_jni.cc \
	$(LOCAL_PATH)/src/bsq_java.c \
#	$(LOCAL_PATH)/src/libnet_advanced.c \
#	$(LOCAL_PATH)/src/libnet_asn1.c \
#	$(LOCAL_PATH)/src/libnet_build_802.1q.c \
#	$(LOCAL_PATH)/src/libnet_build_802.1x.c \
#	$(LOCAL_PATH)/src/libnet_build_802.2.c \
#	$(LOCAL_PATH)/src/libnet_build_802.3.c \
#	$(LOCAL_PATH)/src/libnet_build_arp.c \
#	$(LOCAL_PATH)/src/libnet_build_bgp.c \
#	$(LOCAL_PATH)/src/libnet_build_cdp.c \
#	$(LOCAL_PATH)/src/libnet_build_data.c \
#	$(LOCAL_PATH)/src/libnet_build_dhcp.c \
#	$(LOCAL_PATH)/src/libnet_build_dns.c \
#	$(LOCAL_PATH)/src/libnet_build_ethernet.c \
#	$(LOCAL_PATH)/src/libnet_build_fddi.c \
#	$(LOCAL_PATH)/src/libnet_build_gre.c \
#	$(LOCAL_PATH)/src/libnet_build_hsrp.c \
#	$(LOCAL_PATH)/src/libnet_build_icmp.c \
#	$(LOCAL_PATH)/src/libnet_build_igmp.c \
#	$(LOCAL_PATH)/src/libnet_build_ip.c \
#	$(LOCAL_PATH)/src/libnet_build_ipsec.c \
#	$(LOCAL_PATH)/src/libnet_build_isl.c \
#	$(LOCAL_PATH)/src/libnet_build_link.c \
#	$(LOCAL_PATH)/src/libnet_build_mpls.c \
#	$(LOCAL_PATH)/src/libnet_build_ntp.c \
#	$(LOCAL_PATH)/src/libnet_build_ospf.c \
#	$(LOCAL_PATH)/src/libnet_build_rip.c \
#	$(LOCAL_PATH)/src/libnet_build_rpc.c \
#	$(LOCAL_PATH)/src/libnet_build_sebek.c \
#	$(LOCAL_PATH)/src/libnet_build_snmp.c \
#	$(LOCAL_PATH)/src/libnet_build_stp.c \
#	$(LOCAL_PATH)/src/libnet_build_tcp.c \
#	$(LOCAL_PATH)/src/libnet_build_token_ring.c \
#	$(LOCAL_PATH)/src/libnet_build_udp.c \
#	$(LOCAL_PATH)/src/libnet_build_vrrp.c \
#	$(LOCAL_PATH)/src/libnet_checksum.c \
#	$(LOCAL_PATH)/src/libnet_cq.c \
#	$(LOCAL_PATH)/src/libnet_crc.c \
#	$(LOCAL_PATH)/src/libnet_error.c \
#	$(LOCAL_PATH)/src/libnet_if_addr.c \
#	$(LOCAL_PATH)/src/libnet_init.c \
#	$(LOCAL_PATH)/src/libnet_internal.c \
#	$(LOCAL_PATH)/src/libnet_link_linux.c \
#	$(LOCAL_PATH)/src/libnet_pblock.c \
#	$(LOCAL_PATH)/src/libnet_port_list.c \
#	$(LOCAL_PATH)/src/libnet_prand.c \
#	$(LOCAL_PATH)/src/libnet_raw.c \
#	$(LOCAL_PATH)/src/libnet_resolve.c \
#	$(LOCAL_PATH)/src/libnet_version.c \
#	$(LOCAL_PATH)/src/libnet_write.c \
	$(LOCAL_PATH)/src/libnetcore_jni.cc
LOCAL_SHARED_LIBRARIES += crypto-prebuilt ssl-prebuilt
LOCAL_LDLIBS := -lz -llog -pthread
LOCAL_CFLAGS += \
	-Wall -O4 -fexpensive-optimizations -funroll-loops -mfpu=neon -D_POSIX_SOURCE
LOCAL_CPP_FEATURES := rtti	

include $(BUILD_SHARED_LIBRARY)
